/**
 * Functions used in panel
 */

function showChangeSentimentDropdown(mention_id)
{
    if ($('#li-mention-sentiment-'+mention_id).is(':visible'))
    {
        $('#li-mention-sentiment-'+mention_id).hide()
    }
    else
    {
        $('#li-mention-sentiment-'+mention_id).show();
    }

}

function showNewResultsCountList()
{
    if (new_results_sum > 0)
    {
        if ($('#new_resuls_count_searches_list').is(':visible'))
            $('#new_resuls_count_searches_list').hide();
        else
        {
            var offset = $('#new_resuls_count_box').offset();

            $('#new_resuls_count_searches_list').css({'left':(offset.left+33)+'px','top':'40px'});
            $('#new_resuls_count_searches_list').show();
        }
    }
}

function showUserAccounts()
{
    if ($('#user_accounts_list').is(':visible'))
        $('#user_accounts_list').hide();
    else
    {
        var offset = $('#user_accounts_list_box').offset();

        $('#user_accounts_list').css({'left':(offset.left)+'px','top':'50px'});
        $('#user_accounts_list').show();
    }
}

function bocyClickTrigger(evt)
{
    //alert(evt.target.id);
    if ($('#groupsAddList:visible').length == 1)
        $('#groupsAddList:visible').hide();

    if ($('#sentimentChangeList:visible').length == 1)
        $('#sentimentChangeList:visible').hide();

    if ((evt.target.id != 'new_resuls_count_box_value') && (evt.target.id != 'new_resuls_count_box'))
        if ($('#new_resuls_count_searches_list:visible').length == 1)
            $('#new_resuls_count_searches_list:visible').hide();

    if ((evt.target.id != 'user_accounts_list') && (evt.target.id != 'user_accounts_list_box'))
        if ($('#user_accounts_list:visible').length == 1)
            $('#user_accounts_list:visible').hide();
}

function showResultsFilters()
{
    if ($('.hidden_filter').is(':visible'))
    {
    	setCookie('showResultsFilters', '0', 60);
        $('.hidden_filter').hide();
        $('#show_results_filters_btn').html(lp._('Pokaż dodatkowe filtry'));
		toggleFilter('sa', '');
    }
    else
    {
    	setCookie('showResultsFilters', '1', 60);
        $('.hidden_filter').show();
        $('#show_results_filters_btn').html(lp._('Ukryj dodatkowe filtry'));
		toggleFilter('sa', 1);
    }
}

/**
 * Gdy konto ma ustawiony limit czasowy wtedy przy zmianie daty w kalendarzu sprawdzamy czy zakres sie miesci w limicie i ewentualnie wywalammy komunikar.
 * @param int maximumCalendarLookbackDaysLimit
 * @param Date dateFrom
 * @param Date dateTo
 * @return bool
 */
function isDateRangeAvailable(maximumCalendarLookbackDaysLimit, dateFrom)
{
    $('#date_range_box_info').remove();
    //jesli jest limit na ilosc dni sprawdzam czy ktoras z dat jest poza zakresem
    if (maximumCalendarLookbackDaysLimit > 0 && dateFrom.getTime() < maximumCalendarLookbackDaysLimit)
    {
        showInfoDateRangeNotAvailable();
    }
}

/**
 * Metoda inicjuje kalendarz z wyborem dat
 */
function showDateRangeBox()
{
    if ($('#date_range_box').is(':visible'))
    {
        $('#date_range_box').hide();
    }
    else
    {
        var offset = $('#date_range_offset').offset();

        $('#date_range_box').css({'left': (offset.left - 60) + 'px', 'top': (offset.top + 25) + 'px'});
        $('#date_range_box').show();

		var ffrom = $('#date1').val();
		var fto = $('#date2').val();
		var from = strToDate(ffrom);
		var to = strToDate(fto);

		if (datePicker == null)
		{
			datePicker = $('#datepicker-calendar').DatePicker({
				inline: true,
				date: [from, to],
				calendars: 3,
				mode: 'range',
				current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
				onChange: function(dates, el) {

					$('#range_type').val('');
					// update the range display
					var new_from = dates[0].getFullYear() + '-' + ('0' + (dates[0].getMonth() + 1)).substr(-2) + '-' + ('0' + dates[0].getDate()).substr(-2);
					var new_to = dates[1].getFullYear() + '-' + ('0' + (dates[1].getMonth() + 1)).substr(-2) + '-' + ('0' + dates[1].getDate()).substr(-2);

					//tylko przy zmianie pierwszej daty
                    if (new_from != $('#date1').val())
                    {
                        //jesli jest limit na ilosc dni sprawdzam czy ktoras z dat jest poza zakresem
                        isDateRangeAvailable(maximumCalendarLookbackDaysLimit, dates[0]);
                    }

                    $('#date1').val(new_from);
					$('#date2').val(new_to);
					$('#date-range-field span').text(new_from + ' - ' + new_to);
					$('#save_default_date_range').removeAttr('checked');
				}
			});
		}

   		// initialize the special date dropdown field
   		$('#date-range-field span').text(ffrom + ' - ' + fto);

   		$('#datepicker-calendar').click(function(event){
     		event.stopPropagation();
   		});
    }
}

/**
 * Metoda na podstawie wybranego typu zakresu dat ustawia odpowiednie daty od do
 * @return bool
 */
function setDateRangeByType()
{
	var type = $('#range_type').val();
	if(type == '1')
	{
		var from = new Date();
		var to = from;
	}
	else if(type == '2')
	{
		var today = new Date();
		var from = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 1);
		var to = from;
	}
	else if(type == '3')
	{
		var today = new Date();
		var from = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 7);
		var to = today;
	}
	else if(type == '4')
	{
		var today = new Date();
		var from = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 30);
		var to = today;
	}
	else if(type == '5')
	{
		var today = new Date();
		var from = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 90);
		var to = today;
	}
	else if(type == '6')
	{
		var today = new Date();
		var from = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 365);
		var to = today;
	}
	else if(type == '7')
	{
  		var fromArray = dateStart.split('-');
		var from = new Date(fromArray[0], fromArray[1]-1,fromArray[2]);
		var to = new Date();
	}
	else
	{
  			var ffrom = $('#date1').val();
        	var fto = $('#date2').val();
      		var from = strToDate(ffrom);
        	var to = strToDate(fto);
	}

    //jesli jest limit na ilosc dni sprawdzam czy ktoras z dat jest poza zakresem
    isDateRangeAvailable(maximumCalendarLookbackDaysLimit, from);

	if(type==defaultDateRangeType)
	{
		$('#save_default_date_range').attr('checked','true');
	}
	else
	{
		$('#save_default_date_range').removeAttr('checked');
	}
    var new_from = from.getFullYear()+'-'+('0'+(from.getMonth()+1)).substr(-2)+'-'+('0'+from.getDate()).substr(-2);
    var new_to = to.getFullYear()+'-'+('0'+(to.getMonth()+1)).substr(-2)+'-'+('0'+to.getDate()).substr(-2);
    $('#date1').val(new_from);
    $('#date2').val(new_to);
    $('#date-range-field span').text(new_from + ' - ' + new_to);
    $('#datepicker-calendar').DatePickerSetDate([from, to], true);
    return true;
}

/**
 * Akcja uruchamiana po wybraniu i zatwierdzeniu zakresu dat
 * Sprawdza poprawnosc, zapisuje domyslny filtr jesli wskazano oraz filtruje wyniki
 */
function customDateRange()
{
	var date1 = $('#date1').val();
	var date2 = $('#date2').val();
	var type = $('#range_type').val();
	var params_ov = '';

	if (date1.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/))
	{
	}
	else
	{
		prepareDialogBox(2,lp._("Błędna forma daty, poprawna forma to: RRRR-MM-DD"));
		$("#dialog-window").dialog({
			resizable: false,
			height: 180,
			modal: true,
		});
		return;
	}

	if (date2.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/))
	{
	}
	else
	{
		prepareDialogBox(2, lp._("Błędna forma daty, poprawna forma to: RRRR-MM-DD"));
		$("#dialog-window").dialog({
			resizable: false,
			height: 180,
			modal: true,
		});
		return;
	}

	if ((date1 == '') || (date2 == ''))
	{
		prepareDialogBox(2, lp._("Uzupełnij poprawnie zakres dat"));
		$("#dialog-window").dialog({
			resizable: false,
			height: 180,
			modal: true,
		});
		return;
	}

	if ($('#save_default_date_range').is(':checked'))
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + 1095);
		document.cookie = 'defaultDateRangeType' + searchesId + '=' + type + '; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		document.cookie = 'defaultDate1' + searchesId + '='+date1 + '; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		document.cookie = 'defaultDate2' + searchesId + '='+date2 + '; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		$('#save_default_date_range').removeAttr(':checked');
		$('#default_range_type').val(type);
		defaultDateRangeType = type;
	}

	if (!$('#save_default_date_range').is(':checked') && type == defaultDateRangeType)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() - 1095);
		document.cookie = 'defaultDateRangeType' + searchesId + '=; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		document.cookie = 'defaultDate1' + searchesId + '=; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		document.cookie = 'defaultDate2' + searchesId + '=; expires=' + exdate.toUTCString() + '; path=/; domain=' + panelDomain;
		defaultDateRangeType = '4';
		var today = new Date();
		var from = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
		var to = today;
		$('#range_type').val(defaultDateRangeType);
		$('#date1').val(from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).substr(-2) + '-' + ('0' + from.getDate()).substr(-2));
		$('#date2').val(to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).substr(-2) + '-' + ('0' + to.getDate()).substr(-2));
		$('#date_range_offset a').html($('#range_type :selected').text());
		$('#datepicker-calendar').DatePickerSetDate([from, to], true);
		$('#date-range-field span').text($('#date1').val() + ' - ' + $('#date2').val());
		$('#default_range_type').val('4');
	}

	$('.date1').val(date1);
	$('.date2').val(date2);

	var params_data = '/sid/' + searches_id + '/date1/' + date1 + '/date2/' + date2 + '/dr/' + type;
	if (only_valuable == 1)
	{
		params_ov = '/ov/1';
	}
	params_data = params_data+params_ov;

	var link = '';

	switch (tab)
	{
		case 'quotes':
			link = '/panel/quotes'+params_data;
			break;
		case 'results':
		case 'sources':
		case 'compare':
			page = 1;
			showDateRangeBox();
			loadTabData();
			return;
			break;
		case 'analysis':
			//ustawiam zmienne tak by wskazywaly wczytywanie rankingu wpisow po raz pierwszy
			loadTabDataTimeout = null;
			firstLoadOfAnalysisMostInteractiveEntriesFromSocialMedia = true;
			firstLoadOfAnalysisEntriesFromMostPopularAuthors = true;
			showDateRangeBox();
			loadTabData();
			return;
			break;
		default:
			page = 1;
			showDateRangeBox();
			loadTabData();
			return;
			break;
	}

	//window.location = 'http://'+window.location.host+link;
	if (link != '')
	{
		window.location = link;
	}
}

function getInfoAboutSource(url, sid)
{
	$('#ajaxLoader').css({'display':'block'});
	$('#ajaxLoader').center();

	$('#overlay').css({'height':$('#site').height()});
	$('#overlay').fadeIn('slow', function() {});

	var date1 = $('#date1').val();
	var date2 = $('#date2').val();
    var type = $('#range_type').val();

	$.post('/main/getinfoaboutsource', {url: url, sid: sid},
			 function(data)
			 {
				//$('#overlay').css({'height':$('#site').height()});
				//$('#overlay').fadeIn('slow', function() {});

				//alert(data);
				$('#ajaxLoader').css({'display':'none'});

				$('#sourceInfo').css({'display':'block','width':'868px'});
				$('#sourceInfo').center();

				$('#sourceInfoWeb20').css({'display':'none'});
				$('#sourceInfoAdPlanner').css({'display':'block'});

				$('#sourceInfoHost').html('<a style="color:#0884B2" href="http://'+data.site['host']+'" target="_blank">'+data.site['host']+'</a>');

				if (tab != 'results')
					$('#sourceInfoUrlResults').html('<a href="/panel/results/?sid='+searches_id+'#do='+urlEncode(data.site['host'])+'&d1='+date1+'&d2='+date2+'&dr='+type+'">'+lp._("pokaż wpisy z danej strony")+'</a>');
				else
					$('#sourceInfoUrlResults').html('<a onclick="closeSourceInfo();$(\'#filter_input_do\').val(\''+data.site['host']+'\');toggleFilter(\'do\', \''+data.site['host']+'\');" href="javascript:void(0);">'+lp._("pokaż wpisy z danej strony")+'</a>');

				$('#sourceInfoHostTrafficVisits').html(data.site['host_traffic_visits']);

				/*$('#sourceInfoReachChart').html('<img src="'+data.site['reach_chart']+'">');
                $('#sourceInfoPageviewsChart').html('<img src="'+data.site['pageviews_chart']+'">');

                var urlsHtml = '';
                for (key in data.urls)
                    urlsHtml = urlsHtml+'<div class="panel_option"><a href="#"><img width="12" height="12" src="static/img/icons/alerts_create.png"> '+data.urls[key]+'</a></div>';

                if (urlsHtml != '')
                    $('#sourceInfoSitesAlsoVisited').html(urlsHtml); else
                        $('#sourceInfoSitesAlsoVisited').html('Brak danych');

                if(data.stats['uniqueUsersCookie'] != lp._('B/d') || data.stats['views'] != lp._('B/d') || data.stats['visits'] != lp._('B/d') || data.stats['averageUserVisits'] != lp._('B/d') || data.stats['averageTime'] != lp._('B/d'))
                {
                    $('#source_info_stats').show();
                    $('#sourceInfoUniqueUsersCount').html(data.stats['uniqueUsersCookie']);
                    $('#sourceInfoViewsCount').html(data.stats['views']);
                    $('#sourceInfoVisitsCount').html(data.stats['visits']);

                    $('#sourceInfoAverageUserVisits').html(data.stats['averageUserVisits']);

                    if (data.stats['averageTime'] != lp._('B/d'))
                        $('#sourceInfoAverageTime').html(data.stats['averageTime']+' sek.'); else
                            $('#sourceInfoAverageTime').html(data.stats['averageTime']);
                }
                else
                {
                    $('#source_info_stats').hide();
				}*/
				//$('#sourceInfoContent').html('<img src="'+data.site['traffic_chart']+'">');
			 }, "json");
}

function getInfoAboutSourceFromWeb20(url,pages_id,searches_id)
{
	$('#ajaxLoader').css({'display':'block'});
	$('#ajaxLoader').center();

	$('#overlay').css({'height':$('#site').height()});
	$('#overlay').fadeIn('slow', function() {});

	var date1 = $('#date1').val();
	var date2 = $('#date2').val();
    var type = $('#range_type').val();

	$.post('/main/getinfoaboutsourcefromweb20', {url: url,pages_id: pages_id,searches_id: searches_id},
			 function(data)
			 {
				//alert(data);

				$('#source_type_facebook').css({'display':'none'});
				$('#source_type_blip').css({'display':'none'});
				$('#source_type_pinger').css({'display':'none'});
				$('#source_type_twitter').css({'display':'none'});
				$('#source_type_google_plus').css({'display':'none'});
				$('#source_type_instagram').css({'display':'none'});

				$('#source_type_'+data.info['type']).css({'display':'block'});

				if (data.info['author_avatar_url'])
					$('#sourceInfoAuthorAvatarUrl').attr("src",'/static/img/loader.gif');

				//$('#overlay').css({'height':$('#site').height()});
				//$('#overlay').fadeIn('slow', function() {});

				$('#ajaxLoader').css({'display':'none'});

				$('#sourceInfo').css({'display':'block','width':'743px'});
				$('#sourceInfo').center();

				$('#sourceInfoAdPlanner').css({'display':'none'});
				$('#sourceInfoWeb20').css({'display':'block'});

				if (data.info['author'])
					data.info['author_name'] = data.info['author'];

				if (data.info['author_name'])
				{
					$('#sourceInfoAuthor').html('<a target="_blank" href="'+data.info['url']+'">'+data.info['author_name']+'</a>');

					$('#sourceInfoAuthor2').html('<span>'+data.info['author_name']+'</span>');

					$('#sourceInfoAuthor2Url').attr("href",data.info['url']);
				} else
					$('#sourceInfoAuthor2').html('');

				//$('#sourceInfoAuthorResults').attr("href","/panel/results/?sid="+searches_id+"#t="+data.info['author_name']);
				if (tab != 'results')
					$('#sourceInfoAuthorResults').html('<a href="/panel/results/?sid='+searches_id+'#au='+urlEncode(data.info['author_name'])+'&as='+data.info['socialmedia_sites_id']+'&d1='+date1+'&d2='+date2+'&dr='+type+'">'+lp._('pokaż wpisy autora')+'</a>');
				else
					$('#sourceInfoAuthorResults').html('<a onclick="closeSourceInfo();$(\'#filter_input_au\').val(\''+data.info['author_name']+'\');$(\'#filter_input_as\').val(\''+data.info['socialmedia_sites_id']+'\');toggleFilter(\'au\', \''+data.info['author_name']+'\');" href="javascript:void(0);">'+lp._('pokaż wpisy autora')+'</a>');


				if (data.info['quote'])
					$('#sourceInfoQuote').html('<a href="'+data.info['result_open_link']+'" target="_blank">'+data.info['quote']+'</a>...');
				else
					$('#sourceInfoQuote').html(lp._('Nie znaleziono cytatu'));

				if (data.info['author_avatar_url'])
					$('#sourceInfoAuthorAvatarUrl').attr("src",data.info['author_avatar_url']);

				if(data.info['friends_count'] > 0)
					$('#sourceInfoFriendsCount').html(data.info['friends_count'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+'+');
				else
					$('#sourceInfoFriendsCount').html(data.info['friends_count']);

				if (!data.info['city'])
					$('#sourceInfoCity').html('');
				else
					$('#sourceInfoCity').html(lp._('Lokalizacja')+': '+data.info['city']);
			 }, "json");
}

function getInfoAboutSourceFromSocialMedia(url,pages_id,searches_id)
{
    $('#ajaxLoader').css({'display':'block'});
    $('#ajaxLoader').center();

    $('#overlay').css({'height':$('#site').height()});
    $('#overlay').fadeIn('slow', function() {});

    $.post('/panel/get-info-about-source-from-social-media/', {url: url,pages_id: pages_id,sid: searches_id},
        function(data)
        {
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var type  = $('#range_type').val();

            var author_friends_count = data.info['friends_count'];
            var author_avatar_url    = data.info['author_avatar_url'];
            var author_quote         = data.info['quote'];
            var author_name          = data.info['author'];
            var socialmedia_sites_id = data.info['socialmedia_sites_id'];
            var author_url           = data.info['url'];

            var mentions_id          = data.info['mentions_id'];
            var siteType             = data.info['type'];
            var city                 = data.info['city'];
            var result_open_link	 = data.info['result_open_link'];
            //alert(data);

            $('#source_type_facebook').css({'display':'none'});
            $('#source_type_blip').css({'display':'none'});
            $('#source_type_pinger').css({'display':'none'});
            $('#source_type_twitter').css({'display':'none'});
            $('#source_type_google_plus').css({'display':'none'});
            $('#source_type_instagram').css({'display': 'none'});

            //wyswietlanie informacji o danym serwisie
            $('#source_type_'+siteType).css({'display':'block'});

            if (author_avatar_url)
                $('#sourceInfoAuthorAvatarUrl').attr("src",'/static/img/loader.gif');

            //$('#overlay').css({'height':$('#site').height()});
            //$('#overlay').fadeIn('slow', function() {});

            $('#ajaxLoader').css({'display':'none'});

            $('#sourceInfo').css({'display':'block','width':'743px'});
            $('#sourceInfo').center();

            $('#sourceInfoAdPlanner').css({'display':'none'});
            $('#sourceInfoWeb20').css({'display':'block'});

            //wyswietkany nazwe autora i budujemy link prowadzacy do niego w danym serwisie
            if (author_name)
            {
                $('#sourceInfoAuthor').html('<a target="_blank" href="'+author_url+'">'+author_name+'</a>');

                $('#sourceInfoAuthor2').html('<span>'+author_name+'</span>');

                $('#sourceInfoAuthor2Url').attr("href",author_url);
            } else
                $('#sourceInfoAuthor2').html('');

            //$('#sourceInfoAuthorResults').attr("href","/panel/results/?sid="+searches_id+"#t="+data.info['author_name']);

            //budujemy link do wpisow danego autora
            if (tab != 'results')
                $('#sourceInfoAuthorResults').html('<a href="/panel/results/?sid='+searches_id+'#au='+urlEncode(author_name)+'&as='+socialmedia_sites_id+'&d1='+date1+'&d2='+date2+'&dr='+type+'">'+lp._('pokaż wpisy autora')+'</a>');
            else
                $('#sourceInfoAuthorResults').html('<a onclick="closeSourceInfo();page=1;$(\'#filter_input_au\').val(\''+author_name+'\');$(\'#filter_input_as\').val(\''+socialmedia_sites_id+'\');toggleFilter(\'au\', \''+author_name+'\');" href="javascript:void(0);">'+lp._('pokaż wpisy autora')+'</a>');

            //jesli jest cytat, budujemy link do niego i wyswietlamy wraz z cytatem
            if (author_quote)
                $('#sourceInfoQuote').html('<a href="'+result_open_link+'" target="_blank">'+author_quote+'</a>...');
            else
                $('#sourceInfoQuote').html(lp._('Nie znaleziono cytatu'));

            //wyswietlanie avatara
            if (author_avatar_url)
                $('#sourceInfoAuthorAvatarUrl').attr("src",author_avatar_url);


            //wyswietlanie informacji o znajomych
            if(author_friends_count == 0)
                $('#sourceInfoFriendsCount').html('<span style="font-size:18px; font-weight:none;">'+lp._('Brak danych o')+'</span>');
            else
                $('#sourceInfoFriendsCount').html(''+author_friends_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+'+');

            //wyswietlanie informacji o lokalizacji
            if (!city)
                $('#sourceInfoCity').html('');
            else
                $('#sourceInfoCity').html(lp._('Lokalizacja')+': '+city);
        }, "json");
}

function getInfoAboutSocialMediaAuthor(authors_id,pages_id,searches_id)
{
    $('#ajaxLoader').css({'display':'block'});
    $('#ajaxLoader').center();

    $('#overlay').css({'height':$('#site').height()});
    $('#overlay').fadeIn('slow', function() {});

    $.post('/panel/get-info-about-social-media-author/', {authors_id: authors_id,pages_id: pages_id,sid: searches_id},
        function(data)
        {
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var type  = $('#range_type').val();

            var author_friends_count = data.info['friends_count'];
            var author_avatar_url    = data.info['author_avatar_url'];
            var author_quote         = data.info['quote'];
            var author_name          = data.info['author'];
            var socialmedia_sites_id = data.info['socialmedia_sites_id'];
            var author_url           = data.info['url'];

            var mentions_id          = data.info['mentions_id']
            var siteType             = data.info['type'];
            var city                 = data.info['city'];
            var result_open_link	 = data.info['result_open_link'];
            //alert(data);

            $('#source_type_facebook').css({'display':'none'});
            $('#source_type_blip').css({'display':'none'});
            $('#source_type_pinger').css({'display':'none'});
            $('#source_type_twitter').css({'display':'none'});
            $('#source_type_google_plus').css({'display':'none'});
            $('#source_type_instagram').css({'display': 'none'});

            //wyswietlanie informacji o danym serwisie
            if(siteType == "facebook_fanpage")
            {
                $('#source_type_facebook').css({'display':'block'});
            }
            else if(siteType == "google_plus_api")
            {
                $('#source_type_google_plus').css({'display':'block'});
            }
            else
            {
                $('#source_type_'+siteType).css({'display':'block'});
            }



            if (author_avatar_url)
                $('#sourceInfoAuthorAvatarUrl').attr("src",'/static/img/loader.gif');

            //$('#overlay').css({'height':$('#site').height()});
            //$('#overlay').fadeIn('slow', function() {});

            $('#ajaxLoader').css({'display':'none'});

            $('#sourceInfo').css({'display':'block','width':'743px'});
            $('#sourceInfo').center();

            $('#sourceInfoAdPlanner').css({'display':'none'});
            $('#sourceInfoWeb20').css({'display':'block'});

            //wyswietkany nazwe autora i budujemy link prowadzacy do niego w danym serwisie
            if (author_name)
            {
                $('#sourceInfoAuthor').html('<a target="_blank" href="'+author_url+'">'+author_name+'</a>');

                $('#sourceInfoAuthor2').html('<span>'+author_name+'</span>');

                $('#sourceInfoAuthor2Url').attr("href",author_url);
            } else
                $('#sourceInfoAuthor2').html('');

            //$('#sourceInfoAuthorResults').attr("href","/panel/results/?sid="+searches_id+"#t="+data.info['author_name']);

            //budujemy link do wpisow danego autora
            if (tab != 'results')
                $('#sourceInfoAuthorResults').html('<a href="/panel/results/?sid='+searches_id+'#au='+urlEncode(author_name)+'&as='+socialmedia_sites_id+'&d1='+date1+'&d2='+date2+'&dr='+type+'">'+lp._('pokaż wpisy autora')+'</a>');
            else
                $('#sourceInfoAuthorResults').html('<a href="javascript:closeSourceInfo();page=1;$(\'#filter_input_au\').val(\''+author_name+'\');$(\'#filter_input_as\').val(\''+socialmedia_sites_id+'\');toggleFilter(\'au\', \''+author_name+'\');">'+lp._('pokaż wpisy autora')+'</a>');

            //jesli jest cytat, budujemy link do niego i wyswietlamy wraz z cytatem
            if (author_quote)
                $('#sourceInfoQuote').html('<a href="'+result_open_link+'" target="_blank">'+author_quote+'</a>...');
            else
                $('#sourceInfoQuote').html(lp._('Nie znaleziono cytatu'));

            //wyswietlanie avatara
            if (author_avatar_url)
                $('#sourceInfoAuthorAvatarUrl').attr("src",author_avatar_url);


            //wyswietlanie informacji o znajomych
            if(author_friends_count == 0)
                $('#sourceInfoFriendsCount').html(author_friends_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+'+');
            else
                $('#sourceInfoFriendsCount').html(author_friends_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+'+');

            //wyswietlanie informacji o lokalizacji
            if (!city)
                $('#sourceInfoCity').html('');
            else
                $('#sourceInfoCity').html(lp._('Lokalizacja')+': '+city);
        }, "json");
}

function addResultToGroupOpenForm()
{
	if (checkIfUserChooseSomeResults())
	{
		$('#groupsAddListContent').html('');
		$('#groupsAddList').css({'display':'block'});

		if($('#group_info_text'))
		{
			$('#group_info_text').remove();
		}

		var offset = $('#add_to_group_multi').offset();

		$('#groupsAddList').css({'left':(offset.left+50)+'px','top':offset.top+'px'});

		if (groupsAddList.length > 0)
		{
			var cells = new Array();

			for (key in groupsAddList)
			{
				cells.push('<td>' + xssProtect(groupsAddList[key]['title']) + '</td><td>'+'<a href="javascript:addResultToGroupMulti(' + groupsAddList[key]['id'] + ')"><img style="margin-left:5px; margin-right:5px" src="/static/img/btn_add_to_group.png" title="' + lp._('Dodaj wszystkie zaznaczone do tej grupy') + '"></a>' +
							'<a href="javascript:deleteResultsFromGroupMulti(' + groupsAddList[key]['id'] + ')"><img style="margin-right:15px" src="/static/img/btn_delete_from_group.png" title="' + lp._('Usuń wszystkie zaznaczone z tej grupy') + '"></a></td>');
			}

			var temp = '<table>';

			if(groupsAddList.length > 8)
			{
				for (var i = 0; i < groupsAddList.length/3; i++)
				{
					temp += '<tr>' + (typeof cells[i] == "undefined" ? '' : cells[i]) + (typeof cells[Math.ceil(groupsAddList.length/3) + i] == "undefined" ? '' : cells[Math.ceil(groupsAddList.length/3) + i]) + (typeof cells[2*Math.ceil(groupsAddList.length/3) + i] == "undefined" ? '' : cells[2*Math.ceil(groupsAddList.length/3) + i]) + '</tr>';
				}
			}
			else
			{
				for (key in cells)
				{
					temp += '<tr>' + cells[key] + '</tr>';
				}
			}

			temp += '</table>';

			$('#groupsAddList').prepend('<span id="group_info_text" class="group_info_text">' + lp._('Zbiorcze przypisanie wpisów do grup') + '<br /></span>');

			$('#groupsAddListContent').html(temp);

		}
		else
		{
			$('#groupsAddListContent').html(lp._('Nie dodano jeszcze żadnej grupy') + '. <a href="javascript:showGroupsManagementAdd()">' + lp._('Utwórz grupę') + '</a>');
		}
	}
	else
	{
		showInfoToUserAboutNoSelectedResults();
	}
}

function getGroupNameById(id)
{
	var name = '';
	for (key in groupsAddList)
	{
		if (groupsAddList[key]['id'] == id)
		{
			name = groupsAddList[key]['title'];
		}
	}

	return name;
}

function deleteResultFromGroup(resultsId, groupsId)
{
	if ($('.mention-in-group-' + resultsId + '-' + groupsId).hasClass('active'))
	{
		var groupsTitle = getGroupNameById(groupsId);

		$.post(
			'/panel/delete-result-from-group',
			{
				results_id: resultsId,
				groups_id: groupsId,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 1)
				{
					$('.mention_group_added_' + resultsId).each(function( index ){
						if ($(this).attr('data-id') == groupsId)
						{
							$(this).remove();
						}
					});

					if ($('.mention_group_added_' + resultsId).size() == 0)
					{
						$("#groupsListResult_" + resultsId).html('');
					}

					$('.mention-in-group-' + resultsId + '-' + groupsId).removeClass('active');
					$('.mention-in-group-' + resultsId + '-' + groupsId + ' a').attr('href', 'javascript:addResultToGroup(' + resultsId + ', ' + groupsId + ')');
				}
				else
				{
					if (data.error)
					{
						errorMsg = data.error;

					}
					else
					{
						errorMsg = lp._('Błąd podczas usuwania wpisu z grupy');
					}
					showDialogBox(2, errorMsg);
				}
			},
			'json'
		);
	}
}

function addResultToGroup(resultsId, groupsId)
{
	if (!$('.mention-in-group-' + resultsId + '-' + groupsId).hasClass('active'))
	{
		var groupsTitle = getGroupNameById(groupsId);

		$.post(
			'/panel/addresulttogroup',
			{
				results_id: resultsId,
				groups_id: groupsId,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 1)
				{
					$("#groupsListResult_" + resultsId).append(($("#groupsListResult_" + resultsId).html() == '' ? '<i class="fa fa-folder-o"></i> ' + lp._('Grupa')+': ' : '') + '<a href="javascript:toggleFilter(\'gr\',' + groupsId + ')" class="mention_group_added_' + resultsId +' group_added group_added_' + groupsId + '" data-id="' + groupsId + '">' + xssProtect(groupsTitle) + '</a>');
					$("#groupsListResult_" + resultsId).show();

					$('.mention-in-group-' + resultsId + '-' + groupsId).addClass('active');
					$('.mention-in-group-' + resultsId + '-' + groupsId + ' a').attr('href', 'javascript:deleteResultFromGroup(' + resultsId + ', ' + groupsId + ')');
				} 
				else
				{ 
					if (data.error)
					{
						errorMsg = data.error;

					} 
					else 
					{
						errorMsg = lp._('Błąd podczas dodawania wpisu do grupy');
					}
					showDialogBox(2, errorMsg);
				}
			},
			'json'
		);
	}
}

function deleteResultsFromGroupMulti(groupsId)
{
	$('.bulb_checkbox:checked').each(function()
	{
		id = $(this).val();
		deleteResultFromGroup(id, groupsId);
	});
}

function addResultToGroupMulti(groupsId)
{
	$('.bulb_checkbox:checked').each(function()
	{
		id = $(this).val();
		addResultToGroup(id, groupsId);
	});
}

function closeSourceInfo()
{
	$('#sourceInfo').css({'display':'none'});
	$('#overlay').fadeOut('slow', function() {});
}

//pokazuje liste rozwijana z opcjami dla sentymentu

function setSentimentMulti(sentiment)
{
	$('.bulb_checkbox:checked').each(function()
	{
		id = parseInt($(this).val());
		$('#mention-sentiment-' + id).val(sentiment);
		setSentimentInDB(id, sentiment);
	});
}

function setSentiment(id)
{
	if ((checkIfUserChooseSomeResults() && (id == 'multi') || (id != 'multi')))
	{
		$('#sentimentChangeList').css({'display':'block'});
		if (id == 'multi')
		{
			$('#sentimentChangeListContent').html('<a href="javascript:setSentimentMulti(1)" style="color:green;">'+lp._('Pozytywny')+'</a><br /><a href="javascript:setSentimentMulti(0)" style="color:gray;">'+lp._('Neutralny')+'</a><br /><a href="javascript:setSentimentMulti(2)" style="color:red;">'+lp._('Negatywny')+'</a>');
			var offset = $('#sentiment_set_multi').offset();
			$('#sentimentChangeList').css({'left':(offset.left+50)+'px','top':offset.top+'px'});
		}
		else
		{
			$('#sentimentChangeListContent').html('<a href="javascript:setSentimentInDB('+id+',1)" style="color:green;">'+lp._('Pozytywny')+'</a><br /><a href="javascript:setSentimentInDB('+id+',0)" style="color:gray;">'+lp._('Neutralny')+'</a><br /><a href="javascript:setSentimentInDB('+id+',2)" style="color:red;">'+lp._('Negatywny')+'</a>');
		}
	}
	else
	{
		showInfoToUserAboutNoSelectedResults();
	}
}

//wysyla wybrany przez usera sentyment na serwer
function setSentimentInDB(id, sentiment)
{
    $("#mention-sentiment-" + id + " option[value='null']").remove();

	$('#mention-sentiment-' + id).removeClass('positive').removeClass('negative');
	if (sentiment == 1)
	{
		$('#mention-sentiment-'+id).addClass('positive');
	}

	if (sentiment == 2)
	{
		$('#mention-sentiment-' + id).addClass('negative');
	}

	$.post(
		'/panel/setsentiment',
		{
			results_id: id,
			sentiment: sentiment,
			tknB24 : tknB24
		},
		function(data)
		{
			if (data.result == 1)
			{
				return;
			}
			else
			{
				if (data.error)
				{
					errorMsg = data.error;

				}
				else
				{
					errorMsg = lp._('Błąd podczas oznaczania sentymentu wpisu');
				}
				showDialogBox(2, errorMsg);
			}

		},
		"json"
	);
}

function setBulbCheck(id)
{
	if ($('#bulb_checkbox_'+id).attr('checked'))
		$('#bulb_checkbox_'+id).attr('checked', false);
	else
		$('#bulb_checkbox_'+id).attr('checked', true);
}

function detectAndChangeActualResponse(id)
{
	var response = 0;
	if ($('#response_set_'+id).hasClass('mention_option_replied'))
	{
		$('#response_set_'+id).removeClass('mention_option_replied');
		$('#response_set_'+id).addClass('mention_option_replied_ok');
		$('#response_set_'+id+' a span').html(lp._('Odznacz'));
		$('#mention_result_'+id).addClass('mention-responsed');
		response = 1;
	}
	else
	{
		$('#response_set_'+id).removeClass('mention_option_replied_ok');
		$('#response_set_'+id).addClass('mention_option_replied');
		$('#response_set_'+id+' a span').html(lp._('Oznacz'));
		$('#mention_result_'+id).removeClass('mention-responsed');
	}

	return response;
}

function setResponse(id)
{
	var response = detectAndChangeActualResponse(id);

	$.post(
		'/panel/setresponse',
		{
			results_id: id,
			response: response,
			tknB24 : tknB24
		},
		function(data)
		{
			if (data.result == 1)
			{
				return;
			}
			else
			{
				detectAndChangeActualResponse(id);
				if (data.error)
				{
					errorMsg = data.error;

				}
				else
				{
					errorMsg = lp._('Błąd podczas oznaczania wpisu');
				}
				showDialogBox(2, errorMsg);
			}
		},
		"json"
	);
}

function setResponseMulti()
{
	var id = 0;
	var results_ids = '';

	var action = '';
	if($('#response_set_multi a span').html() == lp._('Oznacz'))
	{
		$('#response_set_multi').removeClass('mention_option_replied');
		$('#response_set_multi').addClass('mention_option_replied_ok');
		$('#response_set_multi a span').html(lp._('Odznacz'));
		$('#mention_result_'+id).addClass('mention-responsed');
		action = 'set-response-multi';
	}
	else
	{
		$('#response_set_multi').removeClass('mention_option_replied_ok');
		$('#response_set_multi').addClass('mention_option_replied');
		$('#response_set_multi a span').html(lp._('Oznacz'));
		$('#mention_result_'+id).removeClass('mention-responsed');
		action = 'unset-response-multi';
	}

	if (!checkIfUserChooseSomeResults())
	{
		showInfoToUserAboutNoSelectedResults();
	}
	else
	{
		$('.bulb_checkbox:checked').each(function()
		{
			id = $(this).val();

			if (action == 'unset-response-multi')
			{
				$('#response_set_'+id).removeClass('mention_option_replied_ok');
				$('#response_set_'+id).addClass('mention_option_replied');
				$('#response_set_'+id+' a span').html(lp._('Oznacz'));
				$('#mention_result_'+id).removeClass('mention-responsed');
			}
			else
			{
				$('#response_set_'+id).removeClass('mention_option_replied');
				$('#response_set_'+id).addClass('mention_option_replied_ok');
				$('#response_set_'+id+' a span').html(lp._('Odznacz'));
				$('#mention_result_'+id).addClass('mention-responsed');
			}

			//detectAndChangeActualResponse(id);

			results_ids = results_ids+','+id;
		});

		$.post(
			'/panel/'+action,
			{
				results: results_ids,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 1)
				{
					return;
				}
				else
				{
					if (data.error)
					{
						errorMsg = data.error;

					}
					else
					{
						errorMsg = lp._('Błąd podczas oznaczania wpisu');
					}
					showDialogBox(2, errorMsg);
				}
			},
			"json"
		);
	}
}

function checkIfUserChooseSomeResults()
{
	var length = $('.bulb_checkbox:checked').length;

	if (length == 0)
		return false;
	else
		return true;
}

function showInfoToUserAboutNoSelectedResults()
{
	msg = lp._("Wybierz najpierw grupę wyników na której chcesz przeprowadzić działania");
		buttons = {
			'OK': function(){
				$(this).dialog('close');
			}
		}

	prepareDialogBox(2,msg);
	$("#dialog-window").dialog({
		resizable: false,
		height: 170,
		width: 500,
		modal: true,
		buttons: buttons
	});
}

function chartDataFilter(date1,date2,sid)
{
	if (tab == 'results')
	{
		$('#date1').val(date1);
		$('#date2').val(date2);
		$('#range_type').val('w');
		setDateRangeByType();

		loadTabData();
	} else
	{
		params['d1'] = date1;
		params['d2'] = date2;
    	params['dr'] = 'w';
		window.location.href = '/panel/results/?sid='+sid+'#'+paramsToUrl();
	}
}

function showAllContent (id) {
	$('#mention_text_'+id).css('display', 'none');
	$('#mention_text_oryginal_'+id).css('display', 'inline-block');
}

function hideAllContent (id) {
    $('#mention_text_oryginal_'+id).css('display', 'none');
    $('#mention_text_'+id).css('display', 'inline-block');
}

//Funkcja inicjuje domyslne filtry przy zaladowaniu strony
function useSearchLanguage(elementId,languageWord)
{
	var value = $('#'+elementId).val()+languageWord;
	$('#'+elementId).val(value);
	$('#'+elementId).focus();
}

//Funkcja inicjuje domyslne filtry przy zaladowaniu strony
function isAdditionalFilterSet()
{
	var isSet = false;
	if (params)
	{
		for (var name in params)
		{
			value = params[name];
			if (value != '')
			{
				switch (name)
				{
					case 're':
					case 'vi':
					case 'se':
					{
						if (value == '1' || value == '2')
						{
							isSet = true;
						}
						break;
					}
					case 'ge':
					{
						if (value == '-1' || value == '1')
						{
							isSet = true;
						}
						break;
					}
					case 'ne':
					{
						if (value == '1')
						{
							isSet = true;
						}
						break;
					}
					case 'va':
					{
						if (value == '2' || value == '3')
						{
							isSet = true;
						}
						break;
					}
					case 'rt':
					case 'is':
					case 'te':
					case 'do':
					case 'au':
					case 'as':
					case 'nlc':
					case 'nsc':
					case 'ncc':
					case 'htv':
					case 'gr':
					{
						isSet = true;
						break;
					}
                    case 'ctr':
                    {
                        isSet = true;
                    }
				}
			}
		}
	}
	return isSet;
}

//Funkcja inicjuje domyslne filtry przy zaladowaniu strony
function buttonClearAdditionalFiltersHandler()
{
	if (isAdditionalFilterSet())
	{
		var mentionOrder = params['or'];
		$('.btn-clear-additional-filters').removeClass('btn-nonactive');
		$('.btn-clear-additional-filters').unbind('click');
		$('.btn-clear-additional-filters').click(function() {
			$('.btn-clear-additional-filters').addClass('btn-nonactive');
			unsetAllFilters();
			var paramsUrl = '';
			if (tab=='compare' && params['pc'] && params['pc'] != '')
			{
				paramsUrl += 'pc=' + params['pc'];
				noOfProjects = 0;
				nextProjectId = 0;
				removedProjects = new Array();
				projectsCount = 1;
				projectsToCompare = new Array();
			}
			var new_url = controller_action + '?sid=' + searches_id + '#' + paramsUrl;
			addParamsToMainATags(paramsUrl);
			window.location = new_url;
			$("#edit_my_filter_hidden").val('');
			$("#input_edit_my_filter_name").val('');
			$(".filter_button_mf a").removeClass("filter_on");
			$('.del_filter').css('display', 'none');
			page = 1;
			params['va'] = 1;
			params['or'] = mentionOrder;
			$("#filter_button_va_1 a").addClass("filter_on");
			$('#all_va').css('display', 'table-cell');
			initFilters();
			loadTabData();
		});
	}
	else
	{
		$('.btn-clear-additional-filters').addClass('btn-nonactive');
	}
}

//Funkcja inicjuje domyslne filtry przy zaladowaniu strony
function initFilters()
{
    //jesli jest limit na ilosc dni sprawdzam czy ktoras z dat jest poza zakresem
    isDateRangeAvailable(maximumCalendarLookbackDaysLimit, strToDate($('#date1').val()));

    if (params)
	{
		for (var name in params) {
			value = params[name];
			if(value != '')
			{
				switch(name)
				{
					case 'd1':
					{
						$('#date1').val(value);
						break;
					}
					case 'd2':
					{
						$('#date2').val(value);
						break;
					}
					case 'dr':
					{
						$('#range_type').val(value);
	    				if(value==defaultDateRangeType)
						{
							$('#save_default_date_range').attr('checked','true');
						}
						else
						{
							$('#save_default_date_range').removeAttr('checked');
						}
						break;
					}
	  				case 're':
	  				case 'ge':
	  				case 'vi':
	  				case 'ne':
	  				{
						$(".filter_button_"+name+" a").removeClass("filter_on");
						$("#filter_button_"+name+"_"+value+" a").addClass("filter_on");
						$('#all_'+name).css('display', 'table-cell');
	            		break;
	            	}
	  				case 'va':
	  				{
	  					if(value == '1')
	  					{
							$("#valuablePick").val('2');
						}
						else if(value == '2')
	  					{
							$("#valuablePick").val('3');
						}
						else if(value == '3')
	  					{
							$("#valuablePick").val('1');
						}
	            		break;
	            	}
	  				case 'se':
	  				{
	  					if(value == '1')
	  					{
							$("#sentimentPick").val('3');
						}
						else if(value == '2')
	  					{
							$("#sentimentPick").val('1');
						}
	            		break;
	            	}
			  		case 'or':
			  		{
						if (value == 1)
						{
							$("#sort_found").removeClass("sort_found");
							$("#sort_found").addClass("sort_found_on");
							$("#sort_date").removeClass("sort_date_on");
							$("#sort_date").addClass("sort_date");
						}
						else
						{
							$("#sort_date").removeClass("sort_date");
							$("#sort_date").addClass("sort_date_on");
							$("#sort_found").removeClass("sort_found_on");
							$("#sort_found").addClass("sort_found");
						}
			            break;
			  		}
	  				case 'mf':
	  				{
	  					$("#edit_my_filter_hidden").val(value);
						if(myFilters[value])
							$("#input_edit_my_filter_name").val(myFilters[value]);
						$(".filter_button_"+name+" a").removeClass("filter_on");
						$("#filter_button_"+name+"_"+value+" a").addClass("filter_on");
						$('#all_'+name).css('display', 'table-cell');
	            		break;
	            	}
	  				case 'te':
	  				case 'do':
	  				case 'au':
	  				case 'nor1':
	  				case 'nor2':
	  				case 'fc':
	  				case 'sov':
	  				case 'esr':
	  				case 'htv':
	  				case 'ass':
	  				{
						$("#filter_input_"+name).val(urlDcode(value));
						$('#all_'+name).css('display', 'table-cell');
	            		break;
	            	}
	  				case 'is':
	  				case 'is1':
	  				case 'is2':
	  				{
						$("#filter_input_"+name).val(urlDcode(value));
	            		break;
	            	}
	  				case 'nlc':
	  				case 'nsc':
	  				case 'ncc':
	  				{
						$("#filter_input_"+name).val(urlDcode(value));
						$('#all_'+name).css('display', 'table-cell');
						$('#all_int').css('display', 'table-cell');
	            		break;
	            	}
	            	case 'int':
	            	{
	            		if(params['nsc'] || params['nlc'] || params['ncc'])
	            		{
							$('#all_'+name).css('display', 'table-cell');
						}
	            		break;
	            	}
	  				case 'as':
	  				case 'htvo':
	  				{
						$("#filter_input_"+name).val(value);
	            		break;
	            	}
	            	case 'gr':
	            	{
						var rows = value.split(',');

						if(groups_id == 0)
						{
							groups_id = new Array();
						}
						$.each(rows, function(i, row)
						{
							if(!isNaN(row))
							{
								groups_id.push(parseInt(row));
								$("#panel_filter_group_row_"+row).addClass("filter_on");
							}
						});
						$('#all_'+name).css('display', 'table-cell');
	            		break;
	            	}
	            	case 'pc':
	            	{
	            		if(tab=='compare')
	            		{
							var rows = value.split(',');

							$.each(rows, function(i, row)
							{
								if(!isNaN(row))
								{
									projectsToCompare.push(parseInt(row));

								}
							});
							$('#all_'+name).css('display', 'table-cell');
						}
	            		break;
	            	}
	            	case 'rt':
	            	{
	            		var rows = params['rt'].split(',');
						var name = '';

						if (!rows)
						{
							$('#selected_category_').attr('checked', true);

							$('#results_type_btn_type_').removeClass('results_type_btn').addClass('results_type_btn_on');
						}

						$.each(rows, function(i, row)
						{
							$('#selected_category_'+row).attr('checked', true);

							$('#results_type_btn_type_').removeClass('results_type_btn_on').addClass('results_type_btn');
							$('#results_type_btn_type_'+row).removeClass('results_type_btn').addClass('results_type_btn_on');
						});
						results_tab = params['rt'];
						if(rows.length==1)
							$('#chart_sources_div').hide();
	            		break;
	            	}
	            	case 'p':
	            	{
						page = 1;
	            		break;
	            	}
	            	case 'cdt':
	            	{
						chart_display_type = value;
						$('#chart_main_time_main_days').removeClass('chart_main_time_selected');
						$('#chart_main_time_main_weeks').removeClass('chart_main_time_selected');
						$('#chart_main_time_main_months').removeClass('chart_main_time_selected');
						$('#chart_main_time_main_'+value).addClass('chart_main_time_selected');
	            		break;
	            	}
	            	case 'sa':
	            	{
						if(value == 1)
						{
							$('.hidden_filter').show();
	        				$('#show_results_filters_btn').html(lp._('Ukryj dodatkowe filtry'));
	        			}
	            		break;
	            	}
	            	case 'ssc':
	            	{
	            		if(value == 1)
	            			visibleSentimentChart = true;
	            		else
	            			visibleSentimentChart = false;
	            		break;
	            	}
	            	case 'sis':
	            	{
	            		if(value == 1)
	            			visibleInteractionsSeries = true;
	            		else
	            			visibleInteractionsSeries = false;
	            		break;
	            	}
	            	case 'hmc':
	            	{
	            		if(value == 1 && params['cmt']=='ranges')
	            		{
							mainChartType = 'results';
							$('#chart_main_title_ranges').removeClass('chart_main_title_selected');
							$('#chart_main_title_results').addClass('chart_main_title_selected');
							delete params['cmt'];
						}
	            		break;
	            	}
	            	case 'cmt':
	            	{
	            		mainChartType = value;
						$('#chart_main_title_ranges').removeClass('chart_main_title_selected');
						$('#chart_main_title_results').removeClass('chart_main_title_selected');
						$('#chart_main_title_'+value).addClass('chart_main_title_selected');
	            		break;
	            	}
					case 'ctr':
					{
						if (typeof GeolocationFilter === "object") {
                            var paramArray = params['ctr'].split(',');
                            paramArray.forEach( function(key, index) {
                                GeolocationFilter.addCountryToFilter(CountryUtils.findCountryObjectByIdFromId(countries, paramArray[index]), 1);
                            } );
						}
					}
	            	default:
	            	{
	            		break;
	            	}
				}
			}
			else
			{
				delete params[name];
			}
		}
	}
	if(!params['va'])
	{
		params['va'] = 1;
		$("#valuablePick").val('2');
	}

	if(!params['sa'] && getCookie('showResultsFilters') == '1')
	{
		$('.hidden_filter').show();
	    $('#show_results_filters_btn').html(lp._('Ukryj dodatkowe filtry'));
	}

	if(!$('#sentimentPickSlider a').hasClass('ui-slider-handle'))
	  $('#sentimentPickSlider').slider({
	    range: "min",
	    value: $('#sentimentPick').val(),
	    min: 1,
	    max: 3,
	    slide: function(event, ui) {
	      $('#sentimentPick').val(ui.value);
	    },
	    change: function(event, ui) {
	      changeSentimentPick(ui.value);
	    }
	  });

	  $('#sentimentPick').hide();

	if(!$('#valuablePickSlider a').hasClass('ui-slider-handle'))
	  $('#valuablePickSlider').slider({
	    range: "min",
	    value: $('#valuablePick').val(),
	    min: 1,
	    max: 3,
	    slide: function(event, ui) {
	      $('#valuablePick').val(ui.value);
	    },
	    change: function(event, ui) {
	      changeValuablePick(ui.value);
	    }
	  });

  	$('#valuablePick').hide();

	if(!$('#influenceScoreSlider a').hasClass('ui-slider-handle'))
	  $('#influenceScoreSlider').slider({
	    range: "min",
	    value: $('#filter_input_is').val(),
	    min: 0,
	    max: 10,
	    slide: function(event, ui) {
	      $('#filter_input_is').val(ui.value);
	    },
	    change: function(event, ui) {
	      toggleFilter('is', ui.value);
	    }
	  });

  	$('#filter_input_is').hide();

	if(!$('#influenceScoreSlider1 a').hasClass('ui-slider-handle'))
	  $('#influenceScoreSlider1').slider({
	    range: "min",
	    value: $('#filter_input_is1').val(),
	    min: 0,
	    max: 10,
	    slide: function(event, ui) {
	      $('#filter_input_is1').val(ui.value);
	    },
	    change: function(event, ui) {
	      toggleFilterSourcesAuthors('is1', ui.value);
	    }
	  });

  	$('#filter_input_is1').hide();

	if(!$('#influenceScoreSlider2 a').hasClass('ui-slider-handle'))
	  $('#influenceScoreSlider2').slider({
	    range: "min",
	    value: $('#filter_input_is2').val(),
	    min: 0,
	    max: 10,
	    slide: function(event, ui) {
	      $('#filter_input_is2').val(ui.value);
	    },
	    change: function(event, ui) {
	      toggleFilterSourcesSites('is2', ui.value);
	    }
	  });

  	$('#filter_input_is2').hide();
}

function unsetAllFilters()
{
	if (params)
	{
		for (var name in params) {
			switch (name)
			{
		  		case 're':
		  		case 'ge':
		  		case 'vi':
		  		case 'ne':
		  		{
					$(".filter_button_"+name+" a").removeClass("filter_on");
		            break;
		  		}
		  		case 'va':
		  		{
					$("#valuablePick").val('2');
		            break;
		  		}
		  		case 'se':
		  		{
					$("#sentimentPick").val('2');
		            break;
		  		}
	  			case 'or':
	  			{
					$("#sort_date").removeClass("sort_date");
					$("#sort_date").addClass("sort_date_on");
					$("#sort_found").removeClass("sort_found_on");
					$("#sort_found").addClass("sort_found");
	            	break;
	  			}
		  		case 'gr':
		  		{
					$(".filter_button_"+name+" a").removeClass("filter_on");
					groups_id = [];
		            break;
		  		}
		  		case 'au':
		  		{
		  			$('#filter_input_au').val('');
					$('#filter_input_as').val('');
		            break;
		  		}
		  		case 'htv':
		  		{
		  			$('#filter_input_htv').val('');
					$('#filter_input_htvo').val('');
		            break;
		  		}
		  		case 'do':
		  		case 'te':
	  			case 'nor1':
	  			case 'nor2':
	  			case 'fc':
	  			case 'sov':
	  			case 'esr':
	  			case 'htv':
	  			case 'ass':
	  			case 'nlc':
	  			case 'nsc':
	  			case 'ncc':
		  		{
					$('#filter_input_'+name).val('');
		            break;
		  		}
	  			case 'is':
		  		{
					$('#filter_input_'+name).val('0');
		            break;
		  		}
	  			case 'is1':
		  		{
					$('#filter_input_'+name).val('0');
		            break;
		  		}
	  			case 'is2':
		  		{
					$('#filter_input_'+name).val('0');
		            break;
		  		}
		        case 'rt':
		        {
					$('.selected_category').attr('checked', false);
					$('.results_type_btn_on').removeClass('results_type_btn_on').addClass('results_type_btn');
					$('#results_type_btn_type_').removeClass('results_type_btn').addClass('results_type_btn_on');
		            break;
		        }
		        case 'ssc':
		        {
		        	visibleSentimentChart = false;
		            break;
		        }
		        case 'sis':
		        {
		        	visibleInteractionsSeries = false;
		            break;
		        }
				case 'ctr':
				{
                    params['ctr'] = '';
                    $('.ctr_selected_item').remove();
				}
		  		default:
		  		{
		            break;
		  		}
			}
			if(name != 'pc')
				delete params[name];
		}
	}
	$('#all_'+name).css('display', 'none');

	$('#sentimentPickSlider').slider('destroy');
	$('#valuablePickSlider').slider('destroy');
	$('#influenceScoreSlider').slider('destroy');
	$('#influenceScoreSlider1').slider('destroy');
	$('#influenceScoreSlider2').slider('destroy');
}

//Funkcja wlacza/wylacza okreslony filtr
function toggleFilter(name, value)
{
	if(value != '')
	{
		switch (name)
		{
	  		case 'gr':
	  		{
				if (value != -666)
				{
					$("#filter_button_"+name+"_"+value+" a").addClass("filter_on");
					$('#all_'+name).css('display', 'table-cell');
				}
				else
				{
					$(".filter_button_"+name+" a").removeClass("filter_on");
				}
	  			searchResultsCustomGroup(value);
	  			var temp = '';
				for(var i = 0; i < groups_id.length; i++)
				{
					temp += groups_id[i]+',';
				}
	  			value = temp;
				if(value == '')
				{
					delete params[name];
					$('#all_'+name).css('display', 'none');
				}
	            break;
	  		}
	  		case 're':
	  		case 'ge':
		  	case 'vi':
	  		case 'ne':
	  		{
				if (value != '')
				{
					$(".filter_button_"+name+" a").removeClass("filter_on");
					$("#filter_button_"+name+"_"+value+" a").addClass("filter_on");
					$('#all_'+name).css('display', 'table-cell');
				}
				else
				{
					$(".filter_button_"+name+" a").removeClass("filter_on");
					$('#all_'+name).css('display', 'none');
				}
	            break;
	  		}
	  		case 'va':
	  		{
	  			if(value == '1')
	  			{
					$("#valuablePick").val('2');
				}
				else if(value == '2')
	  			{
					$("#valuablePick").val('3');
				}
				else if(value == '3')
	  			{
					$("#valuablePick").val('1');
				}
	            break;
	         }
	  		case 'se':
	  		{
	  			if(value == '1')
	  			{
					$("#sentimentPick").val('3');
				}
				else if(value == '2')
	  			{
					$("#sentimentPick").val('1');
				}
	            break;
	       }
	  		case 'or':
	  		{
				if (value == 1)
				{
					$("#sort_found").removeClass("sort_found");
					$("#sort_found").addClass("sort_found_on");
					$("#sort_date").removeClass("sort_date_on");
					$("#sort_date").addClass("sort_date");
				}
				else
				{
					$("#sort_date").removeClass("sort_date");
					$("#sort_date").addClass("sort_date_on");
					$("#sort_found").removeClass("sort_found_on");
					$("#sort_found").addClass("sort_found");
				}
	            break;
	  		}
	  		case 'au':
	  		{
	  			value = urlEncode(value);
	  			if($('#filter_input_as').val() != '')
					params['as'] = $('#filter_input_as').val();
				else
					delete params['as'];
				$('#all_'+name).css('display', 'table-cell');
	            break;
	  		}
	  		case 'htv':
	  		{
	  			value = urlEncode(value);
	  			if($('#filter_input_htvo').val() != '')
					params['htvo'] = $('#filter_input_htvo').val();
				else
					delete params['htvo'];
				$('#all_'+name).css('display', 'table-cell');
	            break;
	  		}
	  		case 'do':
	  		case 'te':
	  		case 'nor1':
	  		case 'nor2':
	  		case 'fc':
	  		case 'sov':
	  		case 'esr':
	  		case 'ass':
	  		{
				value = urlEncode(value);
				$('#all_'+name).css('display', 'table-cell');
	            break;
	  		}
	  		case 'is':
	  		case 'is1':
	  		case 'is2':
	  		{
				value = urlEncode(value);
	            break;
	  		}
	  		case 'nlc':
	  		case 'nsc':
	  		case 'ncc':
	  		{
				value = urlEncode(value);
				$('#all_'+name).css('display', 'inline');
				$('#all_int').css('display', 'inline');
				if($('#filter_input_nsc').val() != '')
	            {
	            	params['nsc'] = urlEncode($('#filter_input_nsc').val());
					$('#all_nsc').css('display', 'inline');
				}
				if($('#filter_input_nlc').val() != '')
	           	{
	           		params['nlc'] = urlEncode($('#filter_input_nlc').val());
					$('#all_nlc').css('display', 'inline');
				}
				if($('#filter_input_ncc').val() != '')
	           	{
	           		params['ncc'] = urlEncode($('#filter_input_ncc').val());
					$('#all_ncc').css('display', 'inline');
				}
	            break;
	  		}
	        case 'int':
	        {
	        	if($('#filter_input_nsc').val() != '' || $('#filter_input_nlc').val() != '' || $('#filter_input_ncc').val() != '')
	            {
					$('#all_'+name).css('display', 'inline');
					if($('#filter_input_nsc').val() != '')
	            	{
	            		params['nsc'] = urlEncode($('#filter_input_nsc').val());
						$('#all_nsc').css('display', 'inline');
					}
					if($('#filter_input_nlc').val() != '')
	            	{
	            		params['nlc'] = urlEncode($('#filter_input_nlc').val());
						$('#all_nlc').css('display', 'inline');
					}
					if($('#filter_input_ncc').val() != '')
	            	{
	            		params['ncc'] = urlEncode($('#filter_input_ncc').val());
						$('#all_ncc').css('display', 'inline');
					}
	            }
	            break;
	        }
	        case 'ssc':
	        {
	        	if(value == 1)
	            	visibleSentimentChart = true;
	            else
	            	visibleSentimentChart = false;
	            break;
	        }
	        case 'sis':
	        {
	        	if(value == 1)
	            	visibleInteractionsSeries = true;
	            else
	            	visibleInteractionsSeries = false;
	            break;
	        }
			case 'ctr':
	  		default:
	  		{
	            break;
	  		}
		}
		if(value != ''){
			params[name] = value;
		}
	}
	else
	{
		delete params[name];

		if(name == 'au')
		{
			delete params['as'];
			$('#filter_input_as').val('');
		}

		if(name == 'htv')
		{
			delete params['htvo'];
			$('#filter_input_htvo').val('');
		}

		if(name == 'int')
		{
			$('#filter_input_nsc').val('');
			delete params['nsc'];
			$('#all_nsc').css('display', 'none');
			$('#filter_input_nlc').val('');
			delete params['nlc'];
			$('#all_nlc').css('display', 'none');
			$('#filter_input_ncc').val('');
			delete params['ncc'];
			$('#all_ncc').css('display', 'none');
		}

		if(name == 'nlc' || name == 'nsc' || name == 'ncc')
		{
			$('#filter_input_'+name).val('');
			if($('#filter_input_nsc').val() == '' && $('#filter_input_nlc').val() == '' && $('#filter_input_ncc').val() == '')
			{
				$('#all_int').css('display', 'none');
			}


		}

		if(name == 'te' || name == 'do' || name == 'au' || name == 'is' || name == 'is1' || name == 'is2' || name == 'nor1' || name == 'nor2' || name == 'fc' || name == 'sov' || name == 'esr' || name == 'htv' || name == 'ass')
			$('#filter_input_'+name).val('');

		if(name == 'gr')
			groups_id = [];

		if(name == 'is' || name == 'is1' || name == 'is2')
			$('#filter_input_'+name).val('0');

		if($(".filter_button_"+name+" a"))
			$(".filter_button_"+name+" a").removeClass("filter_on");

		$('#all_'+name).css('display', 'none');
	}

	if(name == 'sa' || name == 'ssc' || name == 'sis' || name == 'hmc' || name == 'cmt' || name == 'voi')
	{
		var paramsUrl = paramsToUrl()+'&p='+page;
		var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;
		addParamsToMainATags(paramsUrl);
		window.location = new_url;
	}
	else
	{
		page = 1;
		updateOtherFilters(name);
		loadTabData();
	}
}

//Funkcja update'uje inne filtry
function updateOtherFilters(newFilter)
{
	if(newFilter != 'te' && $('#filter_input_te').length > 0 && $('#filter_input_te').val() != params['te'])
	{
		if($('#filter_input_te').val() != '')
			params['te'] = urlEncode($('#filter_input_te').val());
		else
			delete	params['te'];
	}
	if(newFilter != 'do' && $('#filter_input_do').val() != params['do'])
	{
		if($('#filter_input_do').val() != '')
			params['do'] = urlEncode($('#filter_input_do').val());
		else
			delete	params['do'];
	}
	if(newFilter != 'au' && $('#filter_input_au').val() != params['au'])
	{
		if($('#filter_input_au').val() != '')
			params['au'] = urlEncode($('#filter_input_au').val());
		else
			delete	params['au'];
	}
	if(newFilter != 'nor1' && $('#filter_input_nor1').val() != params['nor1'])
	{
		if($('#filter_input_nor1').val() != '')
			params['nor1'] = urlEncode($('#filter_input_nor1').val());
		else
			delete	params['nor1'];
	}
	if(newFilter != 'nor2' && $('#filter_input_nor2').val() != params['nor2'])
	{
		if($('#filter_input_nor2').val() != '')
			params['nor2'] = urlEncode($('#filter_input_nor2').val());
		else
			delete	params['nor2'];
	}
	if(newFilter != 'fc' && $('#filter_input_fc').val() != params['fc'])
	{
		if($('#filter_input_fc').val() != '')
			params['fc'] = urlEncode($('#filter_input_fc').val());
		else
			delete	params['fc'];
	}
	if(newFilter != 'esr' && $('#filter_input_esr').val() != params['esr'])
	{
		if($('#filter_input_esr').val() != '')
			params['esr'] = urlEncode($('#filter_input_esr').val());
		else
			delete	params['esr'];
	}
	if(newFilter != 'htv' && $('#filter_input_htv').val() != params['htv'])
	{
		if($('#filter_input_htv').val() != '')
			params['htv'] = urlEncode($('#filter_input_htv').val());
		else
			delete	params['htv'];
	}
	if(newFilter != 'nlc' && $('#filter_input_nlc').val() != params['nlc'])
	{
		if($('#filter_input_nlc').val() != '')
			params['nlc'] = urlEncode($('#filter_input_nlc').val());
		else
			delete	params['nlc'];
	}
	if(newFilter != 'nsc' && $('#filter_input_nsc').val() != params['nsc'])
	{
		if($('#filter_input_nsc').val() != '')
			params['nsc'] = urlEncode($('#filter_input_nsc').val());
		else
			delete	params['nsc'];
	}
	if(newFilter != 'ncc' && $('#filter_input_ncc').val() != params['ncc'])
	{
		if($('#filter_input_ncc').val() != '')
			params['ncc'] = urlEncode($('#filter_input_ncc').val());
		else
			delete	params['ncc'];
	}
}

function getMyFilterNameById(id)
{
	var name = '';
	if (myFilters[id])
	{
		name = myFilters[id];
	}

	return name;
}

function runMyFilter(id, filter)
{
	if(id != -666)
	{
		var title = getMyFilterNameById(id);

		unsetAllFilters();
		var paramsUrl = filter+'&mf='+id+'&p=1';
		if(tab=='compare')
	    {
			noOfProjects = 0;
			nextProjectId = 0;
			removedProjects = new Array();
			projectsCount = 1;
			projectsToCompare = new Array();
			if(params['pc'] && params['pc'] != '')
				paramsUrl += '&pc='+params['pc'];
		}
		var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;
		addParamsToMainATags(paramsUrl);
		window.location = new_url;
		params = parseUrlAfterHashWithUrlDcodeAndEncode();
		$("#edit_my_filter_hidden").val(id);
		$("#input_edit_my_filter_name").val(xssProtect(title));
		$(".filter_button_mf a").removeClass("filter_on");
		$("#filter_button_mf_"+id+" a").addClass("filter_on");
		$('#all_mf').css('display', 'table-cell');
		initFilters();
		loadTabData();
	}
	else
	{
		unsetAllFilters();
		var paramsUrl = '';
		if (tab=='compare' && params['pc'] && params['pc'] != '')
		{
			paramsUrl += 'pc='+params['pc'];
			noOfProjects = 0;
			nextProjectId = 0;
			removedProjects = new Array();
			projectsCount = 1;
			projectsToCompare = new Array();
		}
		var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;
		addParamsToMainATags(paramsUrl);
		window.location = new_url;
		$("#edit_my_filter_hidden").val('');
		$("#input_edit_my_filter_name").val('');
		$(".filter_button_mf a").removeClass("filter_on");
		$('.del_filter').css('display', 'none');
		params['va'] = 1;
		if($('#default_range_type').val() > 0)
			$('#range_type').val($('#default_range_type').val());
		else
			$('#range_type').val('4');
		setDateRangeByType();
		$("#filter_button_va_1 a").addClass("filter_on");
		$('#all_va').css('display', 'table-cell');
		initFilters();
		loadTabData();
	}
}

function showMyFiltersManagementSave()
{
	if(params['mf'] > 0)
	{
		$( "#panel_data_edit_my_filter").dialog( "open" );
	}
	else
	{
		$( "#panel_data_add_my_filter").dialog( "open" );
	}
}

function removeMyFilter(id)
{
	var removeMyFilterButtons = {};
	removeMyFilterButtons[lp._('Nie')] = function(){
		$(this).dialog('close');
	};

	removeMyFilterButtons[lp._('Tak')] = function(){
				showDialogBox(3,null)
				$.post('/panel/delete-my-filter-submit',{ "id": id, "searches_id":searches_id },
					function(data)
					{
						if (data.result == 1) //ok
						{
							//closeDialogBox();
							$("#my_filter_div_"+id).remove();
							showDialogBox(2,lp._('Filtr został usunięty'));
							if($('.filter_button_mf').length == 0)
								$('#panel_filter_my_filters_div').html('<div class="panel_filter_my_filter_row" id="panel_filter_my_filter_row_no_filter">'+lp._('Brak filtrów')+'.</div>');
							if(params['mf'] && params['mf']==id)
							{
								runMyFilter(-666, '');
							}
						}
						else //blad
						{
							if (data.error == 1)
							{
								showDialogBox(2,lp._('Brak uprawnień'));
							}
							else if (data.error == 2)
							{
								showDialogBox(2,lp._('Filtr, który próbujesz usunąć jest wykorzystywany w Powiadomieniu. Aby go usunąć musisz usunąć najpierw Powiadomienie.'));
							}
							else
							{
								showDialogBox(2, lp._('Błąd podczas usuwania'));
							}
						}
					},'json');
			};



	prepareDialogBox(1,lp._("Na pewno chcesz skasować filtr?"));
	$("#dialog-window").dialog({
		resizable: false,
		height: 180,
		modal: true,
		buttons: removeMyFilterButtons
	});

}

function showFiltersPopup()
{
	$( "#filters_popup").dialog( "open" );
}

function removeGroup(id)
{
	var removeGroupButtons = {};
	removeGroupButtons[lp._('Nie')] = function(){
		$(this).dialog('close');
	};
	removeGroupButtons[lp._('Tak')] = function(){
		showDialogBox(3, null);
		$.post(
			'/account/delete-group-submit',
			{
				"id": id,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 1) //ok
				{
					$("#group_div_" + id).remove();
					showDialogBox(2, lp._('Grupa została usunięta'));
					if ($('.filter_button_gr').length == 0)
					{
						$('#panel_filter_groups_div').html('<div class="panel_filter_group_row" id="panel_filter_group_row_no_group">' + lp._('Brak grup') + '.</div>');
					}

					if (tab == 'results')
					{
						for (key in groupsAddList)
						{
							if (groupsAddList[key]['id'] == id)
							{
								groupsAddList.splice(key, 1);
							}
						}

						$('.group_added_' + id).remove();
						$('.groupsListResult').each(function() {
							if ($('#' + this.id + ' .group_added').length == 0)
							{
								$('#' + this.id).html('');
							}
						});

						//dodaję nową grupę do listy przy wyniku
						$(".mention-groups-list").each(function(index){
							$('.mention-in-group-' + $(this).attr('rel') + '-' + id).remove();
						});
					}
				}
				else //blad
				{
					showDialogBox(2, lp._('Błąd podczas usuwania'))
				}
			},
			'json'
		);
	};

	prepareDialogBox(1, lp._("Na pewno chcesz skasować grupę?"));
	$("#dialog-window").dialog({
		resizable: false,
		height: 180,
		modal: true,
		buttons: removeGroupButtons
	});
}

function showGroupsManagementAdd()
{
	$( "#panel_data_add_group").dialog( "open" );
}

function showGroupsManagementEdit(id)
{
	if(id > 0)
	{
		var name = getGroupNameById(id);
		$("#edit_groups_hidden").val(id);
		$("#input_edit_group_name").val(name);
		$("#panel_data_edit_group").dialog( "open" );
	}

}

//Funkcja wlacza/wylacza filtry grup
function searchResultsCustomGroup(gid)
{
	if(groups_id == 0 || gid == -666)
	{
		groups_id = new Array();
		$("#panel_filter_groups_div a").removeClass("filter_on");
	}

	if(gid != -666)
	{
		var key = $.inArray(gid, groups_id);

		if(key != -1)
		{
			groups_id.splice(key,1);
			$("#panel_filter_group_row_"+gid).removeClass("filter_on");
		}else
		{
			groups_id.push(gid);
			$("#panel_filter_group_row_"+gid).addClass("filter_on");
		}
	}
}

function getGooglePlusResults()
{
	page = 1;
	$('#results_search').val('plus.google');
	loadTabData(searches_id);
}

function getWykopResults()
{
	page = 1;
	$('#results_search').val('wykop.pl');
	loadTabData(searches_id);
}

function selectAllResults(checked)
{
	if (checked == 1)
	{
		$('.bulb_checkbox').attr('checked', true);
		$('#select_all_results').html('<a href="javascript:selectAllResults(0)"><i class="fa fa-times-circle"></i> '+lp._('Odznacz wszystkie')+'</a>');
	}
	else
	{
		$('.bulb_checkbox').attr('checked', false);
		$('#select_all_results').html('<a href="javascript:selectAllResults(1)"><i class="fa fa-check-circle"></i> '+lp._('Zaznacz wszystkie')+'</a>');
	}
}

//Funkcja ustawia aktualne url'e, pamietajace filtry dla zakladek
function addParamsToMainATags(paramsUrl)
{
	var paramsUrlRewrite = paramsUrl;
	paramsUrlRewrite = paramsUrlRewrite.replace(new RegExp('&', 'g'),'/');
	paramsUrlRewrite = paramsUrlRewrite.replace(new RegExp('=', 'g'),'/');

	switch (tab)
	{
		case 'results':
			$('#infograph_link').attr('href', '/panel/infograph/sid/' + searches_id + '/' + paramsUrlRewrite + '/');
			$('#action_pdf').attr('action', '/searches/generate-pdf/sid/' + searches_id + '/' + paramsUrlRewrite + '/');
			break;
		case 'analysis':
			$('#more_most_important_authors a').attr('href', '/panel/sources/?sid=' + searches_id + '#' + paramsUrl + '&st=a&or=a_estimated_social_reach_desc');
			$('#more_most_active_authors a').attr('href', '/panel/sources/?sid=' + searches_id + '#' + paramsUrl + '&st=a&or=a_count_desc');
			$('#more_most_active_urls a').attr('href', '/panel/sources/?sid=' + searches_id + '#' + paramsUrl + '&st=s&or=s_count_desc');
			$('#more_most_important_urls a').attr('href', '/panel/sources/?sid=' + searches_id + '#' + paramsUrl + '&st=s&or=s_host_traffic_visits_desc');
			break;
		default:
			break;
	}

	//update in tabs urls only date ranges
	$('#menu_results_btn_' + searches_id).attr("href",'/panel/results/?sid=' + searches_id + '#d1=' + params['d1'] + '&d2=' + params['d2'] + '&dr=' + params['dr']);
	$('#menu_analytics_btn_' + searches_id).attr("href",'/panel/analysis/?sid=' + searches_id + '#d1=' + params['d1'] + '&d2=' + params['d2'] + '&dr=' + params['dr']);
	$('#menu_sources_btn_' + searches_id).attr("href",'/panel/sources/?sid=' + searches_id + '#d1=' + params['d1'] + '&d2=' + params['d2'] + '&dr=' + params['dr']);
	$('#menu_compare_btn_' + searches_id).attr("href",'/panel/compare/?sid=' + searches_id + '#d1=' + params['d1'] + '&d2=' + params['d2'] + '&dr=' + params['dr']);
    $('#menu_summary_btn_' + searches_id).attr("href",'/panel/summary/?sid=' + searches_id + '#d1=' + params['d1'] + '&d2=' + params['d2'] + '&dr=' + params['dr']);
	$('#menu_quotes_btn_' + searches_id).attr("href",'/panel/quotes/sid/' + searches_id + '/date1/' + params['d1'] + '/date2/' + params['d2'] + '/dr/' + params['dr'] + '/');

}

//Funkcja pokazuje/ukrywa glowny wykres
function showMainChart()
{
	if($('#main_charts').is(':visible'))
	{
		$('#chart_main_title_results').hide();
		$('#chart_main_title_ranges').hide();
		$('#show_sentiment_series_btn').hide();
		$('#main_charts').hide();
		$('#show_main_chart').html('<i class="fa fa-eye"></i> ' + lp._('Pokaż wykres'));
		toggleFilter('hmc', 1);
	}
	else
	{
		$('#chart_main_title_results').show();
		$('#chart_main_title_ranges').show();
		$('#show_sentiment_series_btn').show();
		$('#main_charts').show();
		$('#show_main_chart').html('<i class="fa fa-eye-slash"></i> ' + lp._('Ukryj wykres'));
		toggleFilter('hmc', 0);
	}
}

//Funkcja pokazuje/ukrywa wykres sentymentu
var visibleSentimentChart = false;
function showSentimentChart()
{
	if($('#chart_main_sentiment').is(':visible'))
	{
		$('#chart_main_sentiment').hide();
		$('#show_sentiment_series_btn').html('<i class="fa fa-heart-o"></i> ' + lp._('Pokaż sentyment'));
		visibleSentimentChart = false;
		toggleFilter('ssc', 0);
	}
	else
	{
		var width = $('#chart_main').width();
		$('#chart_main_sentiment').show();
		schart.setSize(width, 170);
		$('#show_sentiment_series_btn').html('<i class="fa fa-heart-o"></i> ' + lp._('Ukryj sentyment'));
		visibleSentimentChart = true;
		toggleFilter('ssc', 1);
	}
}

//Funkcja pokazuje/ukrywa serie z sentymentem
var visibleSentimentSeries = false;
function showSentimentSeries()
{
	if(mchart.series[2].visible)
	{
		$('#chart_main').height('150px');
		mchart.setSize($('#chart_main').width(), 150);
		mchart.series[2].hide();
		mchart.series[3].hide();
		$('#show_sentiment_series_btn').html('<i class="fa fa-heart-o"></i> ' + lp._('Pokaż sentyment'));
		visibleSentimentSeries = false;
		toggleFilter('ssc', 0);
	}
	else
	{
		$('#chart_main').height('250px');
		mchart.setSize($('#chart_main').width(), 250);
		mchart.series[2].show();
		mchart.series[3].show();
    $('#show_sentiment_series_btn').html('<i class="fa fa-heart-o"></i> ' + lp._('Ukryj sentyment'));
		visibleSentimentSeries = true;
		toggleFilter('ssc', 1);
	}
}

//Funkcja pokazuje/ukrywa serie z interakcjami
var visibleInteractionsSeries = false;
function showInteractionsSeries()
{
	if(mchart.series[2].visible || mchart.series[3].visible || mchart.series[4].visible)
	{
        mchart.series[0].show();
        mchart.series[1].show();
		mchart.series[2].hide();
		mchart.series[3].hide();
		mchart.series[4].hide();
		$('#show_interactions_series_btn').html('<i class="fa fa-exchange"></i> ' + lp._('Pokaż interakcje'));
		visibleInteractionsSeries = false;
		toggleFilter('sis', 0);
		//clear voi (visible one interaction serie)
        toggleFilter('voi', '');
	}
	else
	{
		mchart.series[2].show();
		mchart.series[3].show();
		mchart.series[4].show();
		$('#show_interactions_series_btn').html('<i class="fa fa-exchange"></i> ' + lp._('Ukryj interakcje'));
		visibleInteractionsSeries = true;
		toggleFilter('sis', 1);
	}
}

//Funkcja zmienia typ wyswietlania wykresu - days weeks months
function setChartDisplayType(ctype)
{
	//czyscimy zazaczenia przycisków
	$('#chart_main_time_main_days').removeClass('chart_main_time_selected');
	$('#chart_main_time_main_weeks').removeClass('chart_main_time_selected');
	$('#chart_main_time_main_months').removeClass('chart_main_time_selected');
	$('#chart_main_time_main_'+ctype).addClass('chart_main_time_selected');
	chart_display_type = ctype;
	searchResultsNew(2);
}

//Funkcja zmienia wykres glowny - wyniki/zasieg Social Media
function switchMainChart(type)
{
	$('.chart_main_title').css('pointer-events','none');
	$('#chart_main_title_ranges').removeClass('chart_main_title_selected');
	$('#chart_main_title_results').removeClass('chart_main_title_selected');
	$('#chart_main_title_'+type).addClass('chart_main_title_selected');
	if(type=='ranges' && mainChartType!='ranges')
	{
		var width = $('#chart_main').width();
		$('#show_sentiment_series_btn').hide();
		$('#chart_main').hide();
		$('#chart_main_ranges').slideDown();
		rchart.setSize(width, 150);
	}
	else if(type=='results' && mainChartType!='results')
	{
		var width = $('#chart_main_ranges').width();
		$('#show_sentiment_series_btn').show();
		$('#chart_main_ranges').hide();
		$('#chart_main').slideDown();
		mchart.setSize(width, 150);
		if(visibleSentimentSeries)
		{
			mchart.series[2].hide();
			mchart.series[3].hide();
		}
		else
		{
			mchart.series[2].show();
			mchart.series[3].show();
		}
		showSentimentSeries();
	}
	mainChartType = type;
	$('.chart_main_title').css('pointer-events','');
	toggleFilter('cmt', type);
}

/**
 * Metoda na podstawie zadanej daty (kolejny punkt na osi X wykresu) generuje sformatowana date do chmurki punktu
 * W zaleznosci od trybu wykresu dla
 * - dziennego - data
 * - tygodniowego - przedzial tygodnia
 * - miesiecznego - miesiac roku
 * @param dateTimestamp
 * @return string
 */
function getChartPointDate(dateTimestamp)
{
	var pointDate = '';

	switch (params['cdt'])
	{
		case 'months':
			pointDate = Highcharts.dateFormat('%B %Y', dateTimestamp);
			break;
		case 'weeks':
			var dateFromTimestamp = dateTimestamp - (1000 * 60 * 60 * 24 * 6);
			var dateToTimestamp = dateTimestamp;

			//wyciagam date do/max zakresu - d1
			var minDate = strtotime(params['d1'] + ' 23:59:59');
			//wyciagam date do/max zakresu - d2
			var maxDate = strtotime(params['d2'] + ' 23:59:59');

			//jesli ostatniego punktu jest pozniejsza niz data max - ustawiam aktualna
			if (dateFromTimestamp < minDate)
			{
				dateFromTimestamp = minDate;
			}

			//jesli ostatniego punktu jest pozniejsza niz data max - ustawiam aktualna
			if (dateToTimestamp > maxDate)
			{
				dateToTimestamp = maxDate;
			}
			pointDate = Highcharts.dateFormat('%Y-%m-%d', dateFromTimestamp) + ' - ' + Highcharts.dateFormat('%Y-%m-%d', dateToTimestamp);
			break;
		default:
			pointDate = Highcharts.dateFormat('%A, %B %e, %Y', dateTimestamp);
			break;

	}
	return pointDate;
}

/**
 * Metoda obsługuje kliknięcie w punkt na wykreie w zakładce wyniki
 * W przypadku podziału miesięcznego ustawia dty od-do dla całego miesiąca
 * @param int timestamp
 */
function resultsChartPointClick(timestamp)
{
	var dFrom = new Date(timestamp);
	var dTo = new Date(timestamp);

	var ffrom = params['d1'];
	var fto = params['d2'];
	var fromArray = ffrom.split('-');
	var toArray = fto.split('-');
	var from = new Date(fromArray[0], fromArray[1]-1,fromArray[2], 0, 0, 0);
	var to = new Date(toArray[0],toArray[1]-1,toArray[2], 23, 59, 59);

	if (chart_display_type == 'months')
	{
		//poczatek miesiaca
		dFrom = new Date(dFrom.getFullYear(), dFrom.getMonth(), 1);
		//ostatni dzien miesiaca
		dTo = new Date(dTo.getFullYear(), dTo.getMonth(), daysInMonth(timestamp));
		//jeśli wyliczona data od jest mniejsza od aktualnej daty od ustawiam aktualną datę od
		if (dFrom < from)
		{
			dFrom = from;
		}
		//jeśli wyliczona data od jest większa od aktualnej daty do ustawiam aktualną datę do
		if (dTo > to)
		{
			dTo = to;
		}
		//zmieniam tryb wykresu na dni
		$('#chart_main_time_main_months').removeClass('chart_main_time_selected');
		$('#chart_main_time_main_days').addClass('chart_main_time_selected');
		chart_display_type = 'days';
	}
	else if (chart_display_type == 'weeks')
	{
		//data od - 6 dni od daty koncowej
		dFrom.setDate(dFrom.getDate() - 6);
		//jeśli wyliczona data od jest mniejsza od aktualnej daty od ustawiam aktualną datę od
		if (dFrom < from)
		{
			dFrom = from;
		}
		//jeśli wyliczona data od jest większa od aktualnej daty do ustawiam aktualną datę do
		if (dTo > to)
		{
			dTo = to;
		}
		//zmieniam tryb wykresu na dni
		$('#chart_main_time_main_weeks').removeClass('chart_main_time_selected');
		$('#chart_main_time_main_days').addClass('chart_main_time_selected');
		chart_display_type = 'days';
	}

	dFrom = dFrom.getFullYear()+'-'+('0'+(dFrom.getMonth()+1)).substr(-2)+'-'+('0'+dFrom.getDate()).substr(-2);
	dTo = dTo.getFullYear()+'-'+('0'+(dTo.getMonth()+1)).substr(-2)+'-'+('0'+dTo.getDate()).substr(-2);

	chartDataFilter(dFrom, dTo, 0);
}

//Funkcja inicjuje ladowanie wynikow w zakladce zrodla
function initSources()
{
	params['d1'] = $('#date1').val();
	params['d2'] = $('#date2').val();
    params['dr'] = $('#range_type').val();
    params['cdt'] = 'days';

	if(params['dr']=='w')
    {
    	$('#date_range_offset a').html(params['d1']+' - '+params['d2']);
    }
    else
	{
    	$('#date_range_offset a').html($('#range_type :selected').text());
    }

	var paramsUrl = paramsToUrl();

	var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;

	addParamsToMainATags(paramsUrl);
	window.location = new_url;

	if (search_on_begin == 1)
	{
		window.location.href = new_url;
		return;
	}

	//if load sites first
	if (params['st'] && params['st'] != 'undefined' && params['st'] == 's')
	{
		showSourcesSites();
	}

	if(tab == 'sources-authors' || tab == 'sources')
		loadSourceAuthors();

	if(tab == 'sources-sites' || tab == 'sources')
	loadSourceSites();
}

//Funkcja ladowanie autorow do zakladki Zrodla
function loadSourceAuthors()
{
	var paramsUrl = paramsToUrl();
	var new_url = controller_action + '?sid=' + searches_id + '#' + paramsUrl;
	addParamsToMainATags(paramsUrl);
	window.location = new_url;
	paramsUrl += '&p=' + page;

	//if we load next more results we remove more button and show loader below the results
	if (page > 1)
	{
		$('#load-more-source-authors-' + (page - 1)).remove();
		$('#source_authors').append('<div id="loader-source-authors" align="center"><img src="/static/img/chart-loader.gif" /></div>');
	}
	else
	{
		$('#source_authors').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
	}

	new_url = new_url.replace('#', '&');
	new_url = new_url.replace(controller_action, '/panel/load-source-authors/');
	new_url = new_url + '&export=excel';

	$('#authors_download').attr('href', 'javascript:' + (export_permission == 0 ? 'showInfoNotAdequateAccountType();' : 'goto(\'http://' + urlEncode(location.host + new_url) + '\');') + '');

	ajaxTable.push($.ajax({
		type: 'POST',
		url: '/panel/load-source-authors/sid/' + searches_id,
		data: paramsUrl,
		dataType: "json",
		success:
			function(data)
			{
				if (data.source_authors)
				{
					var html = '';

					$.each(data.source_authors, function(i, item)
					{
						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td width="4%">' +
								'<div class="sources_author_impact">' + ((20 * (page - 1)) + (i + 1)) + '</div>' +
								'</td>' +
								'<td>' +
								'<table width="100%">' +
								'<tr>' +
								'<td align="left" width="30">';

						if (item['author_avatar_url'])
						{
							html += '<div style="padding:2px;"><a class="no-ajax-abort" id="most_active_authors_avatar_' + item['authors_id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');"><img src="' + item['author_avatar_url'] + '" width="20" height="20" border="0" onerror="imgErrorMostActiveAuthors(\'' + item['authors_id'] + '\')" /></a></div>';
						}
						else
						{
							html += '<div style="padding:2px;"><a class="no-ajax-abort" id="most_active_authors_avatar_' + item['authors_id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');"><img src="/static/img/icons/avatar_default_blue_small.png" width="20" height="20" border="0" onerror="imgErrorMostActiveAuthors(\'' + item['authors_id'] + '\')" /></a></div>';
						}

						html += '</td>' +
								'<td>' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');">' + item['author'] + '</a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</td>' +
								'<td width="5%">' +
								'<a class="no-ajax-abort goto" href="' + item['author_url']+ '" target="_blank">' + lp._('idź do') + '</a>' +
								'</td>' +
								'<td width="8%" align="center" id="service_icon_' + item['authors_id'] + '">' +
								'<img src="' + item['service'] + '" onerror="imgErrorService(' + item['authors_id'] + ')" />' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['count'] + '</div>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">';
								if (item['followers_count'] == '' || item['followers_count'] == '0')
								{
									html += lp._('b/d');
								}
								else
								{
									html += item['followers_count'];
								}
								html +=  '</div>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['share_of_voice'] + '</div>' +
								'</td>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['estimated_social_reach'] + '</div>' +
								'</td>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['influence_score'] + '</div>' +
								'</td>' +
								'<td width="8%" align="center" class="table-options">' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ')"><span class="fa-panel-stack"><i class="fa fa-circle-thin"></i><i class="fa fa-info"></i></span></a> ' +
								'<a class="no-ajax-abort" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id+ ',\'' + item['authors_id']+ '\',\'' + item['blocked_content_kind_of_page'] + '\');"><i class="fa fa-lock"></i></a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});

					//if there are more results show 'more' button
					if (data.more > 0)
					{
						page = parseInt(page);

						html = html + '<table id="load-more-source-authors-' + page + '" width="100%"><tr><td class="panel_pager_btns" style="text-align: center; padding-top: 20px; border-top: solid 1px #e3e3e3;">';
						if (maxTopInfluencers == 1)
						{
							html = html + '<a style="font-size:14px" href="javascript:showInfoNotAdequateAccountType()" class="btn-loadMore">' + lp._('Pokaż więcej') + '</a>';
						}
						else
						{
							html = html + '<a href="javascript:void(0)" onclick="page=' + (page + 1) + ';loadSourceAuthors()" class="btn-loadMore">' + lp._('Pokaż więcej') + '</a>';

						}
						html = html + '</td></tr></table>';
					}

					//if we load next more results we remove loader and append results
					if (page > 1)
					{
						$('#loader-source-authors').remove();
						$('#source_authors').append(html);
					}
					else
					{
						$('#source_authors').html(html);
					}
				}
			},
		error:
			function()
			{
				$('#source_authors').html('<div align="center">' + lp._('Wystąpił błąd podczas pobierania autorów') + ' <a href="javascript:void(0);" onclick="loadSourceAuthors();">' + lp._('Spróbuj ponownie') + '</a></div>');
            }
	}));
}

//Funkcja ladowanie strony do zakladci Zrodla
function loadSourceSites()
{
	var paramsUrl = paramsToUrl();
	var new_url = controller_action + '?sid=' + searches_id + '#' + paramsUrl;
	addParamsToMainATags(paramsUrl);
	window.location = new_url;
	paramsUrl += '&p=' + page;

	//if we load next more results we remove more button and show loader below the results
	if (page > 1)
	{
		$('#load-more-source-sites-' + (page - 1)).remove();
		$('#source_sites').append('<div id="loader-source-sites" align="center"><img src="/static/img/chart-loader.gif" /></div>');
	}
	else
	{
		$('#source_sites').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
	}

	new_url = new_url.replace('#', '&');
	new_url = new_url.replace(controller_action, '/panel/load-source-sites/');
	new_url = new_url + '&export=excel';

	$('#sites_download').attr('href', 'javascript:'+(export_permission == 0 ? 'showInfoNotAdequateAccountType();' : 'goto(\'http://'+urlEncode(location.host+new_url)+'\');')+'');


	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-source-sites/sid/' + searches_id,
		data: paramsUrl,
		dataType: "json",
		success:
			function(data)
			{
				if(data.source_sites)
				{
					var html = '';
					var date1 = $('#date1').val();
					var date2 = $('#date2').val();
					var type = $('#range_type').val();

					$.each(data.source_sites, function(i, item)
					{
						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td width="4%">' +
								'<div class="sources_author_impact">' + ((20 * (page - 1)) + (i + 1)) + '</div>' +
								'</td>' +
								'<td>' +
								'<a href="/panel/results/?sid='+searches_id+'#do=' + urlEncode(item['host']) + '&d1=' + date1 + '&d2=' + date2 + '&dr=' + type + '">' + item['host'] + '</a>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['count'] + '</div>' +
								'</td>' +
								'<td width="25%" align="center">' +
								'<div class="sources_author_impact">' + item['host_traffic_visits'] + '</div>' +
								'</td>' +
								'</td>' +
								'<td width="10%" align="center">' +
								'<div class="sources_author_impact">' + item['influence_score'] + '</div>' +
								'</td>' +
								'<td width="8%" align="center" class="table-options">' +
								'<a class="no-ajax-abort" title="' + lp._('Przejdź do strony') + '" target="_blank" href="http://' + item['host'] + '"><span class="fa-panel-stack"><i class="fa fa-sign-out"></i></span></a> ' +
								'<a class="no-ajax-abort" title="' + lp._('Zablokuj źródło') + '" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id + ',\'' + item['host']+ '\',\'' + item['blocked_content_kind_of_page'] + '\');"><i class="fa fa-lock"></i></a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});

					//if there are more results show 'more' button
					if (data.more > 0)
					{
						page = parseInt(page);

						html = html + '<table id="load-more-source-sites-' + page + '" width="100%"><tr><td class="panel_pager_btns" style="text-align: center; padding-top: 20px; border-top: solid 1px #e3e3e3;">';
						if (maxTopInfluencers == 1)
						{
							html = html + '<a style="font-size:14px" href="javascript:showInfoNotAdequateAccountType()" class="btn-loadMore">' + lp._('Pokaż więcej') + '</a>';
						}
						else
						{
							html = html + '<a href="javascript:void(0)" onclick="page=' + (page + 1) + ';loadSourceSites()" class="btn-loadMore">' + lp._('Pokaż więcej') + '</a>';

						}
						html = html + '</td></tr></table>';
					}

					//if we load next more results we remove loader and append results
					if (page > 1)
					{
						$('#loader-source-sites').remove();
						$('#source_sites').append(html);
					}
					else
					{
						$('#source_sites').html(html);
					}
				}
			},
		error:
			function()
			{
				$('#source_sites').html('<div align="center">' + lp._('Wystąpił błąd podczas pobierania stron') + ' <a href="javascript:void(0);" onclick="loadSourceSites();">' + lp._('Spróbuj ponownie') + '</a></div>');
			}
	}));
}

/**
 * Metoda poprawia wysokosc boxa ze statystykami na podstawie widocznego elementu po lewej stronie
 * @param element
 */
function fixHeightOfAnalysisStats(element)
{
	fixHeightOfTwoElements(element, $('#analysis_stats'))
	fixAnalysisStats();
}

/**
 * Metoda poprawia polozenie boxa ze statystykami w zaleznosci od tego jak zescrollowana jest strona
 */
function fixAnalysisStats()
{
    var scrollStart = $('#analysis_stats').offset().top;
	var scrollStop = $('#analysis_stats').height() - $(window).height();
	var scrollTop = $(document).scrollTop();
	if (scrollStop > 0)
	{
		if (scrollTop > scrollStart && scrollTop < scrollStop)
		{
			$('.analysis-stats').attr('style', 'position:fixed; top:20px; margin-right: 18px;');
		}
		else
		{
			if (scrollTop > scrollStop)
			{
				$('.analysis-stats').attr('style', 'position: absolute; bottom:-61px; margin-right: 0px;');
			}
			if (scrollTop < scrollStart && scrollTop < scrollStop)
			{
				$('.analysis-stats').attr('style', 'position: relative;');
			}
		}
	}
}

//Funkcja inicjuje ladowanie wynikow w zakladce analiza
function initAnalysis()
{
	params['d1'] = $('#date1').val();
	params['d2'] = $('#date2').val();
    params['dr'] = $('#range_type').val();
    params['cdt'] = 'days';

	if(params['dr']=='w')
    {
    	$('#date_range_offset a').html(params['d1']+' - '+params['d2']);
    }
    else
	{
    	$('#date_range_offset a').html($('#range_type :selected').text());
    }

	var paramsUrl = paramsToUrl();

	var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;

	addParamsToMainATags(paramsUrl);
	window.location = new_url;

	if (search_on_begin == 1)
	{
		window.location.href = new_url;
		return;
	}

    //if load entries from most popular authors first
    if (params['at'] && params['at'] != 'undefined' && params['at'] == 'efp')
    {
    	showPopularAuthors();
    }

	if (activeInteractions == 1 && accessInteractions == 1)
	{
		loadAnalysisMostInteractiveEntriesFromSocialMedia();
	}

	loadAnalysisEntriesFromMostPopularAuthors();
	loadAnalysisStatistics();
	loadAnalysisChartSources();
	loadAnalysisMostImportantAuthors();
	loadAnalysisMostActiveAuthors();
	loadAnalysisMostActiveUrls();
	loadAnalysisMostImportantUrls();
	loadHashtagsAnalytics();

    if (typeof loadAnalysisMostActiveCountries === "function")
    {
        loadAnalysisMostActiveCountries();
    }

	//wczytujemy tylko raz zaraz po utworzeniu projektu gdy są odświeżanie po utworzeniu projektu
	if (canLoadMostPopularWords == 1 && ((loadTabDataTimeout == null && projectHasJustAdded == true) || projectHasJustAdded == false))
	{
		loadMostPopularWords();
	}

    loadLinksAnalytics();
}

//ilosc wszystkich wpisow
var nOfAllAnalysisMostInteractiveEntriesFromSocialMedia = 0;
//ilosc wpisow i o ile kolejno bedziemy dociagac/odslaniac
var nOfVisibleAnalysisMostInteractiveEntriesFromSocialMediaPerPage = 0;
// aktualnie widoczna strona wpisow
var currentPageAnalysisMostInteractiveEntriesFromSocialMedia = 1;
//ilosc stron wpisow
var noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia = 0;

function resolveHashtagParam(hashtag)
{
    return hashtag ? '/htg/' + hashtag :  '';
}

//Funkcja ladowanie najbardziej interaktywne wpisów w zakladce Analiza
function loadAnalysisMostInteractiveEntriesFromSocialMedia(hashtag)
{
	nOfAllAnalysisMostInteractiveEntriesFromSocialMedia = 0;
	var paramsUrl = paramsToUrl();

	var firstTimeLoaderInfo = '';
	//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery
	if (projectHasJustAdded)
	{
		firstTimeLoaderInfo = '<div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div>';
	}

	//nie wyswietlam loaderka gdy wyniki sa odswiezane cyklicznie po dodaniu wpisu
	if ((firstLoadOfAnalysisMostInteractiveEntriesFromSocialMedia && projectHasJustAdded == true) || projectHasJustAdded ==false)
	{
		$('#most_interactive_entries_from_social_media').html('<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza najpopularniejszych wzmianek') + '.</div>' + firstTimeLoaderInfo + '</div>');
		firstLoadOfAnalysisMostInteractiveEntriesFromSocialMedia = false;
	}

	mostInteractiveEntriesFromSocialMediaLoaded = false;
	ajaxTable.push($.ajax({
		type: 'POST',
		url: '/panel/load-most-interactive-entries-from-social-media/sid/' + searches_id + resolveHashtagParam(hashtag),
		data: paramsUrl,
		dataType: "json",
		success:
			function(data)
			{
				if (data.most_interactive_entries_from_social_media.length > 0)
				{
					nOfAllAnalysisMostInteractiveEntriesFromSocialMedia = data.most_interactive_entries_from_social_media.length;
					nOfVisibleAnalysisMostInteractiveEntriesFromSocialMediaPerPage = data.entriesLimitPerPage;

					var html = '';
					var href = '';
					var results_sentiment = '';
					var result_stats = '';
					var influence_score = '';

					if (data.isFullList == '0')
					{
						html += '<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza najpopularniejszych wzmianek') + '.</div><div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div></div>';
					}

					$.each(data.most_interactive_entries_from_social_media, function(i, item)
					{
						href = '';
						results_sentiment = '';

						if (item['cant_see'])
						{
							href = '/account/upgrade';
							mention_restricted = 'mention_restricted';
						}
						else
						{
							href = item['result_open_link'];
							mention_restricted = '';
						}

						if (item['results_sentiment'] == 0)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-neu" id="sentiment_icon_' + item['id'] + '">' + lp._('neutralny') + '</span>';
						}
						else if (item['results_sentiment'] == 1)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-pos" id="sentiment_icon_' + item['id'] + '">' + lp._('pozytywny') + '</span>';
						}
						else if (item['results_sentiment'] == 2)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-neg" id="sentiment_icon_' + item['id'] + '">' + lp._('negatywny') + '</span>';
						}

						result_stats = '';
						if ((activeInteractions == 1 && accessInteractions == 1) && ((item['shares_count'] != null) || (item['likes_count'] != null) || (item['dislikes_count'] != null) || (item['comments_count'] != null)/* || item['host_traffic_visits'] != null*/))
						{
							var result_stats_likes_icon = '';
							var result_stats_dislikes_icon = '<i class="fa fa-thumbs-o-down"></i>';
							if (item['socialmedia_sites_id'] == 1 || isMentionFromYoutube(item)) //facebook & youtube
							{
								result_stats_likes_icon = '<i class="fa fa-thumbs-o-up"></i>';
							}
							else if (item['socialmedia_sites_id'] == 5)
							{
								result_stats_likes_icon = '<i class="fa fa-heart"></i>';
							}
							else
							{
								result_stats_likes_icon = '<i class="fa fa-star"></i>';
							}

							if (item['likes_count'] != null && item['likes_count'] >= 0)
							{
								result_stats = result_stats + result_stats_likes_icon + ' ' + item['likes_count'];
							}
							else
							{
								//gdy interakcje sa niemozliwe do sparsowania
								if (item['likes_count'] != null && item['likes_count'] == -1)
								{
									result_stats = result_stats;
								}
								else
								{
									result_stats = result_stats + result_stats_likes_icon + ' 0';
								}
							}

							if (isMentionFromYoutube(item) && !isYoutubeComment(item)) //youtube
							{
								if (item['dislikes_count'] != null && item['dislikes_count'] >= 0)
								{
									result_stats = result_stats + result_stats_dislikes_icon + ' ' + item['dislikes_count'];
								}
								else
								{
									//gdy interakcje sa niemozliwe do sparsowania
									if (item['dislikes_count'] != null && item['dislikes_count'] == -1)
									{
										result_stats = result_stats;
									}
									else
									{
										result_stats = result_stats + result_stats_dislikes_icon + ' 0';
									}
								}
							}

							if (item['host'] != null && item['host'] != 'twitter.com' && !isYoutubeComment(item)) //dla twittera nie zbieramy komentarzy, a komentarze na youtube maja tylko ilosc polubien
							{
								var result_stats_comments_icon = '';
								if (item['socialmedia_sites_id'] == 1)
								{
									result_stats_comments_icon = '<i class="icon-comment"></i>';
								}
								else if (item['socialmedia_sites_id'] == 5)
								{
									result_stats_comments_icon = '<i class="fa fa-comment"></i>';
								}
								else
								{
									result_stats_comments_icon = '<i class="fa fa-mail-reply"></i>';
								}

								if (item['comments_count'] != null && item['comments_count'] >= 0)
								{
									result_stats = result_stats + result_stats_comments_icon + ' ' + item['comments_count'];
								}
								else
								{
									//gdy interakcje sa niemozliwe do sparsowania
									if (item['comments_count'] != null && item['comments_count'] == -1)
									{
										result_stats = result_stats;
									}
									else
									{
										result_stats = result_stats + result_stats_comments_icon + ' 0';
									}
								}
							}

							var result_stats_shares_icon = '';
							if (item['socialmedia_sites_id'] == 1)
							{
								result_stats_shares_icon = '<i class="icon-export"></i>';
							}
							else if (item['socialmedia_sites_id'] == 5 || isMentionFromYoutube(item))
							{
								result_stats_shares_icon = '';
							}
							else
							{
								result_stats_shares_icon = '<i class="fa fa-retweet"></i> ';
							}

							if (item['shares_count'] != null && item['shares_count'] >= 0)
							{
								result_stats = result_stats + result_stats_shares_icon + ' ' + item['shares_count'];
							}
							else
							{
								if (item['socialmedia_sites_id'] == 5 || isMentionFromYoutube(item))
								{
									result_stats = result_stats;
								}
								else
								{
									//gdy interakcje sa niemozliwe do sparsowania
									if (item['shares_count'] != null && item['shares_count'] == -1)
									{
										result_stats = result_stats;
									}
									else
									{
										result_stats = result_stats + result_stats_shares_icon + ' 0';
									}
								}
							}
						}

						influence_score = '';
						if (item['influence_value'] != null && item['influence_score'] != '')
						{
							influence_score = influence_score + '<div class="influence-score"><span style="width:' + item['influence_score'] + '0%"></span></div>' +
																'<span>' + lp._('Influence Score') + ': ' + item['influence_score'] + '/10</span>';
						}

						html += '<div class="mention most-interactive-entry-from-social-media';
						//ukryj wiecej niz 10
						if (i > (nOfVisibleAnalysisMostInteractiveEntriesFromSocialMediaPerPage - 1))
						{
							html += ' hidden';
						}
						html += '">' +
						'<table width="100%" cellpadding="0" cellspacing="0">' +
						'<tr>' +
						'<td width="74" valign="top">' +
						'<div class="mention_avatar">' +
						'<div class="sources_author_impact">' + (i + 1) + '</div>' +
						'<a class="no-ajax-abort" id="most_interactive_entries_from_social_media_avatar_' + item['id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');">' +
						'<img src="' + item['author_avatar_url'] + '" width="54" height="54" class="img-avatar" onerror="imgErrorMostInteractiveEntriesFromSocialMedia(' + item['id'] + ')">' +
						'</a>' +
						'</div>' +
						'</td>' +
						'<td>' +
						'<div class="mention_title">' +
						'<table cellpadding="0" cellspacing="0" width="100%">' +
						'<tr>' +
						'<td valign="middle">' +
						'<a class="no-ajax-abort" href="' + href + '" target="_blank">' +
						item['title'] +
						'</a>'+
						'</td>' +
						'<td width="170" valign="middle" class="mention-title-options">' +
						'<table cellpadding="0" cellspacing="0" width="100%">' +
						'<tr>' +
						'<td valign="middle" align="right" style="width: auto">' +
						results_sentiment +
						'</td>' +
						'<td id="results_favicon_' + item['id'] + '" class="results_favicon_' + item['page_category'] + '" valign="middle" align="right" width="16" height="14" style="overflow:hidden">' +
						item['favicon_html'] +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>' +
						'<div class="mention_text">' +
						item['content']  +
						'</div>' +
						'<div class="mention_meta">' +
						'<table width="100%" cellspacing="0" cellpadding="0">' +
						'<tr>' +
						'<td width="100">' +
						'<span class="mention_source">' +
						'<a href="' + href + '" target="_blank" hidden_source_url="' + item['author_url'] + '">' + item['host'] + '</a>' +
						'</span>' +
						'<span class="mention_date">' + item['created_date'] + '</span>' +
						'</td>' +
						'<td width="160" style="font-size: 10px; padding-right: 20px;">' +
						influence_score +
						'</td>' +
						'<td align="right" class="analysis-interactions">' +
						'<div>' +
						result_stats +
						'</div>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>';
					});

					noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia = Math.ceil(nOfAllAnalysisMostInteractiveEntriesFromSocialMedia / nOfVisibleAnalysisMostInteractiveEntriesFromSocialMediaPerPage);

					//jesli wiecej niz jedna strona wynikow pokaz paginacje
					if (noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia > 1)
					{
						html = html + '<div class="clearfix"></div><div class="panel_pager" style="margin-bottom: 0;"><table width="100%"><tr>';

						if (nOfAllAnalysisMostInteractiveEntriesFromSocialMedia == 100)
						{
							html = html + '<td class="bulk_meta">' + lp._('Top 100 najbardziej interaktywnych wpisów') + '</td>';
						}

						html = html + '<td class="panel_pager_btns" align="right">';

						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisMostInteractiveEntriesFromSocialMedia(1);" class="analysis-most-interactive-entries-from-social-media-page-first hidden">' + lp._('Pierwsza') + '</a>';

						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisMostInteractiveEntriesFromSocialMedia(-1);" class="analysis-most-interactive-entries-from-social-media-page-prev hidden">' + lp._('Poprzednia') + '</a>';

						for (i=1; i<=noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia; i++)
						{
							html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisMostInteractiveEntriesFromSocialMedia(' + i + ');" class="analysis-most-interactive-entries-from-social-media-page analysis-most-interactive-entries-from-social-media-page-no analysis-most-interactive-entries-from-social-media-page-' + i + ''+ (i == 1 ? ' selected_page' : '') +  '' + (i > 3 ? ' hidden' : '') + '">' + i + '</a>';
						}

						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisMostInteractiveEntriesFromSocialMedia(100);" class="analysis-most-interactive-entries-from-social-media-page-next">' + lp._('Następna') + '</a>';


						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisMostInteractiveEntriesFromSocialMedia(' + noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia + ')" class="analysis-most-interactive-entries-from-social-media-page-last">' + lp._('Ostatnia') + ' [' + noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia + ']</a>';

						html = html + '</td></tr></table></div>';
					}

					$('#most_interactive_entries_from_social_media').html(html);
					$('.most-interactive-entry-from-social-media').hide();
					$('.most-interactive-entry-from-social-media').not('.hidden').fadeIn(1500);
					$(".help_qtip_ajax").qtip();
                    //move to sources chart
                    if (params['at'] && params['at'] != 'undefined' && params['at'] == 'sc')
                    {
                        $(document).scrollTop( $("#sources-chart-anchor").offset().top );
                    }
                }
				else
				{
					//jesli wczytujemy pierwszy raz a w zakladce najbardziej interaktywnych wpisow brak wpisow oraz zakladka wpisów od najpopularniejszych autorów juz sie wczytala i istnieja w niej wpisy wtedy ja otwieramy
					if (firstLoadOfAnalysisTab && entriesFromMostPopularAuthorsLoaded && $('#entries_from_most_popular_authors .mention').length)
					{
						showPopularAuthors();
					}
					//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery
					if (projectHasJustAdded)
					{
						$('#entries_from_most_popular_authors').html('<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza najpopularniejszych wzmianek') + '.</div><div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div></div>');
					}
					else
					{
						$('#most_interactive_entries_from_social_media').html('<div class="loading-entries panel_main" align="center"><div class="no-entries">' + lp._('Tymczasowy brak wyników w Social Media.') + '.</div></div>');
					}
				}
				mostInteractiveEntriesFromSocialMediaLoaded = true;
				//jesli obie zakladki juz sie zaladowaly
				if (entriesFromMostPopularAuthorsLoaded)
				{
					firstLoadOfAnalysisTab = false;
				}

				if ($('.showMostInteractiveTab').hasClass('active'))
				{
					fixHeightOfAnalysisStats($('#most_interactive_entries_from_social_media'));
				}
			},
		error:
			function()
			{
				$('#most_interactive_entries_from_social_media').html('<div class="loading-entries panel_main" align="center"><div class="no-entries">' + lp._('Wystąpił błąd podczas pobierania najpopularniejszych wzmianek') + ' <a href="javascript:void(0);" onclick="loadAnalysisMostInteractiveEntriesFromSocialMedia();">' + lp._('Spróbuj ponownie') + '</a></div></div>');
			}
	}));
}

//Funkcja do obslugi paginacji najbardziej interaktywnych wpisów w zakladce Analiza
function goToPageAnalysisMostInteractiveEntriesFromSocialMedia(pageNumber)
{
	currentPageAnalysisMostInteractiveEntriesFromSocialMedia = paginationControler(pageNumber, currentPageAnalysisMostInteractiveEntriesFromSocialMedia, noOfPagesAnalysisMostInteractiveEntriesFromSocialMedia, nOfVisibleAnalysisMostInteractiveEntriesFromSocialMediaPerPage, '.analysis-most-interactive-entries-from-social-media-page', '.most-interactive-entry-from-social-media');
	fixHeightOfAnalysisStats($('#most_interactive_entries_from_social_media'));
}

//ilosc wszystkich wpisow
var nOfAllAnalysisEntriesFromMostPopularAuthors = 0;
//ilosc wpisow i o ile kolejno bedziemy dociagac/odslaniac
var nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage = 0;
// aktualnie widoczna strona wpisow
var currentPageAnalysisEntriesFromMostPopularAuthors = 1;
//ilosc stron wpisow
var noOfPagesAnalysisEntriesFromMostPopularAuthors = 0;

//Funkcja ladowanie wpisów od najpopularniejszych autorów w zakladce Analiza
function loadAnalysisEntriesFromMostPopularAuthors(hashtag)
{
	nOfAllAnalysisEntriesFromMostPopularAuthors = 0;
	nOfVisibleAnalysisEntriesFromMostPopularAuthors = 0;

	var paramsUrl = paramsToUrl();

	var firstTimeLoaderInfo = '';
	//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery
	if (projectHasJustAdded)
	{
		firstTimeLoaderInfo = '<div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div>';
	}

	//nie wyswietlam loaderka gdy wyniki sa odswiezane cyklicznie po dodaniu wpisu
	if ((firstLoadOfAnalysisEntriesFromMostPopularAuthors && projectHasJustAdded == true) || projectHasJustAdded == false)
	{
		$('#entries_from_most_popular_authors').html('<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza wpisów od najpopularniejszych autorów') + '.</div>' + firstTimeLoaderInfo + '</div>');
		firstLoadOfAnalysisEntriesFromMostPopularAuthors = false;
	}

	entriesFromMostPopularAuthorsLoaded = false;
	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-entries-from-most-popular-authors/sid/' + searches_id + resolveHashtagParam(hashtag),
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.entries_from_most_popular_authors)
				{
					nOfAllAnalysisEntriesFromMostPopularAuthors = data.entries_from_most_popular_authors.length;
					nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage = data.entriesLimitPerPage;

					var html = '';
					var href = '';
					var results_sentiment = '';
					var result_stats = '';
					var influence_score = '';

					if (data.isFullList == '0')
					{
						html += '<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza wpisów od najpopularniejszych autorów') + '.</div><div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div></div>';
					}

					$.each(data.entries_from_most_popular_authors, function(i, item)
					{
						href = '';
						if (item['cant_see'])
						{
							href = '/account/upgrade';
							mention_restricted = 'mention_restricted';
						}
						else
						{
							href = item['result_open_link'];
							mention_restricted = '';
						}

						if (item['results_sentiment'] == 0)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-neu" id="sentiment_icon_' + item['id'] + '">' + lp._('neutralny') + '</span>';
						}
						else if (item['results_sentiment'] == 1)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-pos" id="sentiment_icon_' + item['id'] + '">' + lp._('pozytywny') + '</span>';
						}
						else if (item['results_sentiment'] == 2)
						{
							results_sentiment = '<span class="sentiment_status sentiment_status-neg" id="sentiment_icon_' + item['id'] + '">' + lp._('negatywny') + '</span>';
						}

						influence_score = '';
						if (item['influence_value'] != null && item['influence_score'] != '')
						{
							influence_score = influence_score + '<div class="influence-score"><span style="width:' + item['influence_score'] + '0%"></span></div>'+
																'<span>' + lp._('Influence Score') + ': '+item['influence_score'] +'/10</span>';
						}

						html += '<div class="mention entry-from-most-popular-authors';
						//ukryj wiecej niz 10
						if (i > (nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage - 1))
						{
							html += ' hidden';
						}
						html += '">' +
						'<table width="100%" cellpadding="0" cellspacing="0">' +
						'<tr>' +
						'<td width="74" valign="top">' +
						'<div class="mention_avatar">'+
						'<div class="sources_author_impact">' + (i + 1) + '</div>' +
						'<a class="no-ajax-abort" id="entries_from_most_popular_authors_avatar_' + item['id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');">' +
						'<img src="' + item['author_avatar_url'] + '" width="54" height="54" class="img-avatar" onerror="imgErrorEntriesFromMostPopularAuthors(' + item['id'] + ')">' +
						'</a>' +
						'</div>' +
						'</td>' +
						'<td>' +
						'<div class="mention_title">' +
						'<table cellpadding="0" cellspacing="0" width="100%">' +
						'<tr>' +
						'<td valign="middle">' +
						'<a class="no-ajax-abort" href="' + href + '" target="_blank">' +
						item['title'] +
						'</a>'+
						'</td>' +
						'<td width="170" valign="middle" class="mention-title-options">' +
						'<table cellpadding="0" cellspacing="0" width="100%">' +
						'<tr>' +
						'<td valign="middle" align="right" style="width: auto">' +
						results_sentiment +
						'</td>' +
						'<td id="results_favicon_' + item['id'] + '" class="results_favicon_' + item['page_category'] + '" valign="middle" align="right" width="16" height="14" style="overflow:hidden">' +
						item['favicon_html'] +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>' +
						'<div class="mention_text">' +
						item['content']  +
						'</div>' +
						'<div class="mention_meta">' +
						'<table width="100%" cellspacing="0" cellpadding="0">' +
						'<tr>' +
						'<td width="100">' +
						'<span class="mention_source">' +
						'<a href="' + href + '" target="_blank" hidden_source_url="' + item['author_url'] + '">' + item['host'] + '</a>' +
						'</span>' +
						'<span class="mention_date">' + item['created_date'] + '</span>' +
						'</td>' +
						'<td width="160" style="font-size: 10px; padding-right: 20px;">' +
						influence_score +
						'</td>' +
						'<td align="right" class="analysis-interactions">' +
						'<div>' +
						lp._('Wpływ') +
						'<br><strong>' +
						item['estimated_social_reach'] +
						'</strong>' +
						'</div>' +
						'<div>' +
						lp._('Dotarcie') +
						'<br><strong>' +
						item['followers_count'] +
						'</strong>' +
						'</div>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>' +
						'</td>' +
						'</tr>' +
						'</table>' +
						'</div>';
					});

					noOfPagesAnalysisEntriesFromMostPopularAuthors = Math.ceil(nOfAllAnalysisEntriesFromMostPopularAuthors / nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage);

					//jesli wiecej niz jedna strona wynikow pokaz paginacje
					if (noOfPagesAnalysisEntriesFromMostPopularAuthors > 1)
					{
						html = html + '<div class="clearfix"></div><div class="panel_pager" style="margin-bottom: 0;"><table width="100%"><tr>';
						if (nOfAllAnalysisEntriesFromMostPopularAuthors == 100)
						{
							html = html + '<td class="bulk_meta">' + lp._('Top 100 wpisów od najpopularniejszych autorów') + '</td>';
						}
						html = html + '<td class="panel_pager_btns" align="right">';
						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisEntriesFromMostPopularAuthors(1);" class="analysis-entries-from-most-popular-authors-page-first hidden">' + lp._('Pierwsza') + '</a>';
						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisEntriesFromMostPopularAuthors(-1);" class="analysis-entries-from-most-popular-authors-page-prev hidden">' + lp._('Poprzednia') + '</a>';
						for (i=1; i<=noOfPagesAnalysisEntriesFromMostPopularAuthors; i++)
						{
							html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisEntriesFromMostPopularAuthors(' + i + ');" class="analysis-entries-from-most-popular-authors-page analysis-entries-from-most-popular-authors-page-no analysis-entries-from-most-popular-authors-page-' + i + ''+ (i == 1 ? ' selected_page' : '') + '' + (i > 3 ? ' hidden' : '') + '">' + i + '</a>';
						}
						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisEntriesFromMostPopularAuthors(100);" class="analysis-entries-from-most-popular-authors-page-next">' + lp._('Następna') + '</a>';
						html = html + '<a href="javascript:scrollTabToTop();void(0);" onclick="goToPageAnalysisEntriesFromMostPopularAuthors(' + noOfPagesAnalysisEntriesFromMostPopularAuthors + ');" class="analysis-entries-from-most-popular-authors-page-last">' + lp._('Ostatnia') + ' [' + noOfPagesAnalysisEntriesFromMostPopularAuthors + ']</a>';
						html = html + '</td></tr></table></div>';
					}

					$('#entries_from_most_popular_authors').html(html);
					$('.entry-from-most-popular-authors').hide();
					$('.entry-from-most-popular-authors').not('.hidden').fadeIn(1500);
					$(".help_qtip_ajax2").qtip();
                    //move to sources chart
                    if (params['at'] && params['at'] != 'undefined' && params['at'] == 'sc')
                    {
                        $(document).scrollTop( $("#sources-chart-anchor").offset().top );
                    }

					//jesli wczytujemy pierwszy raz a zakladka najbardziej interaktywnych wpisow juz sie wczytala i brak w niej wpisow oraz w zakladce wpisów od najpopularniejszych autorów sa wpisy wtedy ja otwieramy
					if (firstLoadOfAnalysisTab && mostInteractiveEntriesFromSocialMediaLoaded && !$('#most_interactive_entries_from_social_media .mention').length)
					{
						showPopularAuthors();
					}
				}
				else
				{
					//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery
					if (projectHasJustAdded)
					{
						$('#entries_from_most_popular_authors').html('<div class="loading-entries panel_main" align="center"><img src="/static/img/new-results-loader.gif" alt=""><div>' + lp._('Trwa analiza wpisów od najpopularniejszych autorów') + '.</div><div>' + lp._('Proces jest jednorazowy i potrwa tylko chwilę') + '.</div></div>');
					}
					else
					{
						$('#entries_from_most_popular_authors').html('<div class="loading-entries panel_main" align="center"><div class="no-entries">' + lp._('Tymczasowy brak wyników w Social Media.') + '.</div></div>');
					}
				}
				entriesFromMostPopularAuthorsLoaded = true;
				//jesli obie zakladki juz sie zaladowaly
				if (mostInteractiveEntriesFromSocialMediaLoaded)
				{
					firstLoadOfAnalysisTab = false;
				}

				if ($('.showPopularAuthors').hasClass('active'))
				{
					fixHeightOfAnalysisStats($('#entries_from_most_popular_authors'));
				}
			},
		error:
			function()
			{
				$('#entries_from_most_popular_authors').html('<div class="loading-entries panel_main" align="center"><div class="no-entries">' + lp._('Wystąpił błąd podczas pobierania wpisów od najpopularniejszych autorów') + ' <a href="javascript:void(0);" onclick="loadAnalysisEntriesFromMostPopularAuthors();">' + lp._('Spróbuj ponownie') + '</a></div></div>');
			}
	}));
}

//Funkcja do obslugi paginacji wpisów od najpopularniejszych autorów w zakladce Analiza
function goToPageAnalysisEntriesFromMostPopularAuthors(pageNumber)
{
	if (pageNumber == -1)
	{
		currentPageAnalysisEntriesFromMostPopularAuthors--;
	}
	else if (pageNumber == 100)
	{
		currentPageAnalysisEntriesFromMostPopularAuthors++;
	}
	else
	{
		currentPageAnalysisEntriesFromMostPopularAuthors = pageNumber;
	}
	if (currentPageAnalysisEntriesFromMostPopularAuthors == 1)
	{
		$('.analysis-entries-from-most-popular-authors-page-prev').addClass('hidden');
		$('.analysis-entries-from-most-popular-authors-page-first').addClass('hidden');
	}
	else
	{
		$('.analysis-entries-from-most-popular-authors-page-prev').removeClass('hidden');
		$('.analysis-entries-from-most-popular-authors-page-first').removeClass('hidden');
	}
	if (currentPageAnalysisEntriesFromMostPopularAuthors == noOfPagesAnalysisEntriesFromMostPopularAuthors)
	{
		$('.analysis-entries-from-most-popular-authors-page-next').addClass('hidden');
		$('.analysis-entries-from-most-popular-authors-page-last').addClass('hidden');
	}
	else
	{
		$('.analysis-entries-from-most-popular-authors-page-next').removeClass('hidden');
		$('.analysis-entries-from-most-popular-authors-page-last').removeClass('hidden');
	}

	$('.analysis-entries-from-most-popular-authors-page').removeClass('selected_page');
	$('.analysis-entries-from-most-popular-authors-page-' + currentPageAnalysisEntriesFromMostPopularAuthors).addClass('selected_page');

	$('.analysis-entries-from-most-popular-authors-page-no').addClass('hidden');
	$('.analysis-entries-from-most-popular-authors-page-no:lt(' + (currentPageAnalysisEntriesFromMostPopularAuthors + 2)+ ')').removeClass('hidden');
	$('.analysis-entries-from-most-popular-authors-page-no:lt(' + (currentPageAnalysisEntriesFromMostPopularAuthors - 3)+ ')').addClass('hidden');

	$('.entry-from-most-popular-authors').hide();
	$('.entry-from-most-popular-authors').addClass('hidden');
	$('.entry-from-most-popular-authors:lt(' + (currentPageAnalysisEntriesFromMostPopularAuthors * nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage) + ')').removeClass('hidden');
	$('.entry-from-most-popular-authors:lt(' + ((currentPageAnalysisEntriesFromMostPopularAuthors * nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage) - nOfVisibleAnalysisEntriesFromMostPopularAuthorsPerPage) + ')').addClass('hidden');
	$('.entry-from-most-popular-authors').not('.hidden').fadeIn(1500);
	fixHeightOfAnalysisStats($('#entries_from_most_popular_authors'));
}


//Funkcja ladowanie dane do statystyk w zakladce Analiza
function loadAnalysisStatistics()
{
	var paramsUrl = paramsToUrl();

	$('.analysis-stats-result').html('<img style="width:25x; height:25px;" src="/static/img/chart-loader.gif" />');
	$('.analysis-stats-result2').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
	$('#analysis_stats_error').html('');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-statistics/sid/'+searches_id,
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.results_count)
				{
					$('#results_count').html(data.results_count);
					$('#results_count_social_media_in').html(data.results_count_social_media_in);
					$('#results_count_social_media_out').html(data.results_count_social_media_out);
					$('#total_range_of_social_media').html(data.total_range_of_social_media);
					$('#positive_results_count').html(data.positive_results_count + ' <sup>(' + data.positive_results_count_percent + '%)</sup>');
					$('#negative_results_count').html(data.negative_results_count + ' <sup>(' + data.negative_results_count_percent + '%)</sup>');
					$('#total_number_of_interactions').html(data.total_number_of_interactions);
					$('#total_number_of_shares').html(data.total_number_of_shares);
					$('#total_number_of_likes').html(data.total_number_of_likes);
					$('#total_number_of_comments').html(data.total_number_of_comments);
					$('#women_results_count').html(data.women_results_count + ' <sup>(' + data.women_results_count_percent + '%)</sup>');
					$('#men_results_count').html(data.men_results_count + ' <sup>(' + data.men_results_count_percent + '%)</sup>');

					//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery - dodatkowo pobieramy liczbe wyników z ostatnich 30 dni uzupełniamy box z loaderkiem
					if(projectHasJustAdded && data.resultsCountFromLastMonth)
					{
						//poprawiam liczbę wyników - jesli > 0
						if (data.resultsCountFromLastMonth > 0)
						{
							$('#results-loading-box-count').html(data.resultsCountFromLastMonth);
							$('#loader-no-of-results-text').show();
						}
						else
						{
							$('#loader-no-of-results-text').hide();
						}
					}
				}

			},
		error:
			function()
			{
				$('#analysis_stats_error').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania danych do statystyk')+' <a href="javascript:void(0);" onclick="loadAnalysisStatistics();" style="font-size:10px;">'+lp._('Spróbuj ponownie')+'</a><br /></div>');
			}
	}));

}

//Funkcja ladowanie dane do wykresu popularnych zrodel wynikow w zakladce Analiza
function loadAnalysisChartSources()
{
	var paramsUrl = paramsToUrl();

	$('#chart_sources').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
	$('.panel_results_counter').html('<img style="width:13px; height:13px;" src="/static/img/chart-loader.gif" />');
	$('.panel_results_types_bar_in').css('width', '0%');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-chart-sources/sid/'+searches_id,
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.chart_sources)
				{
					var chartSources = data.chart_sources;
					var chartSourcesData = new Array();
					var sourcesCount = 0;
					for(i in chartSources)
					{
						chartSourcesData.push([chartSources[i].site, parseFloat(chartSources[i].percent)]);
						if(chartSources[i].count>0)
							sourcesCount += chartSources[i].count;
					}

					//Wykres zrodel
					var chart;
					chart = new Highcharts.Chart({
						chart: {
							 renderTo: 'chart_sources',
							 plotBackgroundColor: null,
							 plotBorderWidth: null,
							 plotShadow: true,
                             borderRadius: 0,
                             borderWidth: 0,
						},
						colors: [
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#232d3b'],[1, '#232d3b']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#82d008'],[1, '#82d008']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#11a258'],[1, '#11a258']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#3fe390'],[1, '#3fe390']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#447d74'],[1, '#447d74']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#c12156'],[1, '#c12156']]},
						],
						title: {
							 text: ''
						},
						tooltip: {
							 	backgroundColor: '#5f6264',
										 borderColor: '#5f6264',
										 borderWidth: 0,
										 shadow: false,
										 style: {
											padding: 8,
											color: '#ffffff',
										 },
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
							 	}
						},
						legend: {
										 enabled:true,
										 align: 'right',
										 layout: 'vertical',
										 verticalAlign: 'middle',
										 symbolWidth: 8,
										 borderRadius: 0,
										 borderWidth: 0,
										 symbolPadding: 15,
										 itemStyle: {
												color: '#7e7e7e',
												fontFamily: 'verdana',
												fontSize: '9px',
												lineHeight: '15px',
												paddingBottom: '10px',
										 },
										 labelFormatter: function() {
											return '<span>'+this.y +'</span>%<span style="color:#ffffff; font-size:9px;"> - </span><b> '+this.name+'</b>'
										 }
						},
						plotOptions: {
							 pie: {
								innerSize: '68%',
								borderColor: '#eeeeee',
								borderWidth: '0',
								size: '100%',
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
								   enabled: false
								},
								showInLegend: true
							 }
						},
						credits: {
								 enabled:false,
						},
						series: [{
							 type: 'pie',
							 name: 'Browser share',
							 data: chartSourcesData
						}],
						exporting: {
            					enabled: false
            			}
					});
				}
				else
				{
					$('#chart_sources').html('<div align="center">'+lp._('Brak wyników')+'</div>');
				}

				if (data.tab_results_count)
				{
					$('.panel_results_counter').html('0');
					$('.analysis-stats-result-cat').html('0');

					var all_main_chart = 0;
					for (key in data.tab_results_count)
					{
						var item = data.tab_results_count[key];
						$('#panel_results_counter_'+item['page_category']).html(item['count']);
						$('#panel_results_bar_'+item['page_category']).css('width', item['percent']+'%');

						$('#category_results_count_' + item['page_category']).html(item['count']);
					}
				}

			},
		error:
			function()
			{
				$('#chart_sources').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania wykresu popularnych źródeł wyników')+' <a href="javascript:void(0);" onclick="loadAnalysisChartSources();" style="font-size:10px; font-weight:normal;">'+lp._('Spróbuj ponownie')+'</a></div>');
			}
	}));

}

//Funkcja ladowanie wpływu autorów Social Media w zakladce Analiza
function loadAnalysisMostImportantAuthors()
{
	var paramsUrl = paramsToUrl();

	$('#most_important_authors').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-most-important-authors/sid/'+searches_id,
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.most_important_authors)
				{

					var html = '';
					$.each(data.most_important_authors, function(i, item)
					{
						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td>' +
								'<table width="100%">' +
								'<tr>' +
								'<td align="left" width="30">';

						if(item['author_avatar_url'])
						{
							html += '<a class="no-ajax-abort" id="most_important_authors_avatar_' + item['authors_id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');"><img src="' + item['author_avatar_url'] + '" width="20" onerror="imgErrorMostImportantAuthors('+item['authors_id']+')" /></a>';
						}

						html += '</td>' +
								'<td>' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');">' + item['author'] + '</a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</td>' +
								'<td width="7%" align="center" id="service_icon_'+item['authors_id']+'">' +
								'<img src="' + item['service'] + '" onerror="imgErrorService('+item['authors_id']+')" />' +
								'</td>' +
								'<td width="18%" align="right">' +
                '<strong class="sources_entry-list-value">' +
                item['share_of_voice'] +
                '%</strong>' +
                '<span class="sources_entry-list-title help_qtip_ajax" title="'+lp._('Procentowy udział  wybranego autora we wpływie na dyskusję  na temat marki w Social Media. Wynik wyliczany jest jako stosunek procentowy liczby wypowiedzi autora,liczby jego znajomych lub śledzących  oraz współczynnika widzialności treści typowych dla wybranych serwisów społecznościowych  do wszystkich wypowiedzi na temat marki w Social Media.')+'">' +
                lp._('Share of Voice') +
                '</span>' +
								'</td>' +
								'</td>' +
								'<td width="20%" align="right">' +
                '<strong class="sources_entry-list-value">' +
                item['estimated_social_reach'] +
                '</strong>' +
                '<span class="sources_entry-list-title help_qtip_ajax" title="'+lp._('Wewnętrzny wskaźnik Brand24 używany do porównania wpływu różnych profilów social media w zadanym okresie. Wpływ szacowany jest na podstawie liczby wpisów autora na temat marki w Social Media, liczby jego znajomych lub śledzących.')+'">' +
                lp._('Szacowany wpływ') +
                '</span>' +
								'</td>' +
								'<td width="10%" align="center" class="table-options">' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ')"><span class="fa-panel-stack"><i class="fa fa-circle-thin"></i><i class="fa fa-info"></i></span></a> ' +
								'<a class="no-ajax-abort" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id+ ',\'' + item['authors_id']+ '\',\'2\');"><i class="fa fa-lock"></i></a>'+
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});
					$('#most_important_authors').html(html);
          $(".help_qtip_ajax").qtip();
				}
				else
				{
					$('#most_important_authors').html('<div align="center">'+lp._('Brak wyników')+'</div>');
				}

			},
		error:
			function()
			{
				$('#most_important_authors').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania wpływu autorów Social Media')+' <a href="javascript:void(0);" onclick="loadAnalysisMostImportantAuthors();">'+lp._('Spróbuj ponownie')+'</a></div>');
			}
	}));

}

//Funkcja ladowanie najaktywniejszych autorow Social Media w zakladce Analiza
function loadAnalysisMostActiveAuthors(hashtag)
{
	var paramsUrl = paramsToUrl();

	$('#most_active_authors').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-most-active-authors/sid/'+searches_id + resolveHashtagParam(hashtag),
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.most_active_authors)
				{

					var html = '';
					$.each(data.most_active_authors, function(i, item)
					{
						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td>' +
								'<table width="100%">' +
								'<tr>' +
								'<td align="left" width="30">';
                '<div class="sources_author_impact">' + (i + 1) + '</div>';
						if(item['author_avatar_url'])
						{
							html += '<a class="no-ajax-abort" id="most_active_authors_avatar_' + item['authors_id'] + '" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');"><img src="' + item['author_avatar_url'] + '" width="20" onerror="imgErrorMostActiveAuthors(\''+item['authors_id']+'\')" /></a>';
						}

						html += '</td>' +
								'<td style="overflow: hidden">' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ');">' + item['author'] + '</a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</td>' +
								'<td width="15%" align="center" id="service_icon_'+item['authors_id']+'">' +
								'<img src="' + item['service'] + '" onerror="imgErrorService('+item['authors_id']+')" />' +
								'</td>' +
								'<td width="15%" align="right">' +
                '<strong class="sources_entry-list-value">';
						if (item['followers_count'] == '' || item['followers_count'] == '0')
						{
							html += lp._('b/d');
						}
						else
						{
							html += item['followers_count'];
						}
						html +=  '</strong>' +
                '<span class="sources_entry-list-title">' +
                lp._('Dotarcie') +
                '</span>' +
                '</td>' +
								'</td>' +
								'<td width="15%" align="right" class="sources_highlight">' +
                '<strong class="sources_entry-list-value">' +
								item['count'] +
                '</strong>' +
                '<span class="sources_entry-list-title">' +
                lp._('Wypowiedzi') +
                '</span>' +
								'</td>' +
								'<td width="10%" align="center" class="table-options">' +
								'<a class="no-ajax-abort" href="javascript:getInfoAboutSocialMediaAuthor(\'' + item['authors_id'] + '\',0,' + searches_id + ')"><span class="fa-panel-stack"><i class="fa fa-circle-thin"></i><i class="fa fa-info"></i></span></a> ' +
								'<a class="no-ajax-abort" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id+ ',\'' + item['authors_id']+ '\',\'2\');"><i class="fa fa-lock"></i></a>'+
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});
					$('#most_active_authors').html(html);
				}
				else
				{
					$('#most_active_authors').html('<div align="center">'+lp._('Brak wyników')+'</div>');
				}

			},
		error:
			function()
			{
				$('#most_active_authors').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania najaktywniejszych autorów Social Media')+' <a href="javascript:void(0);" onclick="loadAnalysisMostActiveAuthors();">'+lp._('Spróbuj ponownie')+'</a></div>');
			}
	}));

}

//Funkcja ladowanie najbardziek aktywne urle zakladce Analiza
function loadAnalysisMostActiveUrls()
{
	var paramsUrl = paramsToUrl();

	$('#most_active_urls').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-most-active-urls/sid/'+searches_id,
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.most_active_urls)
				{

					var html = '';
					var date1 = $('#date1').val();
					var date2 = $('#date2').val();
					var type = $('#range_type').val();

					$.each(data.most_active_urls, function(i, item)
					{
						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td width="7%">' +
								'<div class="sources_author_impact">' + (i + 1) + '</div>' +
								'</td>' +
								'<td>' +
								'<a href="/panel/results/?sid='+searches_id+'#do='+urlEncode(item['host'])+'&d1='+date1+'&d2='+date2+'&dr='+type+'">' + item['host']+ '</a>' +
								'</td>' +
								'<td width="50%" align="right" style="padding-right: 10px">' +
                '<strong class="sources_entry-list-value">' +
                item['count'] +
                '</strong> ' +
                '<span class="sources_entry-list-title">' +
                lp._('Wypowiedzi') +
                '</span> ' +
                '</td>' +
                '<td width="12%" class="table-options">' +
								'<a class="no-ajax-abort" title="'+lp._('Przejdź do strony')+'" target="_blank" href="http://' + item['host']+'"><span class="fa-panel-stack"><i class="fa fa-sign-out"></i></span></a> ' +
								'<a class="no-ajax-abort" title="'+lp._('Zablokuj źródło')+'" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id+ ',\'' + item['host']+ '\',\'' + item['blocked_content_kind_of_page']+ '\');"><i class="fa fa-lock"></i></a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});
					$('#most_active_urls').html(html);
				}
				else
				{
					$('#most_active_urls').html('<div align="center">'+lp._('Brak wyników')+'</div>');
				}

			},
		error:
			function()
			{
				$('#most_active_urls').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania najaktywniejszych stron')+' <a href="javascript:void(0);" onclick="loadAnalysisMostActiveUrls();">'+lp._('Spróbuj ponownie')+'</a></div>');
			}
	}));

}

function loadAnalysisMostImportantUrls()
{
	var paramsUrl = paramsToUrl();

	$('#most_important_urls').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');

	ajaxTable.push($.ajax({
		type: 'POST',
		url:'/panel/load-most-important-urls/sid/'+searches_id,
		data:paramsUrl,
		dataType:"json",
		success:
			function(data)
			{
				if(data.most_important_urls)
				{

					var html = '';
					var influence_score = '';
					var date1 = $('#date1').val();
					var date2 = $('#date2').val();
					var type = $('#range_type').val();

					$.each(data.most_important_urls, function(i, item)
					{
						influence_score = '';
						if(item['influence_value'] != null)
						{
							influence_score = influence_score + '<div class="influence-score"><span style="width:'+item['influence_score']+'0%"></span></div>'+
								lp._('Influence Score') + ' ' +
'<span class="help_qtip_ajax" title="'+lp._('Influence Score')+'">'+item['influence_score']+'/10</span>';
						}

						html += '<div class="sources_entry">' +
								'<table width="100%">' +
								'<tr>' +
								'<td width="7%">' +
								'<div class="sources_author_impact">' + (i + 1) + '</div>' +
								'</td>' +
								'<td width="45%">' +
								'<a href="/panel/results/?sid='+searches_id+'#do='+urlEncode(item['host'])+'&d1='+date1+'&d2='+date2+'&dr='+type+'">' + item['host']+ '</a>' +
								'</td>' +
								'<td width="100" align="left" style="font-size: 10px; color: #aaa; padding-right: 10px;">' +
                				influence_score +
                '</td>' +
                '<td width="50" align="right" style="padding-right: 10px">' +
                '<strong class="sources_entry-list-value">';

                if(item['influence_value'] != null)
				{
					html += item['influence_value'];
				}
				else
				{
					html += lp._('b/d');
				}

                html += '</strong> ' +
                '<span class="sources_entry-list-title">' +
                lp._('Wizyt') +
                '</span> ' +
                '</td>' +
                '<td width="12%" align="right" class="table-options">' +
								'<a class="no-ajax-abort" title="'+lp._('Przejdź do strony')+'" target="_blank" href="http://' + item['host']+'"><span class="fa-panel-stack"><i class="fa fa-sign-out"></i></span></a> ' +
								'<a class="no-ajax-abort" title="'+lp._('Zablokuj źródło')+'" href="#" onclick="return blockHostOrAuthorInSearch(' + searches_id+ ',\'' + item['host']+ '\',\'' + item['blocked_content_kind_of_page']+ '\');"><i class="fa fa-lock"></i></a>' +
								'</td>' +
								'</tr>' +
								'</table>' +
								'</div>';
					});
					$('#most_important_urls').html(html);
          $(".help_qtip_ajax").qtip();
				}
				else
				{
					$('#most_important_urls').html('<div align="center">'+lp._('Brak wyników')+'</div>');
				}
			},
		error:
			function()
			{
				$('#most_important_urls').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania najbardziej wpływowych stron')+' <a href="javascript:void(0);" onclick="loadAnalysisMostImportantUrls();">'+lp._('Spróbuj ponownie')+'</a></div>');
			}
	}));

}

//Funkcja ladowanie najpopularniejszych słów zakladce Analiza
function loadMostPopularWords()
{
	var paramsUrl = paramsToUrl();

	$('#most_popular_words').html('<div align="center"><img src="/static/img/chart-loader.gif" /><br />' + lp._('Przetworzenie wszystkich danych potrzebnych do analizy kontekstu dyskusji może potrwać kilka minut') + '.</div>');

	ajaxTable.push($.ajax({
		type: 'POST',
		url: '/panel/get-most-popular-words/sid/'  +searches_id + '/tab/' + tab,
		data: paramsUrl,
		success: function(data)
		{
			if (data != 'error')
			{
				$('#most_popular_words').html(data);
			}
			else
			{
				$('#most_popular_words').html('<div align="center">' + lp._('Wystąpił błąd podczas pobierania najpopularniejszych słów') + ' <a href="javascript:void(0);" onclick="loadAnalysisMostPopularWords();">' + lp._('Spróbuj ponownie') + '</a></div>');
			}
		},
		error: function()
		{
			$('#most_popular_words').html('<div align="center">' + lp._('Wystąpił błąd podczas pobierania najpopularniejszych słów') + ' <a href="javascript:void(0);" onclick="loadAnalysisMostPopularWords();">' + lp._('Spróbuj ponownie') + '</a></div>');
		}
	}));
}

//Funkcja inicjuje ladowanie wynikow w zakladce cmpare
function initCompareProjects()
{
	params['d1'] = $('#date1').val();
	params['d2'] = $('#date2').val();
    params['dr'] = $('#range_type').val();
    params['cdt'] = 'days';

	if(params['dr']=='w')
    {
    	$('#date_range_offset a').html(params['d1']+' - '+params['d2']);
    }
    else
	{
    	$('#date_range_offset a').html($('#range_type :selected').text());
    }

	var paramsUrl = paramsToUrl();

	var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;

	addParamsToMainATags(paramsUrl);
	window.location = new_url;

	if (search_on_begin == 1)
	{
		window.location.href = new_url;
		return;
	}

	noOfProjects = 0;
	nextProjectId = 0;
	removedProjects = new Array();
	projectsCount = 1;
	mainChart = null;

	$('#chart_main').hide();
	$('#chart_main_loader').show();

	for(var i=0;i<=maxProjectsToCompare;i++)
	{
		if(i > 0) $('#project_'+i).hide();
		$('#project_name_'+i).html('');
		$('#compare_stats_'+i).html('<img src="/static/img/chart-loader.gif" style="margin-top:60px;" />');
		$('#compare_chart_sources_'+i).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
		$('#compare_chart_sentiment_'+i).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
		if(showGender == 1)
			$('#compare_chart_gender_'+i).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
	}

	addProjectToCompare(searches_id, 0);

	$.each(projectsToCompare, function(i, row)
	{
		if(!isNaN(row))
		{
	        nextProjectId = getNextProjectId();
			$('#project_'+nextProjectId).show();

			//setTimeout(function() {
    			addProjectToCompare(row, nextProjectId);
			//}, 4000);

		    $("#project_to_compare_div_"+row).hide();
		    projectsCount++;
	        if(projectsCount <= maxProjectsToCompare && $('.project_to_compare_div').length>0)
		        $(".add_project_to_compare_button").show();
		    else
		        $(".add_project_to_compare_button").hide();
		}
	});

}

//Funkcja otwiera okienko dodawania projektu do porównania
function showAddNewProjectToCompare()
{
	$( "#panel_data_add_project-to-compare").dialog( "open" );
}

//Funkcja dodaje projekt do porównania
function addProjectToCompare(projectId, elementId)
{
	var numberOfDays = calculateDaysBetweenTwoDates(params['d1'], params['d2']);
	var paramsUrl = paramsToUrl();

	$('.add_project_to_compare_button').css('pointer-events','none');
	$('.del_project_to_compare_button').css('pointer-events','none');

	$('#chart_main').hide();
	$('#chart_main_loader').show();

	ajaxTable.push($.ajax({
		type: 'POST',
		url: '/panel/add-project-to-compare/sid/'+projectId,
		data: paramsUrl,
		dataType:"json",
		success:
		function(data)
		{
			if(data.results)
			{
				if(projectId == searches_id)
	        		$('#project_name_'+elementId).html('<span style="color:'+colorsTable[elementId]+';">'+lp._('Projekt')+': '+data.name+'</span>');
				else
          $('#project_name_'+elementId).html('<span style="color:'+colorsTable[elementId]+';">'+lp._('Projekt')+': '+data.name+'</span><div class="mention_option_delete" style="display:inline-block;"><a href="javascript:removeProjectToCompare('+projectId+', \''+data.name+'\', '+elementId+')" class="del_project_to_compare_button no-ajax-abort project-remove"><i class="fa fa-times"></i>'+lp._('Usuń projekt z porównania')+'</a></div>');
				$('#compare_stats_'+elementId).html('<div class="panel_table" style="height:200px;"><div class="compare_title">'+lp._('Wyniki')+':</div><div class="stat_title"><b>'+data.results.results_count+'</b><br />'+lp._('Razem')+'</div><div class="stat_title"><span class="stat_pos"><b>'+data.results.positive_results_count+'</b><br />'+lp._('Pozytywnych')+'</span></div><div class="stat_title"><span  class="stat_neg"><b>'+data.results.negative_results_count+'</b><br />'+lp._('Negatywnych')+'</span></div></div>');
			}
			if (data.charts)
			{
				var chartDisplayInterval = 1;
				var chart_main = data.charts.chart_main;
				var chartPoints = chart_main.pages_count_per_day_chart.length;
				var chartDisplayIntervalMain = Math.ceil(chartPoints/15);

				if(mainChart == null)
				{

					var mainChartOptions = {
								 chart: {
						         renderTo: 'chart_main',
								 backgroundColor: '#ffffff',
								 marginTop: 20,
								 marginBottom: 80,
                                 borderRadius: 0,
                                 borderWidth: 0,
						      },
						      title: {
						         text: ''
						      },
						      xAxis: {
						         type: 'datetime',
						         //tickInterval: 48 * 3600 * 1000,
								 gridLineWidth: 1,
								 gridLineColor :'#bbbbbb',
								 gridLineDashStyle: 'Dot',
						         title: {
						            text: null
						         }
						      },
						      yAxis: {
						         title: {
						            text: null
						         },
								 min: 0,
								 gridLineWidth: 1,
								 gridLineColor :'#bbbbbb',
								 gridLineDashStyle: 'Dot',
						         labels: {
						            formatter: function() {
						               return this.value;
						            }
						         }
						      },
							  tooltip: {
								 backgroundColor: '#5f6264',
								 borderColor: '#5f6264',
								 borderWidth: 0,
								 crosshairs: true,
								 shadow: false,
								 style: {
									padding: 8,
									color: '#ffffff',
								 },
								 formatter: function() {
										   return '<span style="font-size:10px">'+Highcharts.dateFormat('%A, %B %e, %Y', this.x)+'</span><br/>'+
												   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
								 }
							  },
							      plotOptions: {
							         area: {
							            pointInterval: 1 * 24 * 3600 * 1000,
							         	pointStart: Date.UTC(2010, 12, 17),
							            marker: {
							               lineWidth: 3,
							               lineColor: '#ba4cfe',
										   radius: 4
							            }
							         },
						         	series: {
						            cursor: 'pointer',
						         }
						      },
							  legend: {
							 	enabled:true,
							 	verticalAlign: 'bottom',
                                borderRadius: 0,
                                borderWidth: 0,
						      },
							  credits: {
								  enabled:false,
							  },
						      series: [],
							}
							mainChartOptions.tooltip.formatter = function() {
								   return '<span style="font-size:10px">'+Highcharts.dateFormat('%A, %B %e, %Y', this.x)+'</span><br/>'+
										   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
					};

					mainChartOptions.series[0] = {
							id: elementId,
					         name: data.name,
							 color: colorsTable[elementId],
							 type: 'line',
							 plotShadow: false,
							 	shadow: false,
					            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
					         	//pointStart: Date.UTC(2010, 12, 17),
								lineColor: colorsTable[elementId],
								lineWidth: 3,
					            marker: {
					               lineWidth: 0,
					               lineColor: '#ffffff',
								   radius: 0
					            },
					            pointStart:Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month, chart_main.pointStart.day),
					           data: chart_main.pages_count_per_day_chart,
						            point: {
						                events: {
						                    click: function() {
												chartDataFilter(Highcharts.dateFormat('%Y-%m-%d', this.x),Highcharts.dateFormat('%Y-%m-%d', this.x), projectId);
						                    }
						                }
						            }
					         }

        			mainChartOptions.xAxis.tickInterval = chartDisplayIntervalMain * 24 * 3600 * 1000;
					mainChart = new Highcharts.Chart(mainChartOptions);
				}
				else
				{
					var series = {
							id: elementId,
					         name: data.name,
							 color: colorsTable[elementId],
							 type: 'line',
							 plotShadow: false,
							 	shadow: false,
					            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
					         	//pointStart: Date.UTC(2010, 12, 17),
								lineColor: colorsTable[elementId],
								lineWidth: 3,
					            marker: {
					               lineWidth: 0,
					               lineColor: '#ffffff',
								   radius: 0
					            },
					            pointStart:Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month, chart_main.pointStart.day),
					           data: chart_main.pages_count_per_day_chart,
						            point: {
						                events: {
						                    click: function() {
												chartDataFilter(Highcharts.dateFormat('%Y-%m-%d', this.x),Highcharts.dateFormat('%Y-%m-%d', this.x), projectId);
						                    }
						                }
						            }
					         }

        				mainChart.addSeries(series, false);
				}
				var chartSources = data.charts.chart_sources;
				var chartSourcesData = new Array();
				var sourcesCount = 0;
				for(i in chartSources)
				{
					chartSourcesData.push([chartSources[i].site, parseFloat(chartSources[i].percent)]);
					if(chartSources[i].count>0)
						sourcesCount += chartSources[i].count;
				}

				if(sourcesCount > 0)
				{
					//Wykres zrodel
					var chart;
				   chart = new Highcharts.Chart({
					  chart: {
					 renderTo: 'compare_chart_sources_'+elementId,
					 plotBackgroundColor: null,
					 plotBorderWidth: null,
					 plotShadow: true,
                     borderRadius: 0,
                     borderWidth: 0,
					  },
					  colors: [
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#354863'],[1, '#354863']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#82d008'],[1, '#82d008']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#11a258'],[1, '#11a258']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#3fe390'],[1, '#3fe390']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#447d74'],[1, '#447d74']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#c12156'],[1, '#c12156']]},
					  ],
					  title: {
					 text: lp._('Źródła')
					  },
					  tooltip: {
					 formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
					 }
					  },
					  plotOptions: {
					 pie: {
						innerSize: '68%',
						borderColor: '#eeeeee',
						borderWidth: '0',
						size: '100%',
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
						   enabled: false
						},
						showInLegend: true
					 }
					  },
					  legend: {
								 enabled:true,
								 align: 'right',
								 layout: 'vertical',
								 verticalAlign: 'middle',
								  borderRadius: 0,
								  borderWidth: 0,
								 labelFormatter: function() {
									return '<span>'+this.y +'</span>%<span style="color:#ffffff"> - </span><b> '+this.name+'</b>'
								 }
					  },
					  credits: {
						 enabled:false,
					  },
					   series: [{
					 type: 'pie',
					 name: 'Browser share',
					 data: chartSourcesData
					  }]
				   });
			   }
				else
				{
					$('#compare_chart_sources_'+elementId).html('<div class="panel_table" style="height:200px;"><div class="compare_chart_title">'+lp._('Źródła')+'</div><div align="center">'+lp._('Brak wyników')+'</div></div>');

				}

				if((parseFloat(data.charts.positive_results_count_percent) + parseFloat(data.charts.negative_results_count_percent)) > 0)
				{
					//Wykres sentymentu
					var chartSentimentData = new Array();
					chartSentimentData.push([lp._('Pozytywne'), parseFloat(data.charts.positive_results_count_percent)]);
					chartSentimentData.push([lp._('Negatywne'), parseFloat(data.charts.negative_results_count_percent)]);

					var chart2;
				   chart2 = new Highcharts.Chart({
					  chart: {
					 renderTo: 'compare_chart_sentiment_'+elementId,
					 plotBackgroundColor: null,
					 plotBorderWidth: null,
					 plotShadow: true,
                     borderRadius: 0,
                     borderWidth: 0,
					  },
					  colors: [
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#1AA98A'],[1, '#1AA98A']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#e82e56'],[1, '#e82e56']]},
					  ],
					  title: {
					 text: lp._('Sentyment')
					  },
					  tooltip: {
					 formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
					 }
					  },
					  plotOptions: {
					 pie: {
						innerSize: '68%',
						borderColor: '#eeeeee',
						borderWidth: '0',
						size: '100%',
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
						   enabled: false
						},
						showInLegend: true
					 }
					  },
					  legend: {
								 enabled:true,
								 align: 'center',
								 layout: 'vertical',
								 verticalAlign: 'bottom',
								  borderRadius: 0,
								  borderWidth: 0,
								 labelFormatter: function() {
									return '<span>'+this.y +'</span>%<span style="color:#ffffff"> - </span><b> '+this.name+'</b>'
								 }
					  },
					  credits: {
						 enabled:false,
					  },
					   series: [{
					 type: 'pie',
					 name: 'Browser share',
					 data: chartSentimentData
					  }]
				   });
			   }
				else
				{
					$('#compare_chart_sentiment_'+elementId).html('<div class="panel_table" style="height:200px;"><div class="compare_chart_title">'+lp._('Sentyment')+'</div><div align="center">'+lp._('Brak wyników')+'</div></div>');

				}


				if(showGender == 1){
				if((parseFloat(data.charts.men_results_count_percent) + parseFloat(data.charts.women_results_count_percent)) > 0)
				{
					//Wykres plci
					var chartGenderData = new Array();
					chartGenderData.push([lp._('Mężczyźni'), parseFloat(data.charts.men_results_count_percent)]);
					chartGenderData.push([lp._('Kobiety'), parseFloat(data.charts.women_results_count_percent)]);

					var chart3;
				   chart3 = new Highcharts.Chart({
					  chart: {
					 renderTo: 'compare_chart_gender_'+elementId,
					 plotBackgroundColor: null,
					 plotBorderWidth: null,
					 plotShadow: true,
					 borderRadius: 0,
					 borderWidth: 0,
					  },
					  colors: [
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#1AA98A'],[1, '#1AA98A']]},
					   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#8fdc10'],[1, '#78c601']]},
					  ],
					  title: {
					 text: lp._('Płeć')
					  },
					  tooltip: {
					 formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
					 }
					  },
					  plotOptions: {
					 pie: {
						 innerSize: '68%',
						borderColor: '#eeeeee',
						borderWidth: '0',
						size: '100%',
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
						   enabled: false
						},
						showInLegend: true
					 }
					  },
					  legend: {
								 enabled:true,
								 align: 'center',
								 layout: 'vertical',
								 verticalAlign: 'bottom',
								 borderRadius: 0,
								 borderWidth: 0,
								 labelFormatter: function() {
									return '<span>'+this.y +'</span>%<span style="color:#ffffff"> - </span><b> '+this.name+'</b>'
								 }
					  },
					  credits: {
						 enabled:false,
					  },
					   series: [{
					 type: 'pie',
					 name: 'Browser share',
					 data: chartGenderData
					  }]
				   });
			   }
				else
				{
					$('#compare_chart_gender_'+elementId).html('<div class="panel_table" style="height:200px;"><div class="compare_chart_title">'+lp._('Płeć')+'</div><div align="center">'+lp._('Brak wyników')+'</div></div>');

				}
				}
			}
			$('#chart_main_loader').hide();
			$('#chart_main').show();
			mainChart.setSize(width, 330);
			$('.add_project_to_compare_button').css('pointer-events','');
			$('.del_project_to_compare_button').css('pointer-events','');
		},
		error:
			function()
			{
				prepareDialogBox(2,lp._('Wystąpił błąd podczas pobierania wyników'));
				var buttonsReload = {};
				buttonsReload[lp._('Spróbuj ponownie')] = function(){
					addProjectToCompare(projectId, elementId);
					$(this).dialog('close');
				};
				$("#dialog-window").dialog({
					resizable: false,
					height: 170,
					width: 300,
					modal: true,
					buttons: buttonsReload
				});
			}
	}));
}

//Funkcja usuwa projekt do porównania
function removeProjectToCompare(projectId, projectName, id)
{

		var key = $.inArray(projectId, projectsToCompare);
		if(key != -1)
		{
			projectsToCompare.splice(key,1);
		}
		var temp = '';
		for(var i = 0; i < projectsToCompare.length; i++)
		{
			if(!isNaN(projectsToCompare[i]))
			{
				temp += projectsToCompare[i]+',';
			}
		}
		params['pc'] = temp;

		var paramsUrl = paramsToUrl();
		var new_url = controller_action+'?sid='+searches_id+'#'+paramsUrl;
		addParamsToMainATags(paramsUrl);
		window.location = new_url;

		$(".add_project_to_compare_button").show();
		$('#project_name_'+id).html('');
		$('#compare_stats_'+id).html('<img src="/static/img/chart-loader.gif" style="margin-top:60px;" />');
		$('#compare_chart_sources_'+id).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
		$('#compare_chart_sentiment_'+id).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
		$('#compare_chart_gender_'+id).html('<div align="center"><img src="/static/img/chart-loader.gif" style="margin-top:60px;" /></div>');
		$('#project_to_compare_div_'+projectId).show();
		$('#project_to_compare_'+projectId).removeAttr('checked');
		projectsCount--;
		$('#project_'+id).hide();
		if(id==noOfProjects)
		{
			$('#project_'+(noOfProjects-1)).after($('#project_'+id));
		}
		else
		{
			$('#project_'+noOfProjects).after($('#project_'+id));
		}
		removedProjects[id] = id;
		$.each(mainChart.series, function(i, row)
		{
			if(row.options.name == projectName)
				row.remove();
		});
}

//Funkcja pobiera nastepny id elementu z dodawanym projektem do porównania
function getNextProjectId()
{
		var nr = 0;
		$.each(removedProjects, function(key) {
			if(removedProjects[key]>0){
				if(nr == 0)
				{
					nextId = removedProjects[key];
					delete removedProjects[key];
				}
				nr++;
			}
		});
		if(nr>0)
		{
			return nextId;
		}
		else
		{
			noOfProjects++;
			return noOfProjects;
		}
}

//Po 30 sekundach wyswietla komunikat Prosimy o chwilę cierpliwości. Dane projektu ładują się do przestrzeni roboczej po udanym wczytywaniu chowa
var searchResultsLoadingStart = new Date().getTime();
function searchResultsTooLongLoading()
{
	var now = new Date().getTime();
	var loadingTime = now - searchResultsLoadingStart;
	if(loadingTime <= 30000 && ($('#main_charts').html().indexOf("chart-loader.gif") > -1 || $('#results_content').html().indexOf("chart-loader.gif") > -1))
	{
		setTimeout("searchResultsTooLongLoading();",1000);
	}
	else if(loadingTime > 30000 && ($('#main_charts').html().indexOf("chart-loader.gif") > -1 || $('#results_content').html().indexOf("chart-loader.gif") > -1))
	{
		$('#data_loading_text').show();
		setTimeout("searchResultsTooLongLoading();",1000);
	}
	else if($('#main_charts').html().indexOf("chart-loader.gif") == -1 && $('#results_content').html().indexOf("chart-loader.gif") == -1)
	{
		$('#data_loading_text').hide();
	}
}

//Funkcja inicjuje ladowanie wynikow w zakladce results
function searchResults()
{
	searchResultsNew(0);

	if (search_on_begin == 0)
	{
		searchResultsNew(1);
	}
}

function generatePanelPagerFooter(page, pager, value) {
	var html = '<div class="panel_pager_top">' +
					'<select class="results-top-controls__mention-order-select" name="state">' +
						'<option value="0">' + lp._('mention-order.select.recent') + '</option>' +
						'<option value="2">' + lp._('mention-order.select.popular') + '</option>' +
					'</select>' +
					'<div class="results-top-controls__separator"></div>' +
					'<div class="results-top-controls__search">' +
					'<div class="results-top-controls__input-wrapper options-search">' +
						'<input value="" id="filter_input_te" onkeydown="if (event.keyCode == 13) { this.blur(); toggleFilter(\'te\', value); }" name="results_search" class="results_search_input results-top-controls__search-input" placeholder="' + lp._('Szukaj w wynikach') + '" />' +
						'<input type="submit" class="btn-cta btn-filter-te results-top-controls__search-button" onclick="javascript:toggleFilter(\'te\', $(\'#filter_input_te\').val())" value="' + lp._('Szukaj') + '" />' +
					'</div>';
	
	if (params['te']) {
		html += '<a class="filter_icon" id="all_te" href="javascript:toggleFilter(\'te\', \'\')">x</a>';
	}
	
	html += '<a class="filter_icon help_qtip_ajax no-ajax-abort" title="' + lp._('filters.text_search_input.qtip') + '" href="javascript:void(0);">?</a></div>';
			
	if (page !== null) {
		html += '<div class="results-top-controls__separator"></div>';
	}
	
	html += '<div class="panel_pager_btns">';
	
	if (page !== null) {
		if (page > 1) {
			html += '<a href="javascript:void(0)" onclick="page=1;searchResultsNew(0);">' + lp._('Pierwsza') + '</a>' +
					'<a href="javascript:void(0)" onclick="page='+(page-1)+';searchResultsNew(0);">'+lp._('Poprzednia')+'</a>';
		}
		
		for (var i=1; i<=pager; i++) {
			if ((i > page-3) && (i < page+3)) {
				html += '<a class="page_no' + (i == page ? ' selected_page' : '') + '" href="javascript:void(0);" onclick="page=' + i + ';searchResultsNew(0);">' + i + '</a>';
			}
		}
		
		if (page < pager) {
			html += '<a href="javascript:void(0)" onclick="page=' + (page + 1) + ';searchResultsNew(0);">' + lp._('Następna') + '</a>';
			html += '<a href="javascript:void(0)" onclick="page=' + pager + ';searchResultsNew(0);">' + lp._('Ostatnia') + ' [' + pager + ']</a>';
		}
		html += '</div></div>';
	}
	else {
		html += '</div></div>';
		html += '<div class="noresults_box">' + lp._('Brak wyników') + '</div>';
	}
	return html;
}

function injectMentionOrderSelect($select, $wrapper) {
	var $mentionOrderSelect = $('.results-top-controls__mention-order-select')[0];
	window.mentionOrderSelect = new Choices($mentionOrderSelect, {
		searchEnabled: false,
		classNames: {
			containerOuter: 'choices results-top-controls__mention-order-wrapper',
			containerInner: 'b24-choices-select results-top-controls__mention-order-select'
		},
		itemSelectText: ''
	});
	
	$mentionOrderSelect.addEventListener('change', function(event) {
		toggleFilter('or', event.detail.value);
	});
	
	$mentionOrderSelect.addEventListener('showDropdown', function(event) {
		$('.choices__item--choice[data-value=' + params["or"] + ']').attr('data-active', true);
	});
}


//Funkcja wyszukuje dane w wypowiedziach
//if show_charts == 0 - laduje tylko wynikki
//if show_charts == 1 - laduje tylko wykres - przeladowuje liczbe wynikow w kategoriach
//if show_charts == 2 - laduje tylko  wykres - nie przeladowuje liczby wynikow w kategoriach
function searchResultsNew(show_charts, hashtag)
{
	params['d1'] = $('#date1').val();
	params['d2'] = $('#date2').val();
	params['dr'] = $('#range_type').val();
	params['cdt'] = chart_display_type;

	var numberOfDays = calculateDaysBetweenTwoDates(params['d1'], params['d2']);
	(numberOfDays>=30) ? $('#chart_main_time_main_months').show() : $('#chart_main_time_main_months').hide();
	(numberOfDays>=7) ? $('#chart_main_time_main_weeks').show() : $('#chart_main_time_main_weeks').hide();

	//jesli laduje wyniki wlaczam loader wynikow
	if (show_charts == 0)
	{
		$('#results_content').html('<div class="mention_loader"><div class="panel_chart_loader_meta" align="center">' + lp._('Wczytywanie danych') + '...</div><div align="center" class="panel_chart_loader_gif"><img src="/static/img/chart-loader.gif" /></div></div>');
	}

	if (params['dr'] == 'w')
	{
		$('#date_range_offset a').html(params['d1'] + ' - ' + params['d2']);
	}
	else
	{
		$('#date_range_offset a').html($('#range_type :selected').text());
	}

	if (params['or'] != 0 && params['or'] != 1 && params['or'] != 2) {
		params['or'] = orderBy;
	}
	
	var paramsUrl = paramsToUrl() + '&p=' + page;

	var new_url = controller_action + '?sid=' + searches_id + '#' + paramsUrl;

	addParamsToMainATags(paramsUrl);
	window.location = new_url;

	if (search_on_begin == 1)
	{
		window.location.href = new_url;
		return;
	}
	new_url = new_url.replace('#', '&');

	new_url = new_url.replace(controller_action, '/panel/results-search2/');
	new_url = new_url + '&export=excel';

	$('#results_download').attr('href', 'javascript:' + (export_permission == 0 ? 'showInfoNotAdequateAccountType();' : 'showExcelPreloader(); goto(\'http://' + urlEncode(location.host+new_url) + '\');') + '');

	//Excel Zbiorczy z wszystkich projektow konta
	if ($('#results_download_all'))
	{
		var new_url_all = new_url.replace('/panel/results-search2/', '/panel/results-search-from-all-projects-of-account/');
		$('#results_download_all').attr('href', 'javascript:'+ (export_permission == 0 ? 'showInfoNotAdequateAccountType();' : 'showExcelPreloader(); goto(\'http://' + urlEncode(location.host+new_url_all) + '\');') + '');
	}

	//jesli laduje dane do wykresow wlaczam loader wykresow
	if (show_charts != 0)
	{
		$('.chart_main_title').css('pointer-events','none'); //wylaczam przelaczniki wykresu glownego
		$('#chart_main').html('<div align="center" style="padding-top:30px"><img src="/static/img/chart-loader.gif" /></div>');
		$('#chart_main_sentiment').html('<div align="center" style="padding-top:30px"><img src="/static/img/chart-loader.gif" /></div>');
		//jesli oprocz wykresow laduje liczby wynikow w kategoriach - dodatkowo loaderki liczb
		if (show_charts == 1)
		{
			$('.panel_results_counter').html('<img style="width:13px; height:13px;" src="/static/img/chart-loader.gif" />');
			$('#chart_sources').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
		}
		$('#main_charts_help').hide();
		$('#main_charts_export').hide();
	}

	if (!((!params['rt'] || $('#selected_category_1:checked').length || $('#selected_category_2:checked').length || $('#selected_category_4:checked').length || $('#selected_category_5:checked').length)))
	{
		$('#show_interactions_series_btn').hide();
	}

	$('#data_loading_text').hide();
	searchResultsLoadingStart = new Date().getTime();
	searchResultsTooLongLoading();

	ajaxTable.push($.ajax({
		type: 'POST',
		url: '/panel/results-search2/sid/' + searches_id + resolveHashtagParam(hashtag),
		data: paramsUrl + '&sc=' + show_charts,
		dataType: 'json',
		success:
			function(data)
			{
			    if (!data && typeof data !== 'object')
                {
					$('#results_content').html(generatePanelPagerFooter(null, null, this.value));
					injectMentionOrderSelect();
				}

				if (show_charts == 0)
				{
					results = [];
					if (data.hasOwnProperty('results') && data.results)
					{
						var html = '';
						var href = '';
						var mentionVisitedClass = '';
						var mentionVisitedInfo = '';
						var mentionSpamClass = '';
						var mentionSpamInfo = '';
						var mentionRestrictedClass = '';
						var avatarIcon = '';
						var followersCountInfo = '';
						var sentimentSelectClass = '';
						var sentimentListHtml = '';
						var interactionsStatsHtml = '';
						var groupsInfoHtml = '';
						var groupsListHtml = '';
						var historyHref = '';
						var influenceScoreHtml = '';
						var titleIcon = '';
						var title = '';
						var content = '';
						var contentOryginal = '';
						var blockedContentKind = '';
						var hiddenSourceUrl = '';
						var moreOptionsListHtml = '';
						var response = '';
						var mentionResponseClass = '';
						var responseText = '';
						var genderInfo = '';
						var sourceInfo =  '';
						var mentionSiteClass = '';

						if (data.pager)
						{
							page = parseInt(page);
							html += generatePanelPagerFooter(page, data.pager, this.value);
							
						}

						$.each(data.results, function(i, item)
						{
							results[item['results_id']] = item;

							href = item['result_open_link'];

							//sprawdzam czy wynik jest widoczny, czy nie ma ograniczenia w archiwum
							mentionRestrictedClass = '';
							if (item['cant_see'])
							{
								href = '/account/upgrade';
								mentionRestrictedClass = ' mention_restricted';
							}

							//sprawdzam czy wynik jest odwiedzony
							mentionVisitedClass = '';
							mentionVisitedInfo = '';
							if (item['last_visit_date'])
							{
								mentionVisitedClass = ' mention_vis vis_' + item['kind'];
								mentionVisitedInfo = '<span class="visited"> </span>';
							}

							//sprawdzam czy wynik jest spamowy
							mentionSpamClass = '';
							mentionSpamInfo = '';
							if ((item['h1_low_quality'] == 1) || (item['h1_spam'] == 1) || (item['h2_low_quality'] == 1) || (item['h2_spam'] == 1) || (item['host_spam'] == 1))
							{
								mentionSpamClass = ' spam';
								mentionSpamInfo = '<li class="spam-label"><a href="#"><i class="fa fa-ban"></i> SPAM</a></li>';
							}

							//sprawdzam czy wynik jest odwiedzony
							response = '';
							mentionResponseClass = '';
							responseText = lp._('Oznacz');
							if (item['response'])
							{
								response = '_ok';
								mentionResponseClass = ' mention-responsed';
								responseText = lp._('Odznacz');
							}

							//określam typ blokowanego wyniku
							blockedContentKind = item['blocked_content_kind_of_page'];
							if ((blockedContentKind == 1) && (item['kind'] != 7) && (item['kind'] != 6))
							{
								tempBlockedContentKind = 0;
							}

							if (blockedContentKind == 2 && item['authors_id'] == null)
							{
								blockedContentKind = 0;
							}

							//okrślam wartość dla ukrytego atrybutu hidden_source_url
							hiddenSourceUrl = '';
							if (item['authors_id'] != null)
							{
								hiddenSourceUrl = item['author_url'];
							}
							else
							{
								hiddenSourceUrl = item['host'];
							}

							var sourceInfo =  '<a href="#" class="mention-source"><i class="fa fa-share-alt"></i> Facebook.pl</a>';

							//informacja o płci
							genderInfo = '';

							//tworze informacje o followersach
							followersCountInfo = '';
							if (item['authors_id'] != null && item['followers_count'] != null)
							{
								followersCountInfo =  '<p class="followers-count"><span class="followers-count-no">' + item['followers_count'] + '</span> ' + lp._('Śledzących') + '</p>';
							}

							//ikonka avatara
							avatarIcon = '';
							if (item['authors_id'] != null && item['mention_thumb_url'] != '')
							{
								avatarIcon = '<img src="' + item['mention_thumb_url'] + '" onerror="imgError(' + item['results_id'] + ')" />';
								//dodaję ikonke play video
								if (item['page_category'] == 4)
								{
									avatarIcon += '<span class="play-button"><i class="fa fa-youtube-play"></i></span>';
								}
							}
							else
							{
								switch (item['page_category'])
								{
									case 5:
										avatarIcon = '<i class="fa fa-facebook-square fa-avatar"></i>';
										break;
									case 1:
										avatarIcon = '<i class="fa fa-twitter fa-avatar"></i>';
										break;
									case 3:
										avatarIcon = '<i class="fa fa-rss fa-avatar"></i>';
										break;
									case 6:
										avatarIcon = '<i class="fa fa-comments-o fa-avatar"></i>';
										break;
									case 7:
										avatarIcon = '<i class="fa fa-newspaper-o fa-avatar"></i>';
										break;
									case 4:
										avatarIcon = '<i class="fa fa fa-youtube-play fa-avatar"></i>';
										break;
									case 2:
										avatarIcon = '<i class="fa fa-instagram fa-avatar"></i>';
										break;
									default:
										avatarIcon = '<i class="fa fa-share-alt fa-avatar"></i>';
										break;
								}
							}

							//ikonka serwisu w tytule
							titleIcon = '';
							//okreslam ikonke serwisu
							switch (item['socialmedia_sites_id'])
							{
								case 1:
									titleIcon = '<i class="fa fa-facebook-square"></i>';
									break;
								case 2:
									titleIcon = '<i class="fa fa-twitter"></i>';
									break;
								case 4:
									titleIcon = '<i class="fa fa-youtube"></i>';
									break;
								case 5:
									titleIcon = '<i class="fa fa-instagram"></i>';
									break;
								case 7:
									titleIcon = '<i class="fa fa-tumblr"></i>';
									break;
								case 8:
									titleIcon = '<i class="fa fa-google-plus"></i>';
									break;
								case 10:
									titleIcon = '<i class="fa fa-flickr "></i>';
									break;
							}

							//na podstawie hosta
							if (titleIcon == '')
							{
								switch (item['host'])
								{
									case 'youtube.com':
										titleIcon = '<i class="fa fa-youtube"></i>';
										break;
									case 'instagram.com':
										titleIcon = '<i class="fa fa-instagram"></i>';
										break;
									case 'tumblr.com':
										titleIcon = '<i class="fa fa-tumblr"></i>';
										break;
									case 'plus.google.com':
										titleIcon = '<i class="fa fa-google-plus"></i>';
										break;
									case 'flickr.com':
										titleIcon = '<i class="fa fa-flickr"></i>';
										break;
								}
							}

							//akcja po kliknięciu w history
							historyHref = '';
							if (item['authors_id'] != null)
							{
								historyHref = 'javascript:page=1;scrollTabToTop();$(\'#filter_input_au\').val(\'' + item['author'] + '\');$(\'#filter_input_as\').val(\'' + item['socialmedia_sites_id'] + '\');toggleFilter(\'au\', \'' + item['author'] + '\');';
							}
							else
							{
								historyHref =  'javascript:page=1;scrollTabToTop();$(\'#filter_input_do\').val(\'' + item['host'] + '\');toggleFilter(\'do\', \'' + item['host'] + '\');';
							}

							//tytuł i treść wzmianki
							title = html_entity_decode(item['title'], 'ENT_QUOTES');
                            content = '';
                            contentOryginal = item['content_oryginal'];

                            if (item['mention_img_url'])
                            {
                                content = '<div class="mention_instagram_preloader"><a class="mention_instagram_img" id="mention_img_a_' + item['results_id'] + '" onclick="setResultAsVisited(' + item['results_id'] + ',' + item['kind'] + ')" target="_blank" href="' + href + '"><img src="'+item['mention_img_url']+'" onerror="imgErrorMention(' + item['results_id'] + ')" /></a></div>';
                            }

							if(!contentOryginal || contentOryginal == '' || contentOryginal == 'null')
							{
								content += '<p class="mention_text" id="mention_text_' + item['results_id'] + '">' + html_entity_decode(item['content'], 'ENT_QUOTES') + '</p>';
							}
							else
							{
								content += '<p class="mention_text" id="mention_text_' + item['results_id'] + '">' + html_entity_decode(item['content'], 'ENT_QUOTES') + ' <a href="javascript:showAllContent(' + item['results_id'] + ');" class="more">' + lp._('więcej') + ' &raquo;</a></p>' +
										'<p class="mention_text mention_text_oryginal" id="mention_text_oryginal_' + item['results_id'] + '">' + html_entity_decode(item['content_oryginal'], 'ENT_QUOTES') + ' <a href="javascript:hideAllContent(' + item['results_id'] + ');" class="more">&laquo; ' + lp._('mniej') + '</a></p>';
							}

                            sentimentSelectClass = '';
                            sentimentListHtml = '';

                            if (item['results_sentiment'] == null)
                            {
                                sentimentListHtml = '<option selected value="null">---</option>';
                            }

                            sentimentListHtml += '<option ';
                            if (item['results_sentiment'] == 0)
                            {
                                sentimentListHtml += 'selected';
                            }
                            sentimentListHtml += ' value="0">' + lp._('neutralny') + '</option>';
                            sentimentListHtml += '<option ';
                            if (item['results_sentiment'] == 1)
                            {
                                sentimentSelectClass = ' positive';
                                sentimentListHtml += 'selected';
                            }
                            sentimentListHtml += ' value="1">' + lp._('pozytywny') + '</option>';
                            sentimentListHtml += '<option ';
                            if (item['results_sentiment'] == 2)
                            {
                                sentimentSelectClass = ' negative';
                                sentimentListHtml += 'selected';
                            }

                            sentimentListHtml += ' value="2">' + lp._('negatywny') + '</option>';

                            var sentimentLiSelectorHtml = '<li id="li-mention-sentiment-' + item['results_id'] + '"';

                            //tworze select z sentymentem
                            if (item['results_sentiment'] == null)
                            {
                                sentimentLiSelectorHtml += ' style="display: none"';
                            }

                            sentimentLiSelectorHtml += '>';
                            sentimentListHtml = sentimentLiSelectorHtml +
                                '<select id="mention-sentiment-' + item['results_id'] + '" name="mention-sentiment-' + item['results_id'] + '" class="sentiment' + sentimentSelectClass + '" onchange="javascript:setSentimentInDB(' + item['results_id'] + ', this.value)">' +
                                sentimentListHtml +
                                '</select>' +
                                '</li>';



							groupsInfoHtml = '<a id="groupButtonResult_' + item['results_id'] + '" href=",[' +(item['groups_id'].length > 0 ? item['groups_id'] : -666) + ']);"></a>';
							if (item['groups_id'].length != 0)
							{
								groupsInfoHtml += '<li class="added-groups groupsListResult" id="groupsListResult_' + item['results_id']+'"><i class="fa fa-folder-o"></i> ' + lp._('Grupa') + ':';
								for (var x = 0; x < item['groups_id'].length; x++)
								{
									groupsInfoHtml += '<a href="javascript:scrollTabToTop();toggleFilter(\'gr\',' + item['groups_id'][x] + ')" class="mention_group_added_' + item['results_id'] + ' group_added group_added_' + item['groups_id'][x] + '" data-id="' + item['groups_id'][x] + '">' + xssProtect(item['groups_title'][x]) + '</a>';
								}
								groupsInfoHtml += '</li>';
							}
							else
							{
								groupsInfoHtml += '<li class="added-groups groupsListResult" id="groupsListResult_'+item['results_id']+'" style="display:none;"></li>';
							}

							//tworze liste grup
							var entryGroups =  -666;
							if (item['groups_id'].length > 0)
							{
								entryGroups = item['groups_id'];
							}
							groupsListHtml = '';
							if (groupsAddList.length > 0)
							{
								for (key in groupsAddList)
								{
									groupsListHtml += '<li class="mention-in-group-' + item['results_id'] + '-' + groupsAddList[key]['id'] + ($.inArray(groupsAddList[key]['id'], entryGroups) != -1 ? ' active' : '') + '"><a ' + ($.inArray(groupsAddList[key]['id'], entryGroups) != -1 ? 'href="javascript:deleteResultFromGroup(' + item['results_id'] + ',' + groupsAddList[key]['id'] + ')"' : 'href="javascript:addResultToGroup(' + item['results_id'] + ',' + groupsAddList[key]['id'] + ')"')+ '><i class="fa fa-folder-open"></i> <span>' + xssProtect(groupsAddList[key]['title']) + '</span></a></li>';
								}
							}

							//tworze statystyki interakcji
							interactionsStatsHtml = '';
							if ((activeInteractions == 1 && accessInteractions == 1) && ((item['shares_count'] != null) || (item['likes_count'] != null) || (item['dislikes_count'] != null) || (item['comments_count'] != null)))
							{
								var likesPrefix = '';
								var dislikesPrefix = '<p class="likes"><i class="fa fa-thumbs-o-down"></i>';
								if (item['socialmedia_sites_id'] == 1 || isMentionFromYoutube(item)) //facebook & youtube
								{
									likesPrefix = '<p class="likes"><i class="fa fa-thumbs-o-up"></i>';
								}
								else
								{
									likesPrefix = '<p class="favs"><i class="fa fa-heart"></i>';
								}

								if (item['likes_count'] != null && item['likes_count'] >= 0)
								{
									interactionsStatsHtml += likesPrefix + ' ' + item['likes_count'] + '</p>';
								}
								else
								{
									//gdy interakcje sa niemozliwe do sparsowania
									if (item['likes_count'] != -1)
									{
										interactionsStatsHtml += likesPrefix + ' 0</p>';
									}
								}

								if (isMentionFromYoutube(item) && !isYoutubeComment(item)) //youtube
								{
									if (item['dislikes_count'] != null && item['dislikes_count'] >= 0)
									{
										interactionsStatsHtml += dislikesPrefix + ' ' + item['dislikes_count'] + '</p>';
									}
									else
									{
										//gdy interakcje sa niemozliwe do sparsowania
										if (item['dislikes_count'] != -1)
										{
											interactionsStatsHtml += dislikesPrefix + ' 0</p>';
										}
									}
								}

								if (item['host'] != null && item['host'] != 'twitter.com' && !isYoutubeComment(item)) //dla twittera nie zbieramy komentarzy, a komentarze na youtube maja tylko ilosc polubien
								{
									var commentsPrefix = '<p class="comments"><i class="fa fa-comment-o"></i>';

									if (item['comments_count'] != null && item['comments_count'] >= 0)
									{
										interactionsStatsHtml += commentsPrefix +  ' ' + item['comments_count'] + '</p>';
									}
									else
									{
										//gdy interakcje sa niemozliwe do sparsowania
										if (item['comments_count'] != -1)
										{
											interactionsStatsHtml += commentsPrefix + ' 0</p>';
										}
									}
								}

								if (item['socialmedia_sites_id'] != 5 && item['socialmedia_sites_id'] != 4)
								{
									var sharesPrefix = '';
									if (item['socialmedia_sites_id'] == 2)
									{
										sharesPrefix = '<p class="retweets"><i class="fa fa-retweet"></i>';
									}
									else
									{
										sharesPrefix = '<p class="shares"><i class="fa fa-share-square-o"></i>';
									}

									if (item['shares_count'] != null && item['shares_count'] >= 0)
									{
										interactionsStatsHtml += sharesPrefix + ' ' + item['shares_count'] + '</p>';
									}
									else
									{
										//gdy interakcje sa niemozliwe do sparsowania
										if (item['shares_count'] != -1)
										{
											interactionsStatsHtml += sharesPrefix + ' 0</p>';
										}
									}
								}

								if (interactionsStatsHtml != '')
								{
									interactionsStatsHtml = '<li class="social-stats">' + interactionsStatsHtml + '</li>';
								}
							}
							else
							{
								if (!item['authors_id'] && item['influence_value'] != null && item['influence_value'] != '' && item['page_category'] != 4)
								{
									interactionsStatsHtml = '<li class="social-stats"><p class="visits" title="' + lp._('Miesięczna liczba wizyt na stronie') + '"><i class="fa fa-bar-chart"></i>  ' + item['influence_value'] + '</p></li>';
								}
							}
							
							//tworze element z wykresem influence score
							influenceScoreHtml = '';
							if (item['influence_value'] != null && item['influence_score'] != '')
							{
								influenceScoreHtml = '<li class="score">' + lp._('Influence Score') + ': <strong>';

								influenceScoreHtml += item['influence_score'] + '/10';

								influenceScoreHtml += '</strong>' +
													'<div class="influence-score">' +
														'<span style="width:' + item['influence_score'] + '0%"></span>' +
													'</div>' +
												'</li>';
							}

							//lista dodatkowych opcji
							moreOptionsListHtml = '';

							//oznacz / odznacz
                            moreOptionsListHtml += '<li class="mention_option_replied'+response+'" id="response_set_' + item['results_id'] + '" onclick="javascript:setResponse(' + item['results_id'] + ');"><a href="javascript:void(0);"><i class="fa fa-hand-o-up"></i> <span>' + responseText + '</span></a></li>';

							//powiadomienie na email
							if(show_result_notification_email == 1)
							{
								moreOptionsListHtml += '<li class="mention_option_result_notification_email"><a href="javascript:resultNotificationEmailOpen(' + item['results_id'] + ');"><i class="fa fa-envelope"></i> ' + lp._('Powiadom na email') + '</a></li>';
							}

							//blokuj
							if (blockedContentKind > 0)
							{
								if (!item['authors_id'])
								{
									moreOptionsListHtml += '<li><a href="javascript:blockSource(' + item['results_id'] + ',' + blockedContentKind + ');"><i class="fa fa-bell-slash"></i> ' + lp._('Blokuj stronę') + '</a></li>';
								}
								else
								{
									moreOptionsListHtml += '<li><a href="javascript:blockSource(' + item['results_id'] + ',' + blockedContentKind + ');"><i class="fa fa-bell-slash"></i> ' + lp._('Blokuj autora') + '</a></li>';
								}
							}

							//odwiedź profil
							if (item['authors_id'])
							{
								moreOptionsListHtml += '<li><a href="' + item['author_url'] + '" target="_blank"><i class="fa fa-user"></i> ' + lp._('Odwiedź profil autora') + '</a></li>';
							}

							if (isAdminLoggedIn == 1)
							{
								moreOptionsListHtml += '<li id="blogroll_' + item['results_id'] + '"><a href="javascript:sendBlogrollSuspicion(' + item['results_id'] + ');">' + lp._('Blogroll') + '</a></li>'+
														'<li><a href="javascript:showInfoAboutResults(' + item['results_id'] + ');">' + lp._('Info') + '</a></li>' +
														'<li><a href="javascript:showEditResultBox(' + item['results_id'] + ');">' + lp._('Edycja') + '</a></li>';
							}

							if (showExportMentionToPdf == 1)
							{
								moreOptionsListHtml += '<li><a href=" http://brand24.pl/result/site-to-pdf/?rid=' + item['results_id'] + '">' + lp._('Eksport do PDF') + '</a></li>';
							}

							if (isAdminLoggedIn == 1 && item['url'] && (item['url'].indexOf("#") > -1 || item['url'].indexOf("comment_id=") > -1))
							{
								moreOptionsListHtml += '<li style="color:green;">' + lp._('Komentarz') + '</li>';
							}

							if (item['results_sentiment'] === null)
                            {
                                moreOptionsListHtml += '<li><a href="javascript:showChangeSentimentDropdown(' + item['results_id'] + ');"><i class="fa fa-meh-o"></i> ' + lp._('Ustaw sentyment') + '</a></li>';
                            }

							mentionSiteClass = 'mention-site-site';
							if (item['authors_id'])
							{
								mentionSiteClass = 'mention-site-' + item['socialmedia_sites_id'];
							}

							html += '<section class="mention mention-box ' + mentionSiteClass + mentionVisitedClass + mentionSpamClass + mentionRestrictedClass + mentionResponseClass + '" id="mention_result_' + item['results_id'] + '">' +
										'<div class="header">' +
											'<div class="avatar" id="mention_avatar_' + item['results_id'] + '">' +
												'<a onclick="groupsTitle(' + item['results_id'] + ',' + item['kind'] + ')" target="_blank" href="' + href + '">' +
													avatarIcon +
												'</a>' +
											'</div>' +
											'<div class="author">' +
												'<div class="mention-title-box"><a id="mention_title_' + item['results_id'] + '" onclick="setResultAsVisited(' + item['results_id'] + ',' + item['kind'] + ')" target="_blank" href="' + href + '">' + titleIcon + title + '</a></div>' +
												genderInfo +
												followersCountInfo +
												'<div class="visited-box" id="visited-box-' + item['results_id'] + '">' + mentionVisitedInfo + '</div>' +
												'<span class="mention-source"><i class="fa fa-calendar"></i> ' + item['created_date'] + '</span>' +
											'</div>' +
											'<ul class="mention-information" id="mention-information-' + item['results_id'] + '">' +
												'<li><a onclick="setResultAsVisited(' + item['results_id'] + ',' + item['kind'] + ')" target="_blank" href="' + href + '" class="mention-source mention_source" hidden_source_url="' + hiddenSourceUrl + '"><i class="fa fa-share-alt"></i> ' + item['host'] + '</a></li>' +
												influenceScoreHtml +
                                                sentimentListHtml +
												interactionsStatsHtml +
											'</ul>' +
										'</div>' +
										'<div class="description">' +
											content +
                                			groupsInfoHtml +
										'</div>' +
										'<div class="options">' +
											'<ul>' +
												'<li class="featured"><a onclick="setResultAsVisited(' + item['results_id'] + ',' + item['kind'] + ')" target="_blank" href="' + href + '"><i class="fa fa-reply"></i> ' + lp._('Odpowiedz') + '</a></li>' +
												'<li><a href="' + historyHref + '"><i class="fa fa-comments"></i> ' + lp._('Historia') + '</a></li>' +
												'<li>' +
													'<a href="javascript:void(0);"><i class="fa fa-bookmark"></i> ' + lp._('Grupuj') + '</a>' +
													'<ul class="group-more groupsListResult" id="groupsListResult_' + item['results_id'] + '">' +
														'<div class="mention-groups-list" id="mention-groups-list-' + item['results_id'] + '" rel="' + item['results_id'] + '">' +
														groupsListHtml +
														'</div>' +
														'<div class="add-group">' +
															'<p>+ ' + lp._('Dodaj nową grupę') + '</p>' +
															'<div class="add-group-error" id="add-group-error-' + item['results_id'] + '"></div>' +
															'<input type="text" class="create-text mention-groups-list-add-name" id="mention-groups-list-add-name-' + item['results_id'] + '">' +
															'<input type="button" class="create-button" value="' + lp._('Dodaj') + '" onclick="javascript:createNewGroupFromResuls(' + item['results_id'] + ');">' +
														'</div>' +
													'</ul>' +
												'</li>' +
												'<li><a href="javascript:deleteResult(' + item['results_id'] + ',' + blockedContentKind + ');"><i class="fa fa-trash"></i> ' + lp._('Usuń') + '</a></li>' +
												'<li>' +
													'<a href="javascript:void(0);"><i class="fa fa-caret-down"></i> ' + lp._('Więcej') + '</a>' +
													'<ul class="options-more">' +
														moreOptionsListHtml +
													'</ul>' +
												'</li>' +
											'</ul>' +
											mentionSpamInfo +
											'<div class="select">' +
												'<input class="bulb_checkbox mention-checkbox" type="checkbox" id="bulb_checkbox_' + item['results_id'] + '" name="bulb_checkbox_' + item['results_id'] + '" value="' + item['results_id'] + '" data-kind="' + item['kind'] + '" />' +
											'</div>' +
										'</div>' +
									'</section>';
						});

					}
					else
					{
						html = generatePanelPagerFooter(null, null, this.value);
					}

					if (data.hasOwnProperty('pager') && data.pager)
					{
						page = parseInt(page);
						
						html = html + '<div class="clearfix"></div><div class="panel_pager"><table width="100%"><tr><td class="panel_pager_btns">';
						html = html + '<div class="clear-filters bottom"><a href="javascript:void(0);" class="btn-cta  btn-nonactive btn-clear-additional-filters" style="padding:8px; border-radius:4px;">' + lp._('Wyczyść filtrowanie') + '</a></div></td><td class="panel_pager_btns" align="right">';

						if (page > 1)
						{
							html = html + '<a href="javascript:scrollTabToTop();void(0)" onclick="page=1;searchResultsNew(0);">' + lp._('Pierwsza') + '</a>';
							html = html + '<a href="javascript:scrollTabToTop();void(0)" onclick="page=' + (page - 1) + ';searchResultsNew(0);">' + lp._('Poprzednia') + '</a>';
						}

						for (i=1; i<=data.pager; i++)
						{
							if ((i > page-4) && (i < page+4))
							{
								html = html + '<a class="page_no' + (i == page ? ' selected_page' : '') + '" href="javascript:scrollTabToTop();void(0)" onclick="page=' + i + ';searchResultsNew(0);">' + i + '</a>';
							}
						}

						if (page < data.pager) {
							html = html + '<a href="javascript:scrollTabToTop();void(0)" onclick="page=' + (page + 1) + ';searchResultsNew(0);">' + lp._('Następna') + '</a>';
							html = html + '<a href="javascript:scrollTabToTop();void(0)" onclick="page=' + data.pager + ';searchResultsNew(0);">' + lp._('Ostatnia') + ' [' + data.pager + ']</a>';
						}

						html = html + '<div class="goto-page"><input type="text" id="goto-page-nr" name="goto-page-nr" pattern="[0-9.]+" value=""><a href="javascript:scrollTabToTop();void(0)" id="goto-page-btn">' + lp._('idź') + ': &raquo;</a></div>';

						html = html + '</td></tr></table></div>';

						html = html + '<div class="bulk_options">' +
										'<table width="100%" cellpadding="0" cellspacing="0">' +
											'<tr>' +
												'<td class="bulk_meta">' + lp._('Opcje zbiorowe:') + '</td>' +
												'<td align="right" width="150"><div class="clear-filters"><a href="javascript:selectAllResults(1)" class="btn-light">' + lp._('Zaznacz wszystkie') + '</a></div></td>' +
												'<td align="right" width="55"><div id="response_set_multi" class="mention_option_replied" onclick="javascript:setResponseMulti();"><a href="javascript:void(0);"><i class="fa fa-hand-o-up"></i><br><span>' + lp._('Oznacz') + '</span></a></div></td>' +
												'<td align="right" width="55"><div id="visited_set_multi" class="mention_option_replied" onclick="javascript:setResultAsVisitedMulti();"><a href="javascript:void(0);"><i class="fa fa-eye"></i><br><span>' + lp._('Odwiedzony') + '</span></a></div></td>' +
												'<td align="right" width="55"><div class="mention_option_group" id="add_to_group_multi"><a href="javascript:addResultToGroupOpenForm();"><i class="fa fa-check-square-o"></i><br><span>' + lp._('Grupuj') + '</span></a></div></td>' +
												'<td align="right" width="55"><div class="mention_option_delete"><a href="javascript:deleteResultMulti(' + searches_id + ');"><i class="fa fa-remove"></i><br><span>' + lp._('Usuń') + '</span></a></div></td>' +
												'<td align="right" width="55"><div class="mention_option_sentiment" id="sentiment_set_multi"><a href="javascript:setSentiment(\'multi\')"><i class="fa fa-heart-o"></i><br><span>' + lp._('Sentyment') + '</span></a></div></td>' +
											'</tr>'+
										'</table>'+
									'</div>';
					}

					if (html == '')
					{
						$('#results_content').html(generatePanelPagerFooter(null, null, this.value));
						injectMentionOrderSelect();
					}
					else
					{
						$('#results_content').html(html);
						injectMentionOrderSelect();
						$('.mention-meta-interactions').css('width', '-=570px');
						$('#goto-page-btn').click(function (){
							var pageNumber = $('#goto-page-nr').val();
							if (pageNumber.length > 0)
							{
								page = parseInt(pageNumber);
								searchResults(searches_id);
							}
							return true;
						});
						$('#goto-page-nr').keydown(function (event){
							if (event.keyCode == 13)
							{
								var pageNumber = $('#goto-page-nr').val();
								if (pageNumber.length > 0)
								{
									page = parseInt(pageNumber);
									searchResults(searches_id);
									return true;
								}
							}
							if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57))
							{
								return false;
							}
							return true;
						});
					}

					if (params['te'])
					{
						$('#filter_input_te').val(xssProtect(urlDcode(params['te'])));
					}
					
					if (window.hasOwnProperty('mentionOrderSelect') && (params['or'] == 0 || params['or'] == 2)) {
						window.mentionOrderSelect.setValueByChoice(params['or']);
					}

					buttonClearAdditionalFiltersHandler();
				}

				if (data.hasOwnProperty('charts') && data.charts)
				{
					if (data.charts.tab_results_count)
					{
						$('.panel_results_counter').html('0');

						var all_main_chart = 0;
						for (key in data.charts.tab_results_count)
						{
							var item = data.charts.tab_results_count[key];
							all_main_chart = all_main_chart + parseInt(item['count']);
							$('#panel_results_counter_'+item['page_category']).html(item['count']);
						}

						$('#panel_results_counter_all').html(all_main_chart);
					}
				}

				if (search_on_begin == 0)
				{
					if (data.hasOwnProperty('charts') && data.charts)
					{
						//jesli jednak powróci pomysł ze skalowalnym wykresem - odkomentowac
						//var chartDisplayInterval = (data.charts.chart_display_type == 'months' ? 30 : (data.charts.chart_display_type == 'weeks' ? 7 : 1));
						var chartDisplayInterval = 1;
						var chart_main = data.charts.chart_main;
						var chart_sentiment = data.charts.chart_sentiment;
						var chart_main_ranges = data.charts.chart_main_ranges;
						var chart_likes_counts = data.charts.chart_likes_counts;
						var chart_shares_counts = data.charts.chart_shares_counts;
						var chart_comments_counts = data.charts.chart_comments_counts;
						var chartSources = null;
						var sourcesCount = 0;
						var chartPoints = chart_sentiment.sentiment_negative_count_per_day_chart.length;

						//jesli jednak powróci pomysł ze skalowalnym wykresem - odkomentowac
						//var chartDisplayIntervalMain = Math.ceil(chartPoints/15) * chartDisplayInterval;
						//var chartDisplayIntervalSentiment = Math.ceil(chartPoints/3.5) * chartDisplayInterval;
						var chartDisplayIntervalMain = Math.ceil(chartPoints/15) * (data.charts.chart_display_type == 'months' ? 31 : (data.charts.chart_display_type == 'weeks' ? 7 : 1));
						var chartDisplayIntervalSentiment = Math.ceil(chartPoints/3.5) * (data.charts.chart_display_type == 'months' ? 31 : (data.charts.chart_display_type == 'weeks' ? 7 : 1));

						//Wykres glowny - wyniki + zasieg social media
						var options = null;
						if ((!params['rt'] || $('#selected_category_1:checked').length || $('#selected_category_2:checked').length || $('#selected_category_4:checked').length || $('#selected_category_5:checked').length) && range_of_social_media_chart_permission == 1)
						{
							if (activeInteractions == 1 && accessInteractions == 1)
							{
								$('#show_interactions_series_btn').show();
								options = {
									 chart: {
							         renderTo: 'chart_main',
									 backgroundColor: '#ffffff',
									 marginTop: 10,
									 marginBottom: 20,
                                     borderRadius: 0,
                                     borderWidth: 0,
							      },
							      title: {
							         text: ''
							      },
							      xAxis: {
							         type: 'datetime',
							         //tickInterval: 48 * 3600 * 1000,
									 gridLineWidth: 1,
									 gridLineColor :'#e1e1e1',
									 gridLineDashStyle: 'dot',
							         title: {
							            text: null
							         }
							      },
							      yAxis: [{
							         title: {
							            text: null
							         },
		                             labels: {
		                				format: '{value}',
		                				style: {
		                    				color: '#0093dd'
		                				}
		            				},
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#efefef',
									 gridLineDashStyle: 'solid'
							      },
							      {
							         title: {
							            text: null
							         },
		                             labels: {
		                				format: '{value}',
		                				style: {
		                    				color: '#1AA98A'
		                				}
		            				},
		                			showEmpty: false,
		                			showLastLabel: false,
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#efefef',
									 gridLineDashStyle: 'solid',
		            				 opposite: true
							      },
							      {
							         title: {
							            text: null
							         },
		                             labels: {
		                				enabled: false
		            				},
		                			showEmpty: false,
		                			showLastLabel: false,
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#ffffff',
									 gridLineDashStyle: 'solid',
		            				 opposite: true
							      },
							      {
							         title: {
							            text: null
							         },
		                             labels: {
		                				enabled: false
		            				},
		                			showEmpty: false,
		                			showLastLabel: false,
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#ffffff',
									 gridLineDashStyle: 'solid',
		            				 opposite: true
							      },
							      {
							         title: {
							            text: null
							         },
		                             labels: {
		                				enabled: false
		            				},
		                			showEmpty: false,
		                			showLastLabel: false,
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#ffffff',
									 gridLineDashStyle: 'solid',
		            				 opposite: true
							      }],
								  tooltip: {
								  	 shared: true,
									 backgroundColor: '#ffffff',
									 borderColor: '#e3e3e3',
									 borderWidth: 1,
									 crosshairs: true,
									 useHTML: true,
                                     shadow: false,
									  style: {
										padding: 8,
										color: '#0FB36C'
									 },
									 formatter: function() {
									 	var pointDate = getChartPointDate(this.x);

									 	var txt = '';
									 	var tooltipTxt1 = '';
									 	var tooltipTxt2 = '';

									 	// Wyświetl datę niezależnie od konfiguracji filtrów.

									 	tooltipTxt1 += '<span style="color:#000; font-family:Roboto, sans-serif; font-size:12px; font-weight:500; line-height:32px">' + pointDate + '</span><br>';

									 	$.each(this.points, function(i, point) {

									 		if(point.series.color == '#0077cc')
											{
												tooltipTxt1 += '<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+lp._('Liczba Wyników')+'</span><br>'+
									   		    '<span style="color:#0077cc; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span><br>';
											}

									 		if(point.series.color == '#0FB36C')
											{
												tooltipTxt1 += '<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+lp._('Zasięg Social Media')+'</span><br>' +
												'<span style="color:#0FB36C; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span>';
											}
									 		if(point.series.color == '#9968e2')
											{
												tooltipTxt2 += '<span style="color:#9968e2">• </span> <span class="chart_interactions_likes" style="color:#666; font-family:Roboto, sans-serif; font-size:12px; font-weight:400;">'+lp._('Polubień')+': '+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span>';
											}
									 		if(point.series.color == '#b5e319')
											{
												tooltipTxt2 += ' <span style="color:#b5e319">• </span> <span class="chart_interactions_shares" style="color:#666; font-family:Roboto, sans-serif; font-size:12px; font-weight:400;">'+lp._('Udostępnień')+': '+ Highcharts.numberFormat(point.y, 0, null, ' ') +' </span>';
											}
									 		if(point.series.color == '#dfbc52')
											{
												tooltipTxt2 += ' <span style="color:#dfbc52">• </span> <span class="chart_interactions_comments" style="color:#666; font-family:Roboto, sans-serif; font-size:12px; font-weight:400;">'+lp._('Komentarzy')+': '+ Highcharts.numberFormat(point.y, 0, null, ' ') +' </span>';
											}

										});

										txt = tooltipTxt1;
										if(tooltipTxt2 != ''){
											if(tooltipTxt1 == '')
											{
												txt += '<span style="color:#000; font-family:Roboto, sans-serif; font-size:12px; font-weight:500; line-height:32px">'+pointDate+'</span><br>';
											}
											txt += '<br><div class="chart_interactions">' + tooltipTxt2 + '</div>';
										}

										return txt;

									 }
								  },
								      plotOptions: {
								         area: {
								            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
								         	pointStart: Date.UTC(2010, 12, 17),
								            marker: {
								                fillColor: '#ffffff',
												lineColor: '#ba4cfe',
												lineWidth: 2,
												symbol: 'circle',
												radius: 3
								            }
								         },
							         	series: {
							            cursor: 'pointer',
							            point: {
							                events: {
							                    click: function() {
													resultsChartPointClick(this.x);
							                    }
							                }
							            }
							         }
							      },
							        legend: {
							            align: 'top',
							            x: 30,
							            verticalAlign: 'top',
							            y: -15,
							            floating: false,
                                        borderRadius: 0,
                                        borderWidth: 0,
							            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
							        },
								  credits: {
									  enabled:false,
								  },
							      series: [{
								  	yAxis: 0,
							         name: lp._('Liczba Wyników'),
									 color: '#0077cc',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#1d99f2',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#0077cc',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#0077cc',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         },
							         {
								  	yAxis: 1,
							         name: lp._('Zasięg Social Media'),
									 color: '#0FB36C',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#0FB36C',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#0FB36C',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#0FB36C',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         },{
								  	yAxis: 2,
							         name: lp._('Liczba Polubień'),
									 color: '#9968e2',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#9968e2',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#9968e2',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#9968e2',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         },{
								  	yAxis: 3,
							         name: lp._('Liczba Udostępnień'),
									 color: '#b5e319',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#b5e319',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#b5e319',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#b5e319',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         },{
								  	yAxis: 4,
							         name: lp._('Liczba Komentarzy'),
									 color: '#dfbc52',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#dfbc52',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#dfbc52',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#dfbc52',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         }],
							         exporting: {
									    buttons: {
									        exportButton: {
									            enabled:false
									        },
									        printButton: {
									            enabled:false
									        }

									    }
									},

								}

								//alert(chart_main.pages_count_per_day_chart);
							    options.series[0].data = chart_main.pages_count_per_day_chart;
								options.series[1].data = chart_main_ranges.count_per_day_chart;
								options.series[2].data = chart_likes_counts.count_per_day_chart;
								options.series[3].data = chart_shares_counts.count_per_day_chart;
								options.series[4].data = chart_comments_counts.count_per_day_chart;


								options.series[0].pointStart = Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month,chart_main.pointStart.day);
								options.series[1].pointStart = Date.UTC(chart_main_ranges.pointStart.year, chart_main_ranges.pointStart.month, chart_main_ranges.pointStart.day);
								options.series[2].pointStart = Date.UTC(chart_likes_counts.pointStart.year, chart_likes_counts.pointStart.month, chart_likes_counts.pointStart.day);
								options.series[3].pointStart = Date.UTC(chart_shares_counts.pointStart.year, chart_shares_counts.pointStart.month, chart_shares_counts.pointStart.day);
								options.series[4].pointStart = Date.UTC(chart_comments_counts.pointStart.year, chart_comments_counts.pointStart.month, chart_comments_counts.pointStart.day);


								switch (chart_display_type)
								{
									case 'days':
										options.series[0].pointInterval = 24 * 3600 * 1000;
										options.series[1].pointInterval = 24 * 3600 * 1000;
										options.series[2].pointInterval = 24 * 3600 * 1000;
										options.series[3].pointInterval = 24 * 3600 * 1000;
										options.series[4].pointInterval = 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 24 * 3600 * 1000;

									break;
									case 'weeks':
										options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
										options.series[1].pointInterval = 7 * 24 * 3600 * 1000;
										options.series[2].pointInterval = 7 * 24 * 3600 * 1000;
										options.series[3].pointInterval = 7 * 24 * 3600 * 1000;
										options.series[4].pointInterval = 7 * 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 7 * 24 * 3600 * 1000;

									break;
									case 'months':
										options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
										options.series[1].pointInterval = 31 * 24 * 3600 * 1000;
										options.series[2].pointInterval = 31 * 24 * 3600 * 1000;
										options.series[3].pointInterval = 31 * 24 * 3600 * 1000;
										options.series[4].pointInterval = 31 * 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 31 * 24 * 3600 * 1000;

									break;
								}
								mchart = new Highcharts.Chart(options);

								if(visibleInteractionsSeries)
								{
									$('#show_interactions_series_btn').html('<i class="fa fa-exchange"></i> ' + lp._('Ukryj interakcje'));
                                    //if voi (visible one interaction serie) is set
									if (params['voi'])
									{
                                        mchart.series[0].hide();
                                        mchart.series[1].hide();
                                        //show only likes
                                        if (params['voi'] == 'l')
                                        {
                                            mchart.series[2].show();
                                        }
                                        else
										{
                                            mchart.series[2].hide();
										}
										//show only shares
                                        if (params['voi'] == 's')
                                        {
                                            mchart.series[3].show();
                                        }
                                        else
                                        {
                                            mchart.series[3].hide();
                                        }
                                        //show only comments
                                        if (params['voi'] == 'c')
                                        {
                                            mchart.series[4].show();
                                        }
                                        else
                                        {
                                            mchart.series[4].hide();
                                        }
									}
									else
									{
                                        mchart.series[2].show();
                                        mchart.series[3].show();
                                        mchart.series[4].show();
                                    }
								}
								else
								{
									$('#show_interactions_series_btn').html('<i class="fa fa-exchange"></i> ' + lp._('Pokaż interakcje'));
									mchart.series[2].hide();
									mchart.series[3].hide();
									mchart.series[4].hide();
								}
							}
							else
							{
                				if (activeInteractions == 1)
								{
									$('#show_interactions_series_btn').html('<i class="fa fa-exchange"></i> ' + lp._('Pokaż interakcje'));
									$('#show_interactions_series_btn').show();
								}


								options = {
									 chart: {
							         renderTo: 'chart_main',
									 backgroundColor: '#ffffff',
									 marginTop: 10,
									 marginBottom: 20,
                                     marginRight: 33,
                                     borderRadius: 0,
                                     borderWidth: 0,
							      },
							      title: {
							         text: ''
							      },
							      xAxis: {
							         type: 'datetime',
							         //tickInterval: 48 * 3600 * 1000,
									 gridLineWidth: 1,
									 gridLineColor :'#e1e1e1',
									 gridLineDashStyle: 'dot',
							         title: {
							            text: null
							         }
							      },
							      yAxis: [{
							         title: {
							            text: null
							         },
		                             labels: {
		                				format: '{value}',
		                				style: {
		                    				color: '#0077cc'
		                				}
		            				},
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#efefef',
									 gridLineDashStyle: 'solid'
							      },
							      {
							         title: {
							            text: null
							         },
		                             labels: {
		                				format: '{value}',
		                				style: {
		                    				color: '#1aa98a'
		                				}
		            				},
		                			showEmpty: false,
		                			showLastLabel: false,
									 min: 0,
									 gridLineWidth: 1,
									 gridLineColor :'#efefef',
									 gridLineDashStyle: 'solid',
		            				 opposite: true
							      }],
								  tooltip: {
								  	 shared: true,
									 backgroundColor: '#ffffff',
									 borderColor: '#e3e3e3',
									 borderWidth: 1,
									 crosshairs: true,
									 useHTML: true,
									 shadow: false,
									 style: {
										padding: 8,
										color: '#1aa98a',
									 },
									 formatter: function() {
										 var pointDate = getChartPointDate(this.x);

									 	var txt = '';

									 	$.each(this.points, function(i, point) {
									 		if(point.series.color == '#0077cc')
											{
												txt += '<span style="color:#000; font-family:Roboto, sans-serif; font-size:12px; font-weight:500; line-height:32px">'+pointDate+'</span><br>'+
													'<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+lp._('Liczba Wyników')+'</span><br>'+
									   		   		'<span style="color:#0277cb; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span><br>';
											}
									 		if(point.series.color == '#1aa98a')
											{
												txt += '<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:500; line-height:32px">'+lp._('Zasięg Social Media')+'</span><br>' +
											   		'<span style="color:#1aa98a; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span><br>';
											}
										});

									    return txt;

									 }
								  },
								      plotOptions: {
								         area: {
								            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
								         	pointStart: Date.UTC(2010, 12, 17),
								            marker: {
								                fillColor: '#ffffff',
												lineColor: '#ba4cfe',
												lineWidth: 2,
												symbol: 'circle',
												radius: 3
								            }
								         },
							         	series: {
							            cursor: 'pointer',
							            point: {
							                events: {
							                    click: function() {
													resultsChartPointClick(this.x);
							                    }
							                }
							            }
							         }
							      },
							        legend: {
							            align: 'top',
							            x: 30,
							            verticalAlign: 'top',
							            y: -15,
							            floating: false,
                                        borderRadius: 0,
                                        borderWidth: 0,
							            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
							        },
								  credits: {
									  enabled:false,
								  },
							      series: [{
								  	yAxis: 0,
							         name: lp._('Liczba Wyników'),
									 color: '#0077cc',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#0077cc',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#0077cc',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#0077cc',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         },
							         {
								  	yAxis: 1,
							         name: lp._('Zasięg Social Media'),
									 color: '#1aa98a',
									 type: 'line',
									 plotShadow: false,
									 	shadow: false,
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	//pointStart: Date.UTC(2010, 12, 17),
										lineColor: '#1aa98a',
										lineWidth: 2,
							            marker: {
							               		 states: {
												   hover: {
													  enabled: true,
		                                              fillColor: '#ffffff',
												      lineColor: '#1aa98a',
												      lineWidth: 2,
													  radius: 4
												   },
												},
												fillColor: '#1aa98a',
												lineColor: '#ffffff',
												lineWidth: 2,
												symbol: 'circle',
												radius: 0
							            },
							           data: []
							         }],
							         exporting: {
									    buttons: {
									        exportButton: {
									            enabled:false
									        },
									        printButton: {
									            enabled:false
									        }

									    }
									},

								}

								//alert(chart_main.pages_count_per_day_chart);
							    options.series[0].data = chart_main.pages_count_per_day_chart;
								options.series[1].data = chart_main_ranges.count_per_day_chart;


								options.series[0].pointStart = Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month,chart_main.pointStart.day);
								options.series[1].pointStart = Date.UTC(chart_main_ranges.pointStart.year, chart_main_ranges.pointStart.month, chart_main_ranges.pointStart.day);

								switch (chart_display_type)
								{
									case 'days':
										options.series[0].pointInterval = 24 * 3600 * 1000;
										options.series[1].pointInterval = 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 24 * 3600 * 1000;

									break;
									case 'weeks':
										options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
										options.series[1].pointInterval = 7 * 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 7 * 24 * 3600 * 1000;

									break;
									case 'months':
										options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
										options.series[1].pointInterval = 31 * 24 * 3600 * 1000;


										options.xAxis.tickInterval = chartDisplayIntervalMain * 31 * 24 * 3600 * 1000;

									break;
								}
								mchart = new Highcharts.Chart(options);
							}
						}
						else
						{
						 	options = {
								 chart: {
						         renderTo: 'chart_main',
								 backgroundColor: '#ffffff',
								 marginTop: 10,
								 marginBottom: 20,
                                 marginRight: 33,
								 borderRadius: 0,
                                 borderWidth: 0,
						      },
						      title: {
						         text: ''
						      },
						      xAxis: {
						         type: 'datetime',
						         //tickInterval: 48 * 3600 * 1000,
								 gridLineWidth: 1,
								 gridLineColor :'#e1e1e1',
								 gridLineDashStyle: 'dot',
						         title: {
						            text: null
						         }
						      },
						      yAxis: {
						         title: {
						            text: null
						         },
	                             labels: {
	                				format: '{value}',
	                				style: {
	                    				color: '#0077cc'
	                				}
	            				},
								 min: 0,
								 gridLineWidth: 1,
								 gridLineColor :'#efefef',
								 gridLineDashStyle: 'solid'
						      },
							  tooltip: {
								 backgroundColor: '#ffffff',
								 borderColor: '#e3e3e3',
								 borderWidth: 1,
								 crosshairs: true,
								 useHTML: true,
								 shadow: false,
								 style: {
									padding: 8,
									color: '#1aa98a',
								 },
								 formatter: function() {
									 var pointDate = getChartPointDate(this.x);

								    return '<span style="color:#000; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+pointDate+'</span><br>'+
											'<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:500; line-height:32px">'+lp._('Liczba Wyników')+'</span><br>'+
										   '<span style="color:#0277cb; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</span><br>';
								 }
							  },
							      plotOptions: {
							         area: {
							            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
							         	pointStart: Date.UTC(2010, 12, 17),
							            marker: {
							                fillColor: '#ffffff',
											lineColor: '#ba4cfe',
											lineWidth: 2,
											symbol: 'circle',
											radius: 3
							            }
							         },
						         	series: {
						            cursor: 'pointer',
						            point: {
						                events: {
						                    click: function() {
												resultsChartPointClick(this.x);
						                    }
						                }
						            }
						         }
						      },
						        legend: {
						            align: 'top',
						            x: 30,
						            verticalAlign: 'top',
						            y: -15,
						            floating: false,
                                    borderRadius: 0,
                                    borderWidth: 0,
						            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
						        },
							  credits: {
								  enabled:false,
							  },
						      series: [{
							  	yAxis: 0,
						         name: lp._('Liczba Wyników'),
								 color: '#0077cc',
								 type: 'line',
								 plotShadow: false,
								 	shadow: false,
						            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
						         	//pointStart: Date.UTC(2010, 12, 17),
									lineColor: '#0077cc',
									lineWidth: 2,
						            marker: {
						               		 states: {
											   hover: {
												  enabled: true,
	                                              fillColor: '#ffffff',
											      lineColor: '#0077cc',
											      lineWidth: 2,
												  radius: 4
											   },
											},
											fillColor: '#0077cc',
											lineColor: '#ffffff',
											lineWidth: 2,
											symbol: 'circle',
											radius: 0
						            },
						           data: []
						         }],
						         exporting: {
								    buttons: {
								        exportButton: {
								            enabled:false
								        },
								        printButton: {
								            enabled:false
								        }

								    }
								},
							}

							//alert(chart_main.pages_count_per_day_chart);
						    options.series[0].data = chart_main.pages_count_per_day_chart;


							options.series[0].pointStart = Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month,chart_main.pointStart.day);


							switch (chart_display_type)
							{
								case 'days':
									options.series[0].pointInterval = 24 * 3600 * 1000;


									options.xAxis.tickInterval = chartDisplayIntervalMain * 24 * 3600 * 1000;

								break;
								case 'weeks':
									options.series[0].pointInterval = 7 * 24 * 3600 * 1000;


									options.xAxis.tickInterval = chartDisplayIntervalMain * 7 * 24 * 3600 * 1000;

								break;
								case 'months':
									options.series[0].pointInterval = 31 * 24 * 3600 * 1000;


									options.xAxis.tickInterval = chartDisplayIntervalMain * 31 * 24 * 3600 * 1000;

								break;
							}
							mchart = new Highcharts.Chart(options);
						}

						$('#main_charts_help').show();
            $('#main_charts_export').show();
            showChartExportMenu();

						//Wykres sentymentu
						var options = {
							 chart: {
					         renderTo: 'chart_main_sentiment',
							 backgroundColor: '#ffffff',
							 marginTop: 20,
							 marginBottom: 20,
							 marginRight: 33,
                             borderRadius: 0,
                             borderWidth: 0,
					      },
					      title: {
					         text: ''
					      },
					      xAxis: {
					         type: 'datetime',
					         //tickInterval: 48 * 3600 * 1000,
							 gridLineWidth: 1,
							 gridLineColor :'#e1e1e1',
							 gridLineDashStyle: 'dot',
					         title: {
					            text: null
					         }
					      },
					      yAxis: {
					         title: {
					            text: null
					         },
							 min: 0,
							 gridLineWidth: 1,
							 gridLineColor :'#efefef',
							 gridLineDashStyle: 'solid',
					         labels: {
					            formatter: function() {
					               return Highcharts.numberFormat(this.value, 0, null, ' ');
					            }
					         }
					      },
						  tooltip: {
							  	 shared: true,
								 backgroundColor: '#ffffff',
								 borderColor: '#e3e3e3',
								 borderWidth: 1,
								 crosshairs: true,
								 useHTML: true,
								 shadow: false,
								 style: {
									padding: 8,
									color: '#45d52b',
								 },
							 formatter: function() {
								 var pointDate = getChartPointDate(this.x);

									 var txt = '<span style="color:#000; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+pointDate+'</span><br>';

									 $.each(this.points, function(i, point) {
										if(point.series.color == '#b5e319')
										{
											txt += '<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+lp._('Liczba Pozytywnych')+'</span><br>'+
                                            '<span style="color:#b5e319; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span><br>';
										}
								 		if(point.series.color == '#fa2231')
										{
											txt += '<span style="color:#a1a1a1; font-family:Roboto, sans-serif; font-size:12px; font-weight:400; line-height:28px;">'+lp._('Liczba Negatywnych')+'</span><br>'+
                                                '<span style="color:#fa2231; font-family:Roboto, sans-serif; font-size:22px; font-weight:300;">'+ Highcharts.numberFormat(point.y, 0, null, ' ') +'</span><br>';
										}
									});

									return txt;
							 }
						  },
						      plotOptions: {
						         area: {
						            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
						         	pointStart: Date.UTC(2010, 12, 17),
						            marker: {
						                fillColor: '#ffffff',
										lineColor: '#ba4cfe',
										lineWidth: 2,
										symbol: 'circle',
										radius: 3
						            }
						         },
					         	series: {
					            cursor: 'pointer',
					            point: {
					                events: {
					                    click: function() {
											resultsChartPointClick(this.x);
					                    }
					                }
					            }
					         }
					      },
						        legend: {
						            align: 'top',
						            x: 30,
						            verticalAlign: 'top',
						            y: -15,
						            floating: false,
                                    borderRadius: 0,
                                    borderWidth: 0,
						            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
						        },
						  credits: {
							  enabled:false,
						  },
					      series: [{
					             name: lp._('Liczba Pozytywnych'),
					             cursor: 'pointer',
					    		 color: '#b5e319',
					    		 type: 'line',
					    		 plotShadow: false,
					    		 	shadow: false,
					                pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
					             	pointStart: Date.UTC(2010, 12, 17),
									lineColor: '#b5e319',
									lineWidth: 2,
						            marker: {
						               		 states: {
											   hover: {
												  enabled: true,
	                                              fillColor: '#ffffff',
											      lineColor: '#b5e319',
											      lineWidth: 2,
												  radius: 4
											   },
											},
											fillColor: '#b5e319',
											lineColor: '#ffffff',
											lineWidth: 2,
											symbol: 'circle',
											radius: 0
						            },
					             point: {
										events: {
											click: function() {
												resultsChartPointClick(this.x);
											}
										}
								 },
					             data: []
					          },
				    	  {
				             name: lp._('Liczba Negatywnych'),
				             cursor: 'pointer',
				    		 color: '#fa2231',
				    		 type: 'line',
				    		 plotShadow: false,
				    		 	shadow: false,
				                pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
				             	pointStart: Date.UTC(2010, 12, 17),
									lineColor: '#ff0000',
									lineWidth: 2,
						            marker: {
						               		 states: {
											   hover: {
												  enabled: true,
	                                              fillColor: '#ffffff',
											      lineColor: '#fa2231',
											      lineWidth: 2,
												  radius: 4
											   },
											},
											fillColor: '#fa2231',
											lineColor: '#ffffff',
											lineWidth: 2,
											symbol: 'circle',
											radius: 0
						            },
					              point: {
										events: {
											click: function() {
												resultsChartPointClick(this.x);
											}
										}
								 },
				             data: []
				          }],
								 exporting: {
            						enabled: false
            					}
							}

						//alert(chart_main.pages_count_per_day_chart);
						options.series[0].data = chart_sentiment.sentiment_positive_count_per_day_chart;
						options.series[1].data = chart_sentiment.sentiment_negative_count_per_day_chart;


						options.series[0].pointStart = Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month,chart_main.pointStart.day);
						options.series[1].pointStart = Date.UTC(chart_main.pointStart.year, chart_main.pointStart.month,chart_main.pointStart.day);

						switch (chart_display_type)
						{
							case 'days':
								options.series[0].pointInterval = 24 * 3600 * 1000;
								options.series[1].pointInterval = 24 * 3600 * 1000;

								options.xAxis.tickInterval = chartDisplayIntervalMain * 24 * 3600 * 1000;

							break;
							case 'weeks':
								options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 7 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = chartDisplayIntervalMain * 7 * 24 * 3600 * 1000;

							break;
							case 'months':
								options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 31 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = chartDisplayIntervalMain * 31 * 24 * 3600 * 1000;

							break;
						}

						schart = new Highcharts.Chart(options);

						if(visibleSentimentChart)
						{
							$('#chart_main_sentiment').hide();
						}
						else
						{
							$('#chart_main_sentiment').show();
						}
						showSentimentChart(); //zaladuj wykres sentymentu jesli byl wlaczony

						if(params['hmc'] == 1){
							$('main_charts').hide();
							showMainChart();
						}

						if(sourcesCount>0)
						{
							//Wykres zrodel
							var chart;
						   chart = new Highcharts.Chart({
							  chart: {
							 renderTo: 'chart_sources',
							 plotBackgroundColor: null,
							 plotBorderWidth: null,
							 plotShadow: true
							  },
							  colors: [
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#354863'],[1, '#354863']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#82d008'],[1, '#82d008']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#11a258'],[1, '#11a258']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#3fe390'],[1, '#3fe390']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#447d74'],[1, '#447d74']]},
							   {linearGradient: [0, '20%', 0, '70%'], stops: [[0, '#c12156'],[1, '#c12156']]},
							  ],


							  /*colors: ["#005d9d", "#55BF3B", "#ED561B", "#7798BF", "#aaeeee", "#cc2065", "#eeaaee", "#e7c110", "#DF5353", "#7798BF", "#b1defd"],*/
							  title: {
							 text: ''
							  },
							  tooltip: {
							 	backgroundColor: '#5f6264',
										 borderColor: '#5f6264',
										 borderWidth: 0,
										 shadow: false,
										 style: {
											padding: 8,
											color: '#ffffff',
										 },
								formatter: function() {
								return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
							 }
							  },
							  legend: {
										 enabled:true,
										 align: 'right',
										 layout: 'vertical',
										 verticalAlign: 'middle',
										 symbolWidth: 8,
										 borderWidth: 0,
										 symbolPadding: 15,
										 itemStyle: {
												color: '#7e7e7e',
												fontFamily: 'verdana',
												fontSize: '9px',
												lineHeight: '15px',
										 },
										 labelFormatter: function() {
											return '<span>'+this.y +'</span>%<span style="color:#ffffff; font-size:9px;"> - </span><b> '+this.name+'</b>'
										 }
							  },
							  plotOptions: {
							 pie: {
								innerSize: '68%',
								borderColor: '#eeeeee',
								borderWidth: '2',
								size: '100%',
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
								   enabled: false
								},
								showInLegend: true
							 }
							  },
							  credits: {
								 enabled:false,
							  },
							   series: [{
							 type: 'pie',
							 name: 'Browser share',
							 data: chartSourcesData
							  }],
							 exporting: {
            					enabled: false
            				 }
						   });
			   			}
			   			else{
							$('#chart_sources').html('<div align="center">'+lp._('Brak wyników')+'</div>');
			   			}

					}
				}

				//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery - dodatkowo pobieramy liczbe wyników z ostatnich 30 dni uzupełniamy box z loaderkiem
				if(data.hasOwnProperty('resultsCountFromLastMonth') && projectHasJustAdded && data.resultsCountFromLastMonth)
				{
					//jeśli więcej niż 100 wyników odkrywamy wykres
					if(data.resultsCountFromLastMonth > 100)
					{
						$('#panel_results_loader').removeClass('hidden-chart');
					}

					//poprawiam liczbę wyników - jesli > 0
					if (data.resultsCountFromLastMonth > 0)
					{
						$('#results-loading-box-count').html(data.resultsCountFromLastMonth);
						$('#loader-no-of-results-text').show();
					}
					else
					{
						$('#loader-no-of-results-text').hide();
					}
				}

				search_on_begin = 0;

				$(".help_qtip_ajax").qtip();
			},
		error:
			function()
			{
				prepareDialogBox(2,lp._('Wystąpił błąd podczas pobierania wyników'));
				var buttonsReload = {};
				buttonsReload[lp._('Spróbuj ponownie')] = function(){
					searchResults();
					$(this).dialog('close');
				};
				$("#dialog-window").dialog({
					resizable: false,
					height: 170,
					width: 300,
					modal: true,
					buttons: buttonsReload
				});
			}
	}));



}

function chartFilter(chart,step)
{
	if (chart == 'count')
		$('#chart_main').html('<div class="panel_chart_loader_meta" align="center">'+lp._('Wczytywanie danych')+'...</div><div align="center" class="panel_chart_loader_gif"><img src="/static/img/chart-loader.gif" /></div>'); else
			$('#chart_sentyment').html('<div class="panel_chart_loader_meta" align="center">'+lp._('Wczytywanie danych')+'...</div><div align="center" class="panel_chart_loader_gif"><img src="/static/img/chart-loader.gif" /></div>');

	if (chart == 'count')
	{
		$('#chart_main_time_days').removeClass('chart_main_time_selected');
		$('#chart_main_time_weeks').removeClass('chart_main_time_selected');
		$('#chart_main_time_months').removeClass('chart_main_time_selected');
		$('#chart_main_time_'+step).addClass('chart_main_time_selected');
	} else
	{
		$('#chart_main_time_sentiment_days').removeClass('chart_main_time_selected');
		$('#chart_main_time_sentiment_weeks').removeClass('chart_main_time_selected');
		$('#chart_main_time_sentiment_months').removeClass('chart_main_time_selected');
		$('#chart_main_time_sentiment_'+step).addClass('chart_main_time_selected');
	}

	$.post('/panel/getchartdata', {chart:chart,step:step,searches_id:searches_id,only_valuable:only_valuable},
			function(data)
			{
				//alert(data);
				if (chart == 'count')
				{
					var options = {
							 chart: {
				         renderTo: 'chart_main',
						 backgroundColor: '#ffffff',
						 marginTop: 20,
						 marginBottom: 20,
				      },
				      title: {
				         text: ''
				      },
				      xAxis: {
				         type: 'datetime',
				         tickInterval: 48 * 3600 * 1000,
				         //604800000
						 gridLineWidth: 1,
						 gridLineColor :'#bbbbbb',
						 gridLineDashStyle: 'dot',
				         title: {
				            text: null
				         }
				      },
				      yAxis: {
				         title: {
				            text: null
				         },
						 min: 0,
						 gridLineWidth: 1,
						 gridLineColor :'#bbbbbb',
						 gridLineDashStyle: 'dot',
				         labels: {
				            formatter: function() {
				               return this.value;
				            }
				         }
				      },
					  tooltip: {
						 backgroundColor: '#5f6264',
							 borderColor: '#5f6264',
							 borderWidth: 0,
							 crosshairs: true,
							 shadow: false,
							 style: {
								padding: 8,
								color: '#ffffff',
							 },
						 formatter: function() {
								   return '<span style="font-size:10px">'+Highcharts.dateFormat('%A, %B %e, %Y', this.x)+'</span><br/>'+
										   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
						 }
					  },
				      plotOptions: {
				         area: {
				            pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
				         	pointStart: Date.UTC(2010, 12, 17),
				            marker: {
				               lineWidth: 3,
				               lineColor: '#ba4cfe',
							   radius: 4
				            }
				         }
				      },
					  legend: {
				         enabled:false,
				      },
					  credits: {
						  enabled:false,
					  },
				      series: [{
						 name: 'Stats_Area',
						 color: '#e6f2fa',
						 type: 'area',
						 plotShadow: false,
						 shadow: false,
						 pointInterval: 24 * 3600 * 1000,
						 lineColor: '#e6f2fa',
						 lineWidth: 0,
						 marker: {
							 lineWidth: 0,
							 lineColor: '#ffffff',
							 radius: 0
						 },
						 data: []
					  }, {
				         name: 'Stats',
						 color: '#0077cc',
						 type: 'line',
						 plotShadow: false,
						 	shadow: false,
				            pointInterval: 24 * 3600 * 1000,
				         	//pointStart: Date.UTC(2010, 12, 17),
							lineColor: '#0077cc',
							lineWidth: 3,
				            marker: {
				               lineWidth: 3,
				               lineColor: '#ffffff',
							   radius: 6
				            },
				           data: []
				         }]
						}


				    options.series[0].data = data.chart_data;
					options.series[1].data = data.chart_data;

					switch (step)
					{
						case 'days':
							options.tooltip.formatter = function() {
								   return '<span style="font-size:10px">'+Highcharts.dateFormat('%A, %B %e, %Y', this.x)+'</span><br/>'+
										   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
						 	};
							//options.series[0].pointInterval = 24 * 3600 * 1000;
							//options.series[1].pointInterval = 24 * 3600 * 1000;
							//options.xAxis.tickInterval = 24 * 3600 * 1000;
							options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							break;
						case 'weeks':
							options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
							options.series[1].pointInterval = 7 * 24 * 3600 * 1000;
							options.xAxis.tickInterval = 7 * 24 * 3600 * 1000;
							options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							options.tooltip.formatter = function() {
								   return '<span style="font-size:10px">'+Highcharts.dateFormat('%B %e, %Y', this.x-86400000)+' - '+Highcharts.dateFormat('%B %e, %Y', (this.x+432000000))+'</span><br/>'+
										   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
									//86400000 - 1 dzien w milisekundach
									//432000000 - 5 dni w milisekundach
									//w powyższym tooltipie jest specjalny mnożnik 518400000 który dolicza 6 dni w milisekundach. dzięki temu możemy pokazywać daty np. 9 kwietnia - 16 kwietnia.
						 	};
							break;
						case 'months':
							options.tooltip.formatter = function() {
								   return '<span style="font-size:10px">'+Highcharts.dateFormat('%B, %Y', this.x)+'</span><br/>'+
										   '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
						 	};
							//options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
							options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
							options.series[1].pointInterval = 31 * 24 * 3600 * 1000;
							options.xAxis.tickInterval = 31 * 24 * 3600 * 1000;
							options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
							break;
					}
				} else
				{
					if (whitelabel == '')
					{
						var options = {
							chart: {
				             renderTo: 'chart_sentyment',
				    		 backgroundColor: '#ffffff',
				    		 marginTop: 20,
				    		 marginBottom: 20,
				          },
				          title: {
				             text: ''
				          },
				          xAxis: {
				             type: 'datetime',
				             tickInterval: 7 * 24 * 3600 * 1000, // one week
				    		 gridLineWidth: 1,
				    		 gridLineColor :'#bbbbbb',
				    		 gridLineDashStyle: 'dot',
				             title: {
				                text: null
				             }
				          },
				          yAxis: {
				             title: {
				                text: null
				             },
				    		 min: 0,
				    		 gridLineWidth: 1,
				    		 gridLineColor :'#bbbbbb',
				    		 gridLineDashStyle: 'dot',
				             labels: {
				                formatter: function() {
				                   return this.value;
				                }
				             }
				          },
				    	  tooltip: {
				             formatter: function() {
				                       return '<b>'+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b> '+lp._('wpisów')+'<br/>'+
				                   Highcharts.dateFormat('%A, %B %e, %Y', this.x);
				             }
				          },
				          plotOptions: {
				             area: {
				                pointInterval: chartDisplayInterval * 24 * 3600 * 1000,
				             	pointStart: Date.UTC(2010, 12, 17),
				                marker: {
				                   lineWidth: 3,
				                   lineColor: '#ba4cfe',
				    			   radius: 4
				                }
				             }
				          },
				    	  legend: {
				             enabled:false,
				          },
				    	  credits: {
				    		  enabled:false,
				    	  },
				          series: [{
				             name: 'Pos',
				    		 color: '#b5e319',
				    		 type: 'line',
				    		 plotShadow: false,
				    		 	shadow: false,
				                pointInterval: 24 * 3600 * 1000,
				             	pointStart: Date.UTC(2010, 12, 17),
				    			lineColor: '#b5e319',
				    			lineWidth: 3,
				                marker: {
				                   lineWidth: 3,
				                   lineColor: '#ffffff',
				    			   radius: 6
				                },
				             data: []
				          },
				    	  {
				             name: 'Neg',
				    		 color: '#fa2231',
				    		 type: 'line',
				    		 plotShadow: false,
				    		 	shadow: false,
				                pointInterval: 24 * 3600 * 1000,
				             	pointStart: Date.UTC(2010, 12, 17),
				    			lineColor: '#fa2231',
				    			lineWidth: 3,
				                marker: {
				                   lineWidth: 3,
				                   lineColor: '#ffffff',
				    			   radius: 6
				                },
				             data: []
				          }]
				    	  }
						options.series[0].data = data.chart_data[0];
						options.series[1].data = data.chart_data[1];

						switch (step)
						{
							case 'days':
								options.series[0].pointInterval = 24 * 3600 * 1000;
								options.series[1].pointInterval = 24 * 3600 * 1000;

								options.xAxis.tickInterval = 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
							case 'weeks':
								options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 7 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = 7 * 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
							case 'months':
								options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 31 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = 31 * 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
						}
					} else
					{
						var options = {
								chart: {
									renderTo: 'chart_sentyment',
									type: 'spline'
								},
								credits: {
					    		  enabled:false,
					    	    },
								title: {
					            	text: ''
					          	},
								xAxis: {
						            type: 'datetime',
						    		gridLineWidth: 1,
						    		gridLineColor :'#bbbbbb',
						    		gridLineDashStyle: 'dot',
						            title: {
						            	text: null
						            }
					            },
								yAxis: {
						            title: {
						           		text: null
						            },
						    		min: 0,
						    		gridLineWidth: 1,
						    		gridLineColor :'#bbbbbb',
						    		gridLineDashStyle: 'dot',
						            labels: {
							            formatter: function() {
							           		return this.value;
						            	}
						            }
					           	},
								tooltip: {
						            backgroundColor: '#5f6264',
									 borderColor: '#5f6264',
									 borderWidth: 0,
									 shadow: false,
									 style: {
										padding: 8,
										color: '#ffffff',
									 },
						            formatter: function() {
							            return '<span style="font-size:10px">'+Highcharts.dateFormat('%A, %B %e, %Y', this.x)+'</span><br/>'+
							            '<span style="color:#0077cc">'+lp._('Wyników')+':</span><b> '+ Highcharts.numberFormat(this.y, 0, null, ' ') +'</b>';
						            }
					          	},
								plotOptions: {
									spline: {
										lineWidth: 4,
										states: {
											hover: {
												lineWidth: 5
											}
										},
										marker: {
											enabled: false,
											states: {
												hover: {
													enabled: true,
													symbol: 'circle',
													radius: 5,
													lineWidth: 1
												}
											}
										},
										pointInterval: 3600000, // one hour
										pointStart: Date.UTC(2009, 9, 6, 0, 0, 0)
									}
								},
								navigation: {
									menuItemStyle: {
										fontSize: '10px'
									}
								},
								series: [{
					             name: 'Pos',
					             cursor: 'pointer',
								 point: {
										events: {
											click: function() {
												chartDataFilter(Highcharts.dateFormat('%Y-%m-%d', this.x),Highcharts.dateFormat('%Y-%m-%d', this.x), 1);
											}
										}
								 },
					    		 color: '#3399cc',
					    		 type: 'spline',
					    		 plotShadow: false,
					    		 	shadow: false,
					                pointInterval: 24 * 3600 * 1000,
					    			lineColor: '#3399cc',
					    			lineWidth: 4,
					                marker: {
					                   lineWidth: 2,
					                   lineColor: '#ffffff',
					    			   radius: 4.5,
					               	   symbol: 'circle',
					                },
					             data: []
					          },
					    	  {
					             name: 'Neg',
					             cursor: 'pointer',
								 point: {
										events: {
											click: function() {
												chartDataFilter(Highcharts.dateFormat('%Y-%m-%d', this.x),Highcharts.dateFormat('%Y-%m-%d', this.x), 2);
											}
										}
								 },
					    		 color: '#d02432',
					    		 type: 'spline',
					    		 plotShadow: false,
					    		 	shadow: false,
					                pointInterval: 24 * 3600 * 1000,
					    			lineColor: '#d02432',
					    			lineWidth: 4,
					                marker: {
					                   lineWidth: 2,
					                   lineColor: '#ffffff',
					    			   radius: 4.5,
					               	   symbol: 'circle',
					                },
					             data: []
					          }]
					    	}

					    	options.series[0].data = data.chart_data[0];
						options.series[1].data = data.chart_data[1];

						switch (step)
						{
							case 'days':
								options.series[0].pointInterval = 24 * 3600 * 1000;
								options.series[1].pointInterval = 24 * 3600 * 1000;

								options.xAxis.tickInterval = 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
							case 'weeks':
								options.series[0].pointInterval = 7 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 7 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = 7 * 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
							case 'months':
								options.series[0].pointInterval = 31 * 24 * 3600 * 1000;
								options.series[1].pointInterval = 31 * 24 * 3600 * 1000;

								options.xAxis.tickInterval = 31 * 24 * 3600 * 1000;

								options.series[0].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								options.series[1].pointStart = Date.UTC(data.chart_info.pointStart.year, data.chart_info.pointStart.month, data.chart_info.pointStart.day);
								break;
						}
					}
				}
				chart = new Highcharts.Chart(options);
			},"json");
}

/**
 * Oznacza wyniki jako przeczytany
 */
function setResultAsVisited(id, kind)
{
	$('#mention_result_' + id).removeClass('mention');
	$('#mention_result_' + id).addClass('mention_vis');
	$('#mention_result_' + id).addClass('vis_' + kind);
	if ($('#visited-box-' + id).html() == '')
	{
		$('#visited-box-' + id).html('<span class="visited"> </span>');
	}
}

/**
 * Oznacza grupowo zaznaczone wyniki jako przeczytane
 */
function setResultAsVisitedMulti()
{
	var resultsToSetAsVisited = '';

	if (!checkIfUserChooseSomeResults())
	{
		showInfoToUserAboutNoSelectedResults();
	}
	else
	{
		$('.bulb_checkbox:checked').each(function()
		{
			var id = $(this).val();
			var kind = $(this).attr('data-kind');
			resultsToSetAsVisited += '&rid[]=' + id;
			setResultAsVisited(id, kind);
		});

		if (resultsToSetAsVisited != '')
		{
			$.post(
				'/panel/set-result-as-visited-multi-submit',
				'sid=' + searches_id  + resultsToSetAsVisited + '&tknB24=' + tknB24,
				function(data)
				{
					if (data.result != 1)
					{
						if (data.error)
						{
							errorMsg = data.error;
						}
						else
						{
							errorMsg = lp._('Błąd');
						}
						showDialogBox(2, errorMsg);
					}
				},
				'json'
			);
		}
	}
}

//Wyswietla okienko z informacja ze wybrana opcja jest niedostepna jeszcze
function showInfoOptionUnavailable()
{
	return showDialogBox(2,lp._("Wybrana opcja jest w trakcie budowy i obecnie jest niedostępna. Przepraszamy za utrudnienia i prosimy o cierpliwość."));
}

/**
 * Shows info, that account type is not adequate to use choosen option
 * @param {Object} msg - text infomration, if empty then will be set default
 */
function showInfoNotAdequateAccountType(msg)
{
	if (!msg)
	{
        msg = lp._("Plan Abonamentowy Twojego konta nie pozwala na skorzystanie z wybranej opcji.");
    }

	text = msg + '<br/><br/><a href="/account/upgrade" class="btn-cta no-ajax-abort" style="font-size: 14px; font-weight: 400; width:350px; margin: 0 auto;">' + lp._('Zobacz porównanie planów abonamentowych') + '</a>';

    prepareDialogBox(2,text);
    //modal without buttons
    $("#dialog-window").dialog({
        modal : true,
		width : 385,
		buttons: null
    });

    return void(0);
}

/**
 * Delete set of results in one requests
 */
function deleteResultMulti(searchId)
{
	if (!checkIfUserChooseSomeResults())
		showInfoToUserAboutNoSelectedResults();
	else
	{
		//internal function called when user confirm to delete
		function deleteResultMultiFinalStep()
		{
			showDialogBox(3,lp._("Usuwanie wpisów..."));

			//get results ids to delete
			resultIdsToDelete = "";
			$('.bulb_checkbox:checked').each(function()
			{
				resultIdsToDelete +=  "&rid[]="+$(this).val();
			});

			//delete results
			var delete_params = "sid=" + searchId + "&tknB24=" + tknB24  + resultIdsToDelete;
			$.post('/panel/delete-result-multi-submit',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					closeDialogBox();
					//if all results on page are selected and page is last we display previous page after reload
					if ($('.bulb_checkbox:checked').size() == $('.bulb_checkbox').size() && $('.page_no:last-child').hasClass('selected_page') && page > 1)
					{
						page = page - 1;
					}
					searchResults();//load results after delete
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
		}
		//end of internal function called when user confirm to delete


		//ask user to confirm to delete results
		var id = 0;
		var length = $('.bulb_checkbox:checked').length;
		prepareDialogBox(1,lp._("Czy na pewno chcesz usunąć wybrane")+" ("+length+") " +lp._("wyniki")+"?");

		var buttons = {};
		buttons[lp._('Nie')] = function(){
			$(this).dialog('close');
		};
		buttons[lp._('Tak')] = deleteResultMultiFinalStep;

		$("#dialog-window").dialog({
			modal: true, buttons: buttons
		});
	}
}

/**
 * Deletes and blocks result
 * @param {Object} resultId
 * @param {Object} kind:
 * @param {Object} forceDelte - if 1, then delete result directly without asking
 * @param {Object} resultDiv
 */
function deleteResult(resultId,bcKind,forceDelete)
{

	function deleteResultHandler(block)
	{
		showDialogBox(3,null);

		var delete_params = { rid: resultId, bc: bcKind, block : block, tknB24 : tknB24 };
		$.post('/panel/delete-result-submit',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					closeDialogBox();
					//$('#mention_result_'+resultId).remove();

					if(block == 1 && bcKind >= 1)	//jesli usuwamy domene
					{
						host = $('#mention_result_'+resultId+" .mention_source").attr('hidden_source_url');

						$(".mention ").each(function()
								{
									if($("#"+this.id+" .mention_source").attr('hidden_source_url') == host)
										$(this).fadeOut('slow', function()
										{
											$(this).remove();
										});
								}
						);
					}else

						$('#mention_result_'+resultId).fadeOut('slow', function()
						{
						    $('#mention_result_'+resultId).remove();
						});
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}

	function setResultAsSpam()
	{
		showDialogBox(3,null);

		var delete_params = { rid: resultId, tknB24 : tknB24 };
		$.post('/panel/set-result-as-spam',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					deleteResultHandler(0);
					//closeDialogBox();
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}

	function deleteOnlyResultHandler()
	{
		return deleteResultHandler(0);
	}

	function deleteResultAndBlockHandler()
	{
		return deleteResultHandler(1);
	}

	function setResultAsSpamHandler()
	{
		return setResultAsSpam();
	}

	if (forceDelete == 1)
	{
		deleteOnlyResultHandler();
	}
	else
	{
		if (bcKind == 1) {
			msg = lp._("Chcesz usunąć tylko wynik czy również zablokować domenę, aby serwis nie pobierał z niej więcej wyników?");
			var buttons = {};
				buttons[lp._('Nie usuwaj')] = function(){
					$(this).dialog('close');
				};
				buttons[lp._('Usuń wynik')] = deleteOnlyResultHandler;
				buttons[lp._('Usuń wynik i zablokuj domenę')] = deleteResultAndBlockHandler;
				buttons[lp._('SPAM!')] = setResultAsSpamHandler;
		}
		else if (bcKind > 1) {
			msg = lp._("Chcesz usunąć tylko wynik czy również zablokować autora wpisu, aby serwis nie pobierał więcej od niego wiadomości?");
			var buttons = {};
			buttons[lp._('Nie usuwaj')] = function(){
				$(this).dialog('close');
			};
			buttons[lp._('Usuń wynik')] = deleteOnlyResultHandler;
			buttons[lp._('Usuń wynik i zablokuj autora')] = deleteResultAndBlockHandler;
		}
		else
		{
			msg = lp._("Czy na pewno chcesz usunąć wynik?");
			var buttons = {};
				buttons[lp._('Nie')] = function(){
					$(this).dialog('close');
				};
				buttons[lp._('Tak')] = deleteOnlyResultHandler;
		}

		prepareDialogBox(1,msg);
		$("#dialog-window").dialog({
			resizable: false,
			height: 175,
			width: 500,
			modal: true,
			buttons: buttons
		});
		//return false; nie ma tego, bo funkcja jest wywwolywana  poprzez href="javascript:.."
	}
}

/**
 * Blocks author or site
 * Zbudowana na bazie deleteResult
 * @param {Object} resultId
 * @param {Object} bcKind
 */
function blockSource(resultId,bcKind)
{

	function deleteResultHandler(block)
	{
		showDialogBox(3,null);

		var delete_params = { rid: resultId, bc: bcKind, block : block, tknB24 : tknB24 };
		$.post('/panel/delete-result-submit',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					closeDialogBox();
					//$('#mention_result_'+resultId).remove();

					if(block == 1 && bcKind >= 1)	//jesli usuwamy domene
					{
						host = $('#mention_result_'+resultId+" .mention_source").attr('hidden_source_url');

						$(".mention ").each(function()
								{
									if($("#"+this.id+" .mention_source").attr('hidden_source_url') == host)
										$(this).fadeOut('slow', function()
										{
											$(this).remove();
										});
								}
						);
					}else

						$('#mention_result_'+resultId).fadeOut('slow', function()
						{
						    $('#mention_result_'+resultId).remove();
						});
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}

	function deleteResultAndBlockHandler()
	{
		return deleteResultHandler(1);
	}

	if (bcKind == 1) {
		msg = lp._("Czy chcesz zablokować domenę i usunąć wszystkie zgromadzone z niej wyniki?");
		var buttons = {};
		buttons[lp._('Nie')] = function(){
			$(this).dialog('close');
		};
		buttons[lp._('Tak, zablokuj domenę i usuń wyniki')] = deleteResultAndBlockHandler;
	}
	else if (bcKind > 1) {
		msg = lp._("Czy chcesz zablokować autora i usunać wszystkie zgromadzone od niego wpisy?");
		var buttons = {};
		buttons[lp._('Nie')] = function(){
			$(this).dialog('close');
		};
		buttons[lp._('Tak, zablokuj autora i usuń wyniki')] = deleteResultAndBlockHandler;
	}

	prepareDialogBox(1,msg);
	$("#dialog-window").dialog({
		resizable: false,
		height: 170,
		width: 500,
		modal: true,
		buttons: buttons
	});
	//return false; nie ma tego, bo funkcja jest wywwolywana  poprzez href="javascript:.."

}

/**
 * Deletes and blocks result
 * Kopia funkcji deleteResult - po usunieciu zamyka okno - wykorzystywana w podgladzie strony
 * @param {Object} resultId
 * @param {Object} kind:
 * @param {Object} forceDelte - if 1, then delete result directly without asking
 * @param {Object} resultDiv
 */
function deleteResultInFrame(resultId,bcKind,forceDelete,fadeOutMention)
{

	function deleteResultHandler(block)
	{
		showDialogBox(3,null);

		var delete_params = { rid: resultId, bc: bcKind, block : block, tknB24 : tknB24 };
		$.post('/panel/delete-result-submit',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					closeDialogBox();
					//window.opener.$('#mention_result_'+resultId).fadeOut('slow');
					if(fadeOutMention == 1)
					{
						window.opener.$('#mention_result_'+resultId).fadeOut('slow');
					}
					close();
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}

	function setResultAsSpam()
	{
		showDialogBox(3,null);

		var delete_params = { rid: resultId, tknB24 : tknB24 };
		$.post('/panel/set-result-as-spam',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					deleteResultHandler(0);
					//closeDialogBox();
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas usuwania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}

	function deleteOnlyResultHandler()
	{
		return deleteResultHandler(0);
	}

	function deleteResultAndBlockHandler()
	{
		return deleteResultHandler(1);
	}

	function setResultAsSpamHandler()
	{
		return setResultAsSpam();
	}

	if (forceDelete == 1)
	{
		deleteOnlyResultHandler();
	}
	else
	{
		if (bcKind == 1) {
			msg = lp._("Chcesz usunąć tylko wynik czy również zablokować domenę, aby serwis nie pobierał z niej więcej wyników?");
			var buttons = {};
				buttons[lp._('Nie usuwaj')] = function(){
					$(this).dialog('close');
				};
				buttons[lp._('Usuń wynik')] = deleteOnlyResultHandler;
				buttons[lp._('Usuń wynik i zablokuj domenę')] = deleteResultAndBlockHandler;
				buttons[lp._('SPAM!')] = setResultAsSpamHandler;
		}
		else if (bcKind > 1) {
			msg = lp._("Chcesz usunąć tylko wynik czy również zablokować autora wpisu, aby serwis nie pobierał więcej od niego wiadomości?");
			var buttons = {};
			buttons[lp._('Nie usuwaj')] = function(){
				$(this).dialog('close');
			};
			buttons[lp._('Usuń wynik')] = deleteOnlyResultHandler;
			buttons[lp._('Usuń wynik i zablokuj autora')] = deleteResultAndBlockHandler;
		}
		else
		{
			msg = lp._("Czy na pewno chcesz usunąć wynik?");
			var buttons = {};
				buttons[lp._('Nie')] = function(){
					$(this).dialog('close');
				};
				buttons[lp._('Tak')] = deleteOnlyResultHandler;
		}

		prepareDialogBox(1,msg);
		$("#dialog-window").dialog({
			resizable: false,
			height: 170,
			width: 500,
			modal: true,
			buttons: buttons
		});
		//return false; nie ma tego, bo funkcja jest wywwolywana  poprzez href="javascript:.."
	}
}


/**
 * @param {Object} searchId
 * @param {Object} bcName - host or author id
 * @param {Object} bcKind - see server's method description
 */
function blockHostOrAuthorInSearch(searchId, bcName, bcKind)
{

	function blockHostOrAuthorInSearchHandler()
	{
		showDialogBox(3,null);


		var delete_params = { searchId: searchId, bc: bcKind, bn : bcName, tknB24 : tknB24 };
		$.post('/panel/block-host-or-author-submit',delete_params,
			function(data)
			{
				if (data.result == 1) //ok
				{
					window.location.reload(true);
                    return;
				}
				else //blad
				{
					if (data.error)
						errorMsg = data.error;
					else
						errorMsg = lp._('Błąd podczas blokowania');
					showDialogBox(2,errorMsg)
				}
			},'json');
	}



	if (bcKind == 1) {
		msg = lp._("Czy chcesz zablokować domenę i usunąć wszystkie zgromadzone z niej wyniki?");
		var buttons = {};
			buttons[lp._('Nie')] = function(){
					$(this).dialog('close');
				};
			buttons[lp._('Tak, zablokuj domenę i usuń wyniki')] = blockHostOrAuthorInSearchHandler;

	}
	else if (bcKind > 1) {
		msg = lp._("Czy chcesz zablokować autora i usunać wszystkie zgromadzone od niego wpisy?");
		var buttons = {};
			buttons[lp._('Nie')] = function(){
					$(this).dialog('close');
					};
				buttons[lp._('Tak, zablokuj autora i usuń wyniki')] = blockHostOrAuthorInSearchHandler;
	}
	else
	{
		showDialogBox(3,lp._("Nie można zablokować tego autora"));
		return;
	}

	prepareDialogBox(1,msg);

	$("#dialog-window").dialog({
		resizable: false,
		height: 170,
		width: 500,
		modal: true,
		buttons: buttons
	});
	return false;

}



/**
 * Deletes search (project)
 * @param {Object} id
 */
function deleteSearch(id)
{
	prepareDialogBox(1,lp._("Czy  na pewno chcesz skasować projekt?"));

	var buttons = {};
	buttons[lp._('Nie')] = function(){
		$(this).dialog('close');
		};
		buttons[lp._('Tak')] = function(){
			showDialogBox(3,null)
			$.post('/panel/delete-search-submit',{ "id": id, tknB24 : tknB24 },
				function(data)
				{
					if (data.redirect)
					{
						window.location.href = data.redirect;
						return;
					}

					if (data.result == 1) //ok
					{
						closeDialogBox();
						if ($("#phrase_div_" + id).length > 0)
						{
							$("#phrase_div_" + id).remove();
							//showDialogBox(2, lp._('Projekt został usunięty'))
						}
						else
							window.location.href = '/panel/';
					}
					else //blad
					{
						showDialogBox(2,lp._('Błąd podczas usuwania'))
					}
				},'json');

		};

	$("#dialog-window").dialog({
		resizable: false,
		height: 180,
		modal: true,
		buttons: buttons
	});


}

function showPdfConfiguration()
{
	showPdfConfigurationLoader(1);

	$('#pdf_configuration').css({'display':'block'});
	$('#pdf_configuration').center();

	$.post('/searches/get-pdf-configuration-settings',{},
	function(data)
	{
		hidePdfConfigurationLoader();

		$('#pdf_configuration').css({'display':'block'});
		$('#pdf_configuration').center();

		if (data.account.pdf_report_logo_url)
			$('#raport_logo_image').html(lp._('Aktualne logo')+': <a href="'+data.account.pdf_report_logo_url+'" target="_blank">'+lp._('zobacz')+'</a> '+lp._('lub')+' <a href="javascript:deletePdfConfigurationLogo()">'+lp._('usuń')+'</a>');

		if (data.account.pdf_report_comment)
			$('#user_comment').val(data.account.pdf_report_comment);

		if (data.account.pdf_report_color)
			$('#pdf_configuration_color_'+data.account.pdf_report_color).css({'border':'1px solid #90ccff'});

		if (data.account.pdf_report_last_update_timestamp)
			pdf_report_last_update_timestamp = data.account.pdf_report_last_update_timestamp;


		$('#overlay').css({'height':$('#site').height()});
		$('#overlay').fadeIn('slow', function() {});
	},'json');
}

function deletePdfConfigurationLogo()
{
	$('#raport_logo_image').html(lp._('Zapisywanie...'));

	$.post('/searches/delete-pdf-configuration-logo/',{},
	function(data)
	{
		$('#raport_logo_image').html(lp._('Logo usunięte.'));
	},'text');
}

function getPdfLastUpdate()
{
	//wywalilem wywolanie po zwroceniu danych bo na chromie nie dzialalo za pierwszym razem
	if ($('#pdf_configuration').is(':visible'))
		setTimeout('getPdfLastUpdate();',5000);

	$.post('/searches/get-pdf-last-update',{},
	function(data)
	{
		if (parseInt(data) > pdf_report_last_update_timestamp)
		{
			pdf_report_last_update_timestamp = parseInt(data);
			closePdfConfiguration();
            return;
		}

		//setTimeout('getPdfLastUpdate();',5000);
	},'text');
}

function closePdfConfiguration()
{
	$('#pdf_configuration').css({'display':'none'});
	$('#overlay').fadeOut('slow', function() {});
}

function pdfConfigurationColor(color)
{
	$('.pdf_configuration_color').css({'border':'1px solid #999999'});
	$('#pdf_configuration_color_'+color).css({'border':'1px solid #90ccff'});

	$('#pdf_configuration_color_value').val(color);
}

function showPdfConfigurationLoader(type)
{
	if (type == 1)
	{
		$('#pdf_configuration_loader_header').html(lp._('Pobieranie danych...'));
		$('#pdf_configuration_loader_close_txt').css({'display':'none'});
		$('#pdf_configuration_loader_close_btn').css({'display':'none'});
		$('#raport_logo_image').html('');
	} else
	{
		//pierwsze wywolanie
		setTimeout('getPdfLastUpdate();',5000);

		$('#pdf_configuration_loader_header').html(lp._('Trwa generowanie raportu'));
		$('#pdf_configuration_loader_close_txt').css({'display':'block'});
		$('#pdf_configuration_loader_close_btn').css({'display':'block'});
	}

	$('#pdf_configuration_loader').css({'display':'block'});
	$('#pdf_configuration_main').css({'display':'none'});
}

function hidePdfConfigurationLoader()
{
	$('#pdf_configuration_loader').css({'display':'none'});
	$('#pdf_configuration_main').css({'display':'block'});
}

function showPdfUpgradeInfo()
{
	msg = lp._('Raporty PDF nie są dostępne w planie abonamentowym Twojego konta')+'<br/><br/>';
	if (APP_INSTANCE == 'brand24.pl')
		msg += lp._('Więcej informacji oraz przykładowy raport znajdziesz')+' <a href="/raporty-pdf" target="_blank">'+lp._('tutaj')+'</a> <br /><br/>';
	msg +=  '<a href="/account/upgrade/" target="_blank">'+lp._('Zmień')+'</a> '+lp._('plan abonamentowy');
	prepareDialogBox(2,msg);
	$("#dialog-window").dialog({
		resizable: false,
		height: 240,
		modal: true,
		buttons: {
			'OK': function(){
				$(this).dialog('close');
			},
		}
	});
}

		function showInfographPopup()
		{
			infographConfigurationLoaderOff();

			$.post('/searches/get-pdf-infograph-settings', {},
			function(data)
			{
				//alert(pdf_infograph_last_update_timestamp);
				pdf_infograph_last_update_timestamp = data.account.pdf_infograph_last_update_timestamp;
				//alert(pdf_infograph_last_update_timestamp);
				setTimeout('getPdfInfographLastUpdate();',1000);
			},'json');


			$('#infograph_configuration').css({'display':'block'});
			$('#infograph_configuration').center();

			$('#overlay').css({'height':$('#site').height()});
			$('#overlay').fadeIn('slow', function() {});
		}

		function infographConfigurationClose()
		{
			$('#infograph_configuration').css({'display':'none'});

			$('#overlay').fadeOut('slow', function() {});
		}

		function infographConfigurationLoaderOn()
		{
			$('#infograph_configuration_container').css({'display':'none'});
			$('#infograph_configuration_loader').css({'display':'block'});
		}

		function infographConfigurationLoaderOff()
		{
			$('#infograph_configuration_container').css({'display':'block'});
			$('#infograph_configuration_loader').css({'display':'none'});
		}

		function infographConfigurationSubmit()
		{
			if ($('#infograph_configuration_show option:selected').val() == 1)
			{
				window.location = infograph_btn_href;
			}

			if ($('#infograph_configuration_show option:selected').val() == 2)
			{
				infographConfigurationLoaderOn();
				window.location = infograph_btn_href+"export/pdf";
			}
		}

		function getPdfInfographLastUpdate()
		{
			//wywalilem wywolanie po zwroceniu danych bo na chromie nie dzialalo za pierwszym razem
			if ($('#infograph_configuration').is(':visible'))
				setTimeout('getPdfInfographLastUpdate();',5000);

			$.post('/searches/get-pdf-infograph-last-update',{},
			function(data)
			{
				if (parseInt(data) > pdf_infograph_last_update_timestamp)
				{
					pdf_infograph_last_update_timestamp = parseInt(data);
					infographConfigurationClose();
                    return;
				}

				//setTimeout('getPdfLastUpdate();',5000);
			},'text');
		}

function getExcelLastUpdate()
{
	//wywalilem wywolanie po zwroceniu danych bo na chromie nie dzialalo za pierwszym razem
	if ($('#excel_preloader').is(':visible'))
		setTimeout('getExcelLastUpdate();',5000);

	$.post('/searches/get-excel-last-update',{},
	function(data)
	{
		if (parseInt(data) > excel_report_last_update_timestamp)
		{
			excel_report_last_update_timestamp = parseInt(data);
			closeExcelPreloader();
            return;
		}
	},'text');
}

function showExcelPreloader()
{
	$('#excel_preloader').css({'display':'block'});
	$('#excel_preloader').center();
	setTimeout('getExcelLastUpdate();',5000);

	$.post('/searches/get-excel-last-update',{},
	function(data)
	{
		if (parseInt(data) > 0)
			excel_report_last_update_timestamp = parseInt(data);
	});
	$('#overlay').css({'height':$('#site').height()});
	$('#overlay').fadeIn('slow', function() {});
}

function closeExcelPreloader()
{
	$('#excel_preloader').css({'display':'none'});
	$('#overlay').fadeOut('slow', function() {});
}

//Funkcja chowa panel projektów
function hideLeftPanel()
{
	$('#panel_projects_list').hide();
	$('#panel_show_button').show();

	if(tab == 'results' || tab == 'compare')
	{
		loadTabData();
	}

	$.post('/panel/hide-projects-panel/',{hide:0},
	function(data) {	},'text');
}

function showLeftPanel()
{
	$('#panel_projects_list').show();
	$('#panel_show_button').hide();

	if(tab == 'results' || tab == 'compare')
	{
		loadTabData();
	}

	$.post('/panel/hide-projects-panel/',{hide:1},
	function(data) 	{	},'text');
}
/**
 * Zmienne timera
 */
var loadTabDataTimeout, checkIfDataDownloadIsInProgressTimeout = null;
/**
 * Zmienne określająca czy projekt jest po utworzeniu i jeszcze uruchamiane są parsery - dodatkowo podczas ładowania wyników dociągać liczbę wyników z ostatnich 30 dni do boxa z loaderkiem
 */
var projectHasJustAdded = false;
/**
 * Data dodania projektu - używana do odliczenia różnicy czasu od dodania - po 5 minutach przestajemy odświeżać wyniki
 */
var projectAddedDate = null;
/**
 * Poprzednio wybrany zakres dat
 */
var dataDownloadIsInProgressLastDateRange = 0;
/**
 * Czy box wyswietlany po utworzeniu projektu, informujacego o kopiowaniu danych z archiwum jest aktywny (1 - tak, 0 - nie)
 */
var resultsLoadingBoxActive = 0;
/**
 * Czy pokazywac % postepu wewnatrz box'a (1 - tak, 0 - nie)
 */
var resultsLoadingBoxShowPercent = 0;
/**
 * Dodatkowy tekst wewnatrz box'a (gdy nic nie wpisano nie wyswietlamy)
 */
var resultsLoadingBoxAdditionalText = '';
/**
 * Sprawdza, czy wybrany zakres dat chociaż częściowo pokrywa sie z ostatnimi 30 dniami
 * @param string date1
 * @param string date2
 * @return bool
 */
function checkIfDateRangeInLastMonth(date1, date2)
{
	var monthDate2 = new Date();
	var monthDate1 = new Date(monthDate2.getFullYear(), monthDate2.getMonth(), monthDate2.getDate() - 30);
	monthDate2 = monthDate2.getTime();
	monthDate1 = monthDate1.getTime();

	var date1Array = date1.split('-');
	d1 = new Date(date1Array[0], date1Array[1] - 1, date1Array[2]);
	d1 = d1.getTime();
	var date2Array = date2.split('-');
	d2 = new Date(date2Array[0], date2Array[1] - 1, date2Array[2]);
	d2 = d2.getTime();

	if (d1 >= monthDate1 && d1 <= monthDate2)
	{
		return true;
	}

	if (d2 >= monthDate1 && d2 <= monthDate2)
	{
		return true;
	}

	return false;
}
/**
 * Decyduje czy ustawić loadTabDataTimeout
 */
function setLoadTabDataTimeout()
{
	if (resultsLoadingBoxActive == 1 && projectHasJustAdded)
	{
		//po 5 minutach przestajemy odświeżać wyniki
		var dateAfterProjectAdded = new Date().getTime();
		var timeAfterProjectAdded = dateAfterProjectAdded - projectAddedDate;
		if (timeAfterProjectAdded < 300000)
		{
			clearTimeout(loadTabDataTimeout);
			loadTabDataTimeout = null;
			loadTabDataTimeout = setTimeout('loadTabData();', 60000);
		}
	}
}
/**
 * Funkcja inicjuje ladowanie danych w danej zakladce
 * Jeśli uruchamiam parsery przeładowuje wyniki co 30 sekund i dodatkowo doczytuje liczbe wynikow z ost. 30 dni do boxa z loaderkiem
 */
function loadTabData()
{
	//obsługa przycisku wyczyść filtry
	buttonClearAdditionalFiltersHandler();

	//abort all running ajaxes
	abortAjaxes();

	switch (tab)
	{
		case 'results':
			//warunek by nie pozwolić na automatyczne przeładowanie wyników gdy ktoś właśnie wpisuje coś w wyszukkiwarce
			if(!projectHasJustAdded || (projectHasJustAdded && $('#filter_input_te:focus')[0] != document.activeElement))
			{
				searchResults();
			}
		break;
		case 'analysis':
			initAnalysis();
		break;
        case 'hashtags':
            initHashtagAnalysis();
        break;
		case 'sources':
		case 'sources-authors':
		case 'sources-sites':
			initSources();
		break;
		case 'compare':
			initCompareProjects();
		break;
		default:
		break;
	}

	//jeśli projekt jest po utworzeniu i jeszcze uruchamiane są parsery - powtarzamy wczytywanie wyników co 30 sekund
	setLoadTabDataTimeout();

	//jeśli jest loaderek i ustawione jest 30 dni to wyświetla info o ilości wyników
	if ($('#panel_results_loader').length)
	{
		checkIfDataDownloadIsInProgress();
		if (checkIfDateRangeInLastMonth(params['d1'], params['d2']))
		{
			$('#panel_results_loader .loader-no-of-results').show();
		}
		else
		{
			$('#panel_results_loader .loader-no-of-results').hide();
		}
	}
}
/**
 * Sprawdza czy parsery skonczyly dzialanie)
 * Dodatkowo sprawdza ilość pobranych wyników
 */
function checkIfDataDownloadIsInProgress()
{
	clearTimeout(checkIfDataDownloadIsInProgressTimeout);
	checkIfDataDownloadIsInProgressTimeout = null;

	$.post(
		'/panel/check-if-data-download-is-in-progress-for-date-range/',
		{
			sid: searches_id,
			d1: params['d1'],
			d2: params['d2']
		},
		function (data)
		{
			//jeśli uruchomił już wszystko chowamy box i wyłączamy sprawdzanie
			if (data.search_ready && data.search_ready == 1)
			{
				$('.results-loading-box').remove();
				projectHasJustAdded = false;
				clearTimeout(loadTabDataTimeout);
				loadTabDataTimeout = null;
				loadTabData();
			}
			else
			{
				//gdy projekt ma ponad 3 min wyświetlamy zminimalizowany box
				if (data.miniBox == 1)
				{
					$('#panel_results_loader').removeClass('active-loader');
					$('#panel_results_loader').hide();
					$('#panel_results_loader_mini').addClass('active-loader');
				}
				//gdy projekt ma ponad 5 min wyświetlamy zminimalizowany box
				if (data.microBox == 1)
				{
					$('#panel_results_loader').removeClass('active-loader');
					$('#panel_results_loader').hide();
					$('#panel_results_loader_mini').removeClass('active-loader');
					$('#panel_results_loader_mini').hide();
					$('#panel_results_loader_micro').addClass('active-loader');
				}

				//jeśli pobrał procent
				if (data.percent)
				{
					//jeśli już 100% ukryj box i załaduj dane
					if (data.percent == 100)
					{
						$('.results-loading-box').hide();
					}
					//gdy mniej niż 100% popraw procent
					else
					{
						$('.results-loading-box.active-loader').show();
						var oldPercent = parseInt($('#panel_results_loader .percent-count').text());
						if (parseInt(data.percent) < oldPercent)
						{
							data.percent = oldPercent;
						}
						//animacja
						if (dataDownloadIsInProgressLastDateRange == params['dr'] && $('#panel_results_loader .percent-count').text() != '' && parseInt(data.percent) > oldPercent)
						{
							var percentToAdd = parseInt(data.percent) - oldPercent;
							var duration = 2000 * percentToAdd;
							$({countNum: oldPercent}).animate({countNum: data.percent}, {
								duration: duration,
								easing: 'linear',
								step: function() {
									$('#panel_results_loader .percent-count').html(parseInt(this.countNum));
									$('#panel_results_loader_mini .percent-count').html(parseInt(this.countNum));
								},
								complete: function() {
									$('#panel_results_loader .percent-count').html(parseInt(this.countNum));
									$('#panel_results_loader_mini .percent-count').html(parseInt(this.countNum));
								}
							});
						}
						else
						{
							$('#panel_results_loader .percent-count').html(data.percent);
							$('#panel_results_loader_mini .percent-count').html(data.percent);
						}

						//generuje tekst do tooltipa micro boxa
						var panelResultsLoaderMicroText = lp._('results.loading_box.data_download_progress_info');
						//jesli pokazywac %
						if (resultsLoadingBoxShowPercent == 1)
						{
							panelResultsLoaderMicroText = panelResultsLoaderMicroText + ' (' + data.percent + '%)';
						}
						panelResultsLoaderMicroText = panelResultsLoaderMicroText + '.';
						//jesli dodatkowy tekst
						if (resultsLoadingBoxAdditionalText != '')
						{
							panelResultsLoaderMicroText = panelResultsLoaderMicroText + ' ' + resultsLoadingBoxAdditionalText;
						}
						$('#panel_results_loader_micro img').qtip({
							content:{
								text: panelResultsLoaderMicroText,
							}
						});

						//jeśli zakres 30 dni pokaż info o ilości wynikow
						if (checkIfDateRangeInLastMonth(params['d1'], params['d2']))
						{
							$('#panel_results_loader .loader-no-of-results').show();
						}
						else
						{
							$('#panel_results_loader .loader-no-of-results').hide();
						}
					}
				}
				//gdy błąd ukryj box
				else
				{
					$('.results-loading-box').hide();
				}

				//za 15 sekund sprawdzam ponownie
				checkIfDataDownloadIsInProgressTimeout = setTimeout('checkIfDataDownloadIsInProgress();', 15000);
			}
			dataDownloadIsInProgressLastDateRange = params['dr'];
		},
		'json'
	);
}
function updateAccountAutomaticRenewal(value,redirectAfterSuccess,backendConfirmationMessage)
{
	if (value)
		value = 1;
	else
		value = 0;
	$.ajax({type : "POST",
		dataType : "json",
		data : "switch="+value + "&bcm="+backendConfirmationMessage+  "&tknB24=" + tknB24,
		url : "/account/update-account-automatic-renewal-submit/",
		success : function(data)
		{
			if (data.result != 1)
			{
				if (data.error)
					msg = data.error;
				else
					msg =lp._("Wystąpił błąd");
				showDialogBox(2,msg);
			}
			else
			{
                window.location.replace(redirectAfterSuccess);
                return;
			}
		}
	});
}

//otwiera okienko z formularzem do wysylki maila z powiadomieniem o znalezionym wyniku oraz uzupelnia tresc
function resultNotificationEmailOpen(resultId)
{
	var mentionDivContent = '';

	mentionDivContent += '<div style="padding-bottom:10px; margin-bottom:40px; color:#444444; font-family:arial,sans-serif; font-size:12px; line-height:18px; word-wrap:break-word">' +
							'<table width="100%" cellpadding="0" cellspacing="0">' +
								'<tr>';

	mentionDivContent += '<td valign="top" width="70" align="left" class="avatar-photo"><a>' + $('#mention_avatar_'+resultId+' a').html() + '</a></td>';

	mentionDivContent += '<td valign="top">' +
							'<div><span class="mention-title">' + $('#mention_title_' + resultId).html() + '</span></div>' +
							'<div><span class="mention-content">' + $('#mention_text_' + resultId).html().substr(0,250) + '</span></div>'+
						'</td>';
	mentionDivContent += '</tr></table></div>';

	$('#result_notification_email_content').html(mentionDivContent);
	$('#result_notification_email_rid').val(resultId);
	$('#result_notification_email_email').val('');
	$('#result_notification_email_comment').val(lp._('Przesyłam interesujący wpis znaleziony w Internecie przez Brand24'))

	var resultNotificationEmailButtons = {};
	resultNotificationEmailButtons[lp._('Anuluj')] = function(){
		$(this).dialog('close');
	};

	resultNotificationEmailButtons[lp._('Wyślij')] = function(){
		resultNotificationEmailSubmit();
	};

	$("#result_notification_email").dialog({
		resizable: false,
		height: 550,
		width: 550,
		modal: true,
		buttons: resultNotificationEmailButtons
	});
}

//wysylazamyka maila z powiadomieniem o znalezionym wyniku oraz zamyka okienko z formularzem do wysylki lub wyrzuca bledy
function resultNotificationEmailSubmit()
{
	showDialogBox(3,null);
	$.post('/panel/result-notification-email-submit',{searches_id:searches_id, results_id:$('#result_notification_email_rid').val(), email:$('#result_notification_email_email').val(), comment:$('#result_notification_email_comment').val(), tknB24 : tknB24},
	function(data)
	{
		if (data.result != 1)
		{
			if (data.error)
				msg = data.error;
			else
				msg =lp._("Wystąpił błąd");

			showDialogBox(2,msg);
            return;
		}
		else
		{
			$("#result_notification_email").dialog('close');
			showDialogBox(2,lp._('Powiadomienie zostało wysłane'));
            return;
		}
	},'json');
}

function changeValuablePick(valuableValue){
    if (valuableValue == 1) {
      toggleFilter('va', '3');
    }
    if (valuableValue == 2) {
      toggleFilter('va', '1');
    }
    if (valuableValue == 3) {
      toggleFilter('va', '2');
    }
}

function changeSentimentPick(sentimentValue){
    if (sentimentValue == 1) {
      toggleFilter('se', '2');
    }
    if (sentimentValue == 2) {
      toggleFilter('se', '');
    }
    if (sentimentValue == 3) {
      toggleFilter('se', '1');
    }
}

function showMostInteractiveTab() {
	$('.showPopularAuthors').removeClass('active');
	$('.showMostInteractiveTab').addClass('active');
	$('#entries_from_most_popular_authors').removeClass('in');
	$('#most_interactive_entries_from_social_media').addClass('in');
	fixHeightOfAnalysisStats($('#most_interactive_entries_from_social_media'));
}

function showPopularAuthors() {
  	$('.showMostInteractiveTab').removeClass('active');
  	$('.showPopularAuthors').addClass('active');
  	$('#most_interactive_entries_from_social_media').removeClass('in');
  	$('#entries_from_most_popular_authors').addClass('in');
	fixHeightOfAnalysisStats($('#entries_from_most_popular_authors'));
}

function showSourcesAuthors() {
  $('.showSourcesSites').removeClass('active');
  $('.showSourcesAuthors').addClass('active');
  $('#sources-tabs-sites').removeClass('in');
  $('#sources-tabs-authors').addClass('in');
  $('.sources-sites').removeClass('in');
  $('.sources-authors').addClass('in');
}

function showSourcesSites() {
  $('.showSourcesAuthors').removeClass('active');
  $('.showSourcesSites').addClass('active');
  $('#sources-tabs-authors').removeClass('in');
  $('#sources-tabs-sites').addClass('in');
  $('.sources-authors').removeClass('in');
  $('.sources-sites').addClass('in');
}

function showChartExportMenu() {
  $('#main_charts_export-menu').hide();

  $('#main_charts_export').click(function() {
    $('#main_charts_export-menu').toggle();
  });

  $('#main_charts_export-menu .export-png').click(function() {
    mchart.exportChart({type: 'image/png', filename: 'chart'}, {subtitle: {text:''}});
  });

  $('#main_charts_export-menu .export-jpeg').click(function() {
    mchart.exportChart({type: 'image/jpeg', filename: 'chart'}, {subtitle: {text:''}});
  });

  $('#main_charts_export-menu .export-pdf').click(function() {
    mchart.exportChart({type: 'application/pdf', filename: 'chart'}, {subtitle: {text:''}});
  });

  $('#main_charts_export-menu .export-svg').click(function() {
    mchart.exportChart({type: 'image/svg+xml', filename: 'chart'}, {subtitle: {text:''}});
  });
}


$(document).ready(function() {
  $('.collapse-header .fa').click(function() {
    $(this).parents('.collapse-header').next('.collapse').toggleClass('in');
    $(this).parents('.collapse-header').toggleClass('collapse-header-current');
  });

  /* Disabled becuase we can't hide messages from users after few seconds
  if ($('.message--wrapper').length > 0) {
      $('.message--wrapper').delay(5000).fadeOut(300);
  }
  */
});


function createAlertByFilters(alertEmail)
{

	var createAlertByFiltersButtons = {};
	createAlertByFiltersButtons[lp._('Nie')] = function(){
		$(this).dialog('close');
	};

	createAlertByFiltersButtons[lp._('Tak')] = function(){

			showDialogBox(3,null);

			//check if limit isn't exceeded (by ajax) and if everything is okey add row to html table
			$.post("/searches/check-if-can-add-user-to-account-by-email-submit/",
				{
					email: alertEmail
				},
				function(data)
				{
					if (data.result == 1)
					{
						var filter = paramsToUrlForSaveMyFilter();

						var title = lp._("Nowy filtr") + ' ' + ($(".panel_filter_my_filter_row").length + 1);

						var filtersId = 0;
						if(params['mf'])
						{
							filtersId = params['mf'];
						}

						$.post("/panel/create-alert-by-current-filters/",
							{
								email: alertEmail,
								sid: searches_id,
								filters_id: filtersId,
								filter: filter,
								title: title,
								tknB24: tknB24
							},
							function(data)
							{
								if (data.result > 0)
								{
									$("#dialog-window").dialog('close');
									closeDialogBox();

									if(!params['mf'])
									{
										$("#panel_filter_my_filters_div").append(
										'<div id="my_filter_div_'+data.result+'"><table width="100%"><tr>'+
										'<td width="60%"><div id="filter_button_mf_' + data.result + '" class="panel_filter_my_filter_row filter_button_mf"><a href="javascript:runMyFilter(' + data.result + ', \'' + urlEncode(filter) + '\')" class="panel_filter_my_filter_row_title" id="panel_filter_my_filter_row_' + data.result + '" data-id="' + data.result + '" data-title="' + xssProtect(title) + '"><i class="fa fa-folder-open-o"></i> <span>' + xssProtect(title) + '</span></a></div></td>'+
										'<td align="right"><div class="panel_filter_group_row_hidden" style="display:block"><a href="javascript:removeMyFilter(' + data.result + ')" style="font-size:10px">' + lp._('Usuń') + '</a></div></td></tr></table></div>');
									}

									$('#filter_input_alert_email').val(lp._("podaj email") + '...');

									$("#input_my_filter_name").val('');
									$("#input_edit_my_filter_name").val(title);
									$("#edit_my_filter_hidden").val(data.result);

									myFilters[parseInt(data.result)] = title;

									showDialogBox(2, lp._("Alert został utworzony"));
									return;
								}
								else
								{
									if (data.error)
										msg = data.error;
									else
										msg = lp._("Wystąpił błąd");

									showDialogBox(2, msg);
								}
							},
						"json"
						);

						return false;
					}

					if (data.error)
					{
						$("#dialog-window").dialog('close');
						closeDialogBox();
						showDialogBox(2,'<div style="text-align:left;">' + data.error + '</div>');
						return false;
					}

					return false;

				},
				"json"
			);
	};

	if(alertEmail.length > 5 && alertEmail != lp._("podaj email")+'...')
	{
		prepareDialogBox(1,lp._("Na pewno chcesz utworzyć nowy alert?"));
		$("#dialog-window").dialog({
			resizable: false,
			height: 180,
			modal: true,
			buttons: createAlertByFiltersButtons
		});
	}
	else
	{
		showDialogBox(2, '<div style="text-align:left;">' + lp._("Wprowadź prawidłowy email") + '</div>');
	}

}

function createNewGroup(name, resultId)
{
	if (resultId != null)
	{
		$('#add-group-error-' + resultId).html('');
	}

	if (name == '')
	{
		if (resultId != null)
		{
			$('#add-group-error-' + resultId).html(lp._("Musisz podać nazwę grupy"));
		}
		else
		{
			showDialogBox(2, lp._("Musisz podać nazwę grupy"));
		}
	}
	else
	{
		if (resultId == null)
		{
			showDialogBox(3, null);
		}

		$.post(
			"/account/manage-group-submit/",
			{
				groups_id: -666,
				sid: searches_id,
				name:  name,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 666)
				{
					if (resultId != null)
					{
						$('#add-group-error-' + resultId).html(lp._("Nie masz uprawnień do tworzenia grup"));
					}
					else
					{
						showDialogBox(2, lp._("Nie masz uprawnień do tworzenia grup"));
					}
				}
				else if (data.result == 1)
				{
					var id = data.gid;

					//dodaję nową grupę do listy w filtrach
					$("#panel_filter_groups_div").append(
						'<div id="group_div_' + id + '"><table width="100%"><tr>' +
							'<td width="60%"><div id="filter_button_gr_' + id + '" class="panel_filter_group_row filter_button_gr"><a href="javascript:toggleFilter(\'gr\', ' + id + ')" class="panel_filter_group_row_title" id="panel_filter_group_row_' + id + '" data-id="" data-title=""><i class="fa fa-folder-open-o"></i> <span>' + xssProtect(name) + '</span></a></div></td>' +
							'<td width="20%" align="right"><div class="panel_filter_group_row_hidden" style="display:block"><a href="javascript:showGroupsManagementEdit(' + id + ')" style="font-size:10px">' + lp._('Edytuj') + '</a></div></td>' +
							'<td align="right"><div class="panel_filter_group_row_hidden" style="display:block"><a href="javascript:removeGroup(' + id + ')" style="font-size:10px">' + lp._('Usuń') + '</a></div></td></tr></table></div>'
					);
					$('#panel_filter_group_row_' + id).attr('data-id', id);
					$('#panel_filter_group_row_' + id).attr('data-title', name);

					if ($("#panel_filter_group_row_no_group"))
					{
						$("#panel_filter_group_row_no_group").remove();
					}

					if (resultId == null)
					{
						showDialogBox(2, lp._("Grupa została dodana"));
					}

					var groupsAddListRow = [];
					groupsAddListRow['id'] = parseInt(id);
					groupsAddListRow['title'] = name;
					groupsAddList.push(groupsAddListRow);

					if(tab == 'results')
					{
						//dodaję nową grupę do listy przy wyniku
						$(".mention-groups-list").each(function(index){
							$(this).append(
								'<li class="mention-in-group-' + $(this).attr('rel') + '-' + id + '"><a href="javascript:addResultToGroup(' + $(this).attr('rel') + ',' + id + ')"' + '><i class="fa fa-folder-open"></i> <span>' + xssProtect(name) + '</span></a></li>'
							);
						});

						if (resultId != null)
						{
							$('#mention-groups-list-add-name-' + resultId).val('');
						}
					}
				}

				if (resultId == null)
				{
					$("#panel_data_add_group").dialog('close');
				}

				$("#input_group_name").val('');
			},
			"json"
		);
	}
}

function createNewGroupFromResuls(id)
{
	showGroupsManagementOptions();
	createNewGroup($('#mention-groups-list-add-name-' + id).val(), id);
}

function editGroup(name, id)
{
	if (name == '')
	{
		showDialogBox(2, lp._("Musisz podać nazwę grupy"));
	}
	else
	{
		showDialogBox(3,null);

		$.post(
			"/account/manage-group-submit/",
			{
				groups_id: id,
				sid: searches_id,
				name:  name,
				tknB24 : tknB24
			},
			function(data)
			{
				if (data.result == 666)
				{
					showDialogBox(2, lp._("Nie masz uprawnień do tworzenia grup"));
				}
				else if(data.result == 1)
				{
					$("#group_div_" + id).html(
						'<table width="100%"><tr>'+
							'<td width="60%"><div id="filter_button_gr_' + id + '" class="panel_filter_group_row filter_button_gr"><a href="javascript:toggleFilter(\'gr\', ' + id + ')" class="panel_filter_group_row_title" id="panel_filter_group_row_' + id + '" data-id="" data-title=""><i class="fa fa-folder-open-o"></i> <span>' + xssProtect(name) + '</span></a></div></td>' +
							'<td width="20%" align="right"><div class="panel_filter_group_row_hidden" style="display:block"><a href="javascript:showGroupsManagementEdit(' + id + ')" style="font-size:10px">' + lp._('Edytuj') + '</a></div></td>' +
							'<td align="right"><div class="panel_filter_group_row_hidden" style="display:block"><a href="javascript:removeGroup(' + id + ')" style="font-size:10px">' + lp._('Usuń') + '</a></div></td></tr></table>'
					);
					$('#panel_filter_group_row_' + id).attr('data-id', id);
					$('#panel_filter_group_row_' + id).attr('data-title', name);

					showDialogBox(2, lp._("Nazwa została zmieniona"));

					for (key in groupsAddList)
					{
						if (groupsAddList[key]['id'] == id)
						{
							groupsAddList[key]['title'] = name;
						}
					}

					if (tab == 'results')
					{
						$('.group_added_'+id).html(xssProtect(name));

						//dodaję nową grupę do listy przy wyniku
						$(".mention-groups-list").each(function(index){
							if ($('.mention-in-group-' + $(this).attr('rel') + '-' + id).hasClass('active'))
							{
								$('.mention-in-group-' + $(this).attr('rel') + '-' + id).html('<a href="javascript:deleteResultFromGroup(' + $(this).attr('rel') + ',' + id + ')"' + '><i class="fa fa-folder-open"></i> <span>' + xssProtect(name) + '</span></a>');
							}
							else
							{
								$('.mention-in-group-' + $(this).attr('rel') + '-' + id).html('<a href="javascript:addResultToGroup(' + $(this).attr('rel') + ',' + id + ')"' + '><i class="fa fa-folder-open"></i> <span>' + xssProtect(name) + '</span></a>');
							}
						});
					}
				}
				$("#panel_data_edit_group").dialog('close');
				$("#input_group_name").val('');
			},
			"json"
		);
	}
}

/**
 * Anuluje subskrybcję i automatyczne opłacanie konta. Konto zostaje wyłączone oraz wszystkie dane usunięte
 */
function cancelSubscriptionAndDisableAutomaticPaymentsSubmit()
{
	showDialogBox(3, null);
	$.post(
		'/account/cancel-subscription-and-disable-automatic-payments-submit',
		{
			tknB24 : tknB24
		},
		function(data)
		{
			if (data.result == 1)
			{
				window.location.href = '/account/limits';
			}
			else
			{
				showDialogBox(2, lp._('Błąd podczas anulowania subskrybcji. Spróbuj jeszcze raz.'));
			}
		},
		'json'
	);
	return false;
}

/**
 * Metoda scrolluje zakładke do góry np. po włączeniu filtra - jest zależna od zakładki
 */
function scrollTabToTop()
{
	if (tab == 'results')
	{
		window.scrollTo(0,370);
	}
	else if (tab == 'analysis')
	{
		window.scrollTo(0,60);
	}
}

/**
 * Method to abort all running ajaxes - it is using for example when you change filter but old results are still loading
 */
function abortAjaxes()
{
	$.each(ajaxTable, function (key, value) {
		value.abort();
	});
}

function thereWasAnErrorWithSearchSave(sid)
{
	$.post("/searches/saveerror/",
		{
			sid:sid
		},
		function(data)
		{},
		"text"
	);
}

function isMentionFromYoutube(mention)
{
	if (mention['socialmedia_sites_id'] == 4)
	{
		return true;
	}

	return false;
}

function isYoutubeComment(mention)
{
	return isMentionFromYoutube(mention) &&
		mention['url'] &&
		typeof mention['url'] === 'object' &&
		mention['url'].hasOwnProperty('includes') &&
		mention['url'].includes('&lc=');
}

var currentPageTrendingHashtags = 1;
var noOfPagesAnalysisTrendingHashtags = 0;
var noOfSourcesOnPageAnalysisTrendingHashtags = 20;

function loadHashtagsAnalytics()
{
    var $hashtags_analytics = $('#hashtags_analytics');

    $hashtags_analytics.html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
    var paramsUrl = paramsToUrl();

    ajaxTable.push($.ajax({
        type: 'POST',
        url:'/panel/hashtags-analytics/project_id/'+searches_id,
        data: paramsUrl,
        dataType:"json",
        success:
            function(data)
            {
                if(data != 'error')
                {
                    var dataAsArray = Object.keys(data);
                    if (dataAsArray.length > 0) {
                        var html = '';
                        var date1 = $('#date1').val();
                        var date2 = $('#date2').val();
                        noOfPagesAnalysisTrendingHashtags = Math.ceil(dataAsArray.length/noOfSourcesOnPageAnalysisTrendingHashtags);

                        dataAsArray.forEach(function(key, index) {

                            if(index == 0 && tab =='hashtags')
                            {
                                loadHashtagAnalyticsExplicit(key);
                            }

                            if (index % 10 == 0)
                            {
                                if (index != 0)
                                {
                                    html += '</div>';
                                }

                                html += '<div class="trending-hashtags__column-box">';
                            }

                            if (index > (noOfSourcesOnPageAnalysisTrendingHashtags-1))
                            {
                                html += '<div class="trending-hashtags-entry sources_entry hidden">';
                            }
                            else
                            {
                                html += '<div class="trending-hashtags-entry sources_entry">';
                            }

                            if (tab == 'hashtags')
                            {
                                html += '<table width="100%">' +
                                    '<tr>' +
                                    '<td width="3%"><div class="sources_author_impact">' + (index + 1) + '</div></td>' +
                                    '<td width="23%">' +
                                    '<a href="javascript:loadHashtagAnalyticsExplicit(\''+key+'\')" onclick="IntercomEventsHandler.handle(\'user_click_hashtag\');">'  + key + '</a></td>'+
                                    '<td width="8%" class="sources_highlight" align="right"><strong class="sources_entry-list-value">' + data[key]['mentions_count'] + '</strong><span class="sources_entry-list-title">' + lp._('Wypowiedzi') + '</span></td>' +
                                    '<td width="6%" class="sources_highlight" align="right"><strong class="sources_entry-list-value">' + getSentimentProgressBar(data[key]['sentiment_score']) + '</strong><span class="sources_entry-list-title">' + lp._('Sentyment') + '</span></td>' +
                                    '<td width="10%" class="sources_highlight" align="right"><strong class="sources_entry-list-value">' + data[key]['social_media_reach'].toFixed(0) + '</strong><span class="sources_entry-list-title">' + lp._('Zasięg') + '</span></td>' +
                                    '</tr>' +
                                    '</table>' +
                                    '</div>';
                            }
                            else
                            {
                                html += '<table width="100%">' +
                                    '<tr>' +
                                    '<td width="5%"><div class="sources_author_impact">' + (index + 1) + '</div></td>' +
                                    '<td width="44%"><a href="/panel/results/?sid='+searches_id+'#te={is}'+urlEncode(key)+'&d1='+date1+'&d2='+date2+'" target="_blank" onclick="IntercomEventsHandler.handle(\'user_click_hashtag\');">' + key + '</a></td>' +
                                    '<td width="17%" class="sources_highlight" align="right"><strong class="sources_entry-list-value">' + data[key]['mentions_count'] + '</strong><span class="sources_entry-list-title">' + lp._('Wypowiedzi') + '</span></td>' +
                                    '</tr>' +
                                    '</table>' +
                                    '</div>';
                            }
                        });
                        html += '</div>'

                        html += '<div class="clearfix"></div>';
                        if (dataAsArray.length > (noOfSourcesOnPageAnalysisTrendingHashtags-1))
                        {
                            html += '<div class="panel_pager" style="margin-bottom: 0;"><table width="100%"><tr>';
                            html += '<td class="panel_pager_btns" align="right">';
                            html += '<a href="javascript:void(0);" onclick="goToPageAnalysisTrendingHashtags(1);" class="analysis-trending-hashtags-page-first hidden">' + lp._('Pierwsza') + '</a>';
                            html += '<a href="javascript:void(0);" onclick="goToPageAnalysisTrendingHashtags(-1);" class="analysis-trending-hashtags-page-prev hidden">' + lp._('Poprzednia') + '</a>';
                            for (var i=1; i<=noOfPagesAnalysisTrendingHashtags; i++)
                            {
                                html += '<a href="javascript:void(0);" onclick="goToPageAnalysisTrendingHashtags(' + i + ');" class="analysis-trending-hashtags-page analysis-trending-hashtags-page-no analysis-trending-hashtags-page-' + i + ''+ (i == 1 ? ' selected_page' : '') + '' + (i > 3 ? ' hidden' : '') + '">' + i + '</a>';
                            }

                            html += '<a href="javascript:void(0);" onclick="goToPageAnalysisTrendingHashtags(100);" class="analysis-trending-hashtags-page-next">' + lp._('Następna') + '</a>';
                            html += '<a href="javascript:void(0);" onclick="goToPageAnalysisTrendingHashtags(' + noOfPagesAnalysisTrendingHashtags + ')" class="analysis-trending-hashtags-page-last">' + lp._('Ostatnia') + ' [' + noOfPagesAnalysisTrendingHashtags + ']</a>';
                            html += '</td></tr></table></div>';
                        }

                        $hashtags_analytics.html(html);
                        $(".help_qtip_ajax").qtip();
                    } else {
                        $hashtags_analytics.html('<div align="center">'+lp._('Brak wyników')+'</div>');
                    }
                }
            },
        error:
            function()
            {
                $hashtags_analytics.html('<div align="center">'+lp._('hashtags_analytics.trending.table_error')+' <a href="javascript:void(0);" onclick="loadHashtagsAnalytics();">'+lp._('hashtags_analytics.trending.table_try_again')+'</a></div>');
            }
    }));
}

function goToPageAnalysisTrendingHashtags(pageNumber)
{
	currentPageTrendingHashtags = paginationControler(pageNumber, currentPageTrendingHashtags, noOfPagesAnalysisTrendingHashtags, noOfSourcesOnPageAnalysisTrendingHashtags, '.analysis-trending-hashtags-page', '.trending-hashtags-entry');
}

//Funkcja do obslugi paginacji
function paginationControler(pageNumber, currentPage, noOfPages, noOfSourcesOnPage, page, entry)
{
	if (pageNumber == -1)
	{
		currentPage--;
	}
	else if (pageNumber == 100)
	{
		currentPage++;
	}
	else
	{
		currentPage = pageNumber;
	}

	if (currentPage == 1)
	{
		$(page + '-prev').addClass('hidden');
		$(page + '-first').addClass('hidden');
	}
	else
	{
		$(page + '-prev').removeClass('hidden');
		$(page + '-first').removeClass('hidden');
	}
	if (currentPage == noOfPages)
	{
		$(page + '-next').addClass('hidden');
		$(page + '-last').addClass('hidden');
	}
	else
	{
		$(page + '-next').removeClass('hidden');
		$(page + '-last').removeClass('hidden');
	}

	$(page).removeClass('selected_page');
	$(page + '-' + currentPage).addClass('selected_page');

	$(page + '-no').addClass('hidden');
	$(page + '-no:lt(' + (currentPage + 2)+ ')').removeClass('hidden');
	$(page + '-no:lt(' + (currentPage - 3)+ ')').addClass('hidden');

	$(entry).hide();
	$(entry).addClass('hidden');
	$(entry + ':lt(' + (currentPage * noOfSourcesOnPage) + ')').removeClass('hidden');
	$(entry + ':lt(' + ((currentPage * noOfSourcesOnPage) - noOfSourcesOnPage) + ')').addClass('hidden');
	$(entry).not('.hidden').fadeIn(1500);

	return currentPage;
}

function loadLinksAnalytics()
{
    $('#links_analytics').html('<div align="center"><img src="/static/img/chart-loader.gif" /></div>');
    var paramsUrl = paramsToUrl();

    ajaxTable.push($.ajax({
        type: 'POST',
        url:'/panel/links-analytics/project_id/'+searches_id,
        data: paramsUrl,
        dataType:"json",
        success:
            function(data)
            {
                if(data != 'error')
                {
                    var html = '';
                    var date1 = $('#date1').val();
                    var date2 = $('#date2').val();

                    i = 1;

                    $.each(data, function(item, score)
                    {
                        html += '<div class="sources_entry">' +
                            '<table width="100%">' +
                            '<tr>' +
                            '<td width="7%">' +
                            '<div class="sources_author_impact">' + i + '</div>' +
                            '</td>' +
                            '<td width="75%"><a href="' +
                            item +
                            '" target="_blank">'+
                            item +
                            '</a></td>' +
                            '<td width="100" align="left" style="font-size: 10px; color: #aaa; padding-right: 10px;">' +
                            score +
                            '</td>' +
                            '</tr></table></div>';

                        i++;
                    });

                    $('#links_analytics').html(html);
                    $(".help_qtip_ajax").qtip();
                }
                else
                {
					$('#links_analytics').html('<div align="center">'+lp._('Brak danych')+'</div>');
                }
            },
        error:
            function()
            {
				$('#links_analytics').html('<div align="center">'+lp._('Wystąpił błąd podczas pobierania najpopularniejszych linków')+' <a href="javascript:void(0);" onclick="loadLinksAnalytics();">'+lp._('Spróbuj ponownie')+'</a></div>');
            }
    }));
}


// CANCEL SUB

var CANCEL_MODAL_HEIGHT = 170;
var CANCEL_MODAL_WIDTH = 500;

var successCancelSubModalButtons = {};
var errorCancelSubModalButtons = {};
var confirmCancelSubModalButtons = {};


successCancelSubModalButtons[lp._('cancel_subscription.confirmation_modal.success.primary_action')] = function () {
    $(this).dialog('close');
};
errorCancelSubModalButtons[lp._('cancel_subscription.confirmation_modal.error.primary_action')] = function () {
    $(this).dialog('close');
};
confirmCancelSubModalButtons[lp._('cancel_subscription.confirmation_modal.close')] = function () {
    $(this).dialog('close');
};

confirmCancelSubModalButtons[lp._('cancel_subscription.confirmation_modal.agree')] = function () {
    triggerCancelSubscription(
        (function (data) {
            $(this).dialog('close');
            $("#cancel-subscription__confirmation-modal--success").dialog('open');
        }).bind(this),
        (function (error) {
            $(this).dialog('close');
            $("#cancel-subscription__confirmation-modal--error").dialog('open');
        }).bind(this)
    );
};

function triggerCancelSubscription(successCallback, errorCallback) {
    ajaxTable.push($.ajax({
        type: 'POST',
        url: '/api-rest/v1/accounts/' + account_id + '/cancel-subscription/',
        dataType: 'json',
        success: successCallback,
        error: errorCallback,
    }));
}

$(document).ready(function() {
    $("#cancel-subscription__confirmation-modal--success").dialog({
        title: lp._('cancel_subscription.confirmation_modal.success.title'),
        autoOpen: false,
        resizable: false,
        height: CANCEL_MODAL_HEIGHT,
        width: CANCEL_MODAL_WIDTH,
        modal: true,
        buttons: successCancelSubModalButtons
    });

    $("#cancel-subscription__confirmation-modal--error").dialog({
        title: lp._('cancel_subscription.confirmation_modal.error.title'),
        autoOpen: false,
        resizable: false,
        height: CANCEL_MODAL_HEIGHT,
        width: CANCEL_MODAL_WIDTH,
        modal: true,
        buttons: errorCancelSubModalButtons
    });

    $("#cancel-subscription__modal").dialog({
        autoOpen: false,
        modal : true,
        dialogClass: 'cancel-subscription__modal',
        height: 300,
        width: 500,
        buttons: confirmCancelSubModalButtons
    });

    $('#cancel-subscription__trigger').click(function() {
        $( "#cancel-subscription__modal").dialog('open');
    });
});

function getUrlHashParamByName(param, url) {
	var href = url || window.location.href;
	var paramName = param.replace(/[[\]]/g, '\\$&');
	var results = new RegExp("[?&|#]"+paramName+"(=([^&#]*)|&|#|$)").exec(href);
	if (!results || !results[2]) return null;
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}